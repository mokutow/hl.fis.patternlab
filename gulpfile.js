// gulp core
const gulp = require('gulp');
const gulpPlugins = require('gulp-load-plugins')();
const gulpNotify = require('gulp-notify');
require('gulp-load')(gulp);

const browserSync = require('browser-sync').create();

const context = {
    gulp,
    gulpPlugins,
    gulpNotify,
    browserSync,
    fisclient: {
        src: 'fis-client',
        target: 'fis-client/build'
    },
    fisuicomponents: {
        src: 'fis-ui-components',
        target: 'fis-ui-components/build'
    },
    fispatternlab: {
        src: 'fis-patternlab'
    }
};

function getTask(task, path) {
    return require(`./${path}/_gulp/${task}.js`)(context);
}

// ###### FIS CLIENT ######

// fis:client:clean task
gulp.task('fis:client:clean', getTask('clean', context.fisclient.src));

// fis:client:cp:assets task
gulp.task('fis:client:cp:assets', ['fis:ui:sass:build', 'fis:ui:cp:assets'], getTask('assets', context.fisclient.src));

// fis:client:js:lint
gulp.task('fis:client:js:lint', getTask('js-lint', context.fisclient.src));

// fis:client:js:build task
gulp.task('fis:client:js:build', ['fis:ui:js:lint', 'fis:client:js:lint'], getTask('js', context.fisclient.src));

// fis:client:build task
gulp.task('fis:client:build', [
    'fis:ui:clean',
    'fis:client:clean',
    'fis:client:cp:assets',
    'fis:client:js:build'], callback => { callback(); });

// fis:client:serve task
gulp.task('fis:client:serve', ['fis:client:build'], getTask('serve', context.fisclient.src));


// ###### FIS UI ######

// fis:ui:clean task
gulp.task('fis:ui:clean', getTask('clean', context.fisuicomponents.src));

// fis:ui:sass:lint
gulp.task('fis:ui:sass:lint', getTask('css-lint', context.fisuicomponents.src));

// fis:ui:cp:assets task
gulp.task('fis:ui:cp:assets', getTask('assets', context.fisuicomponents.src));

// fis:ui:svg build task : merge sprites + build scss file
gulp.task('fis:ui:svgSprite', getTask('svg-sprite', context.fisuicomponents.src));

// fis:ui:sass:build task
gulp.task('fis:ui:sass:build', ['fis:ui:svgSprite', 'fis:ui:sass:lint'], getTask('css', context.fisuicomponents.src));

// fis:ui:js:lint
gulp.task('fis:ui:js:lint', getTask('js-lint', context.fisuicomponents.src));

// fis:ui:js:build task
gulp.task('fis:ui:js:build', ['fis:ui:js:lint'], getTask('js', context.fisuicomponents.src));

// fis:ui:build task
gulp.task('fis:ui:build', [
    'fis:ui:clean',
    'fis:ui:sass:build',
    'fis:ui:js:build',
    'fis:ui:cp:assets'], callback => { callback(); });


// ###### FIS PATTERNLAB ######

// patternlab task
gulp.loadTasks(`${__dirname}/${context.fispatternlab.src}/builder/patternlab_gulp.js`);

// lab:clean
gulp.task('lab:clean', getTask('clean', context.fispatternlab.src));

// lab:cp:fis:uibuild task
gulp.task('lab:cp:fis:uibuild', ['fis:ui:cp:assets'], getTask('assets', context.fispatternlab.src));

// lab:cp:data task
gulp.task('lab:cp:data', getTask('data', context.fispatternlab.src));

// lab:cp:styleguide task
gulp.task('lab:cp:styleguide', getTask('styleguide', context.fispatternlab.src));

// lab:styleguide:sass:build task
gulp.task('lab:styleguide:sass:build', getTask('styleguide-css', context.fispatternlab.src));

// lab:sass:lint
gulp.task('lab:sass:lint', getTask('css-lint', context.fispatternlab.src));

// lab:sass:build task
gulp.task('lab:sass:build', ['fis:ui:svgSprite', 'lab:sass:lint', 'fis:ui:sass:lint'], getTask('css', context.fispatternlab.src));

// lab:js:lint
gulp.task('lab:js:lint', getTask('js-lint', context.fispatternlab.src));

// lab:js:build task
gulp.task('lab:js:build', ['fis:ui:js:lint', 'lab:js:lint'], getTask('js', context.fispatternlab.src));

// lab:styleguide:build task
gulp.task('lab:styleguide:build', ['lab:cp:styleguide', 'lab:styleguide:sass:build'], callback => { callback(); });

// lab:build task
gulp.task('lab:build', [
    'lab:clean',
    'lab:styleguide:build',
    'lab:sass:build',
    'lab:js:build',
    'lab:cp:data',
    'lab:cp:fis:uibuild',
    'patternlab'], callback => { callback(); });

// lab:mustache:reload task
gulp.task('lab:mustache:reload', ['lab:cp:data', 'patternlab'], () => {browserSync.reload();});

// lab:assets:reload task
gulp.task('lab:assets:reload', ['lab:cp:fis:uibuild'], () => {browserSync.reload();});

// lab:styleguide:reload task
gulp.task('lab:styleguide:reload', ['lab:styleguide:build'], () => {browserSync.reload();});

// lab:serve task
gulp.task('lab:serve', ['lab:build'], getTask('serve', context.fispatternlab.src));

gulp.task('default', ['lab:serve']);
