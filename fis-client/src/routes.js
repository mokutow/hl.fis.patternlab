import React from 'react';
import { IndexRoute, Route } from 'react-router';

// import { loginUser } from './redux/actions/user';

import Dashboard from 'components/views/Dashboard';


import FisLoginContainer from './components/container/fisLoginContainer.jsx';
import FisApplicationContainer from './components/container/fisApplicationContainer.jsx';
import FisClientContainer from './components/container/fisClientContainer.jsx';
import NotFound from './components/views/NotFound';

export default (store) => {
    const requireLogin = (nextState, replace, cb) => {
        function checkAuth() {
            // console.log('routes checkauth:', nextState);
            // is user in store?
            const { user } = store.getState();
            if (!user) {
                // oops, not logged in, so can't be here!
                replace('/login');
            }
            cb();
        }
        checkAuth();
    };

    return (
        <Route path="/" component={ FisClientContainer }>
            { /* redirect route for login */ }
            <Route path="/login" component={FisLoginContainer} />

            { /* Routes requiring login */ }
            <Route onEnter={requireLogin}>
                { /* Home (main) route */ }
                <IndexRoute component={ Dashboard } />
                <Route component={ Dashboard } path="/app/dashboard" />
                <Route component={FisApplicationContainer} path="/app/:trancode" />
            </Route>

            { /* Catch all route */ }
            <Route path="*" component={NotFound} status={ 404 } />
        </Route>
    );
};
