import React from 'react';

import { FisClient } from 'fisui.js';
import { FisPanel } from 'fisui.js';
import { FisStripe } from 'fisui.js';
import { FisFieldset } from 'fisui.js';
import { FisIcon } from 'fisui.js';
import { FisButtonIcon } from 'fisui.js';
import { FisButtonGroup } from 'fisui.js';
import { FisInputGroup } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';
import { FisInputBase } from 'fisui.js';
import { FisInputDate } from 'fisui.js';
import { FisCheckboxRadioGroup } from 'fisui.js';
import { FisInputPickFlow } from 'fisui.js';
import { FisTabPanel } from 'fisui.js';
import { FisInputGeoHierarchy } from 'fisui.js';
import { FisInputMatchCode } from 'fisui.js';
import { FisInputEquipmentNumber } from 'fisui.js';
import { FisDataGrid2 } from 'fisui.js';
import { FisText } from 'fisui.js';
import { FisButton } from 'fisui.js';
import { FisInputOffsetDate } from 'fisui.js';
import { FisTextArea } from 'fisui.js';
// FIXME: DELETE WHEN  IS REALLY USED
/* eslint-disable */
export default class T9500Cleaned extends React.Component {
    render() {
        const user = {};
        const openAppList =  [
            { app: { name: 'Dashboard', icon: 'fis-icon-glyphicons-home', url: '/', id: 'D000' }, noClose: true, className: 'homeBtn' },
            { app: { name: 'T9500', url: '/t9500/', id: 't9500', params: '&date=2016-01-17&end=&start=DEHAM&end=DEBRV' },
                modal: { name: 'S8020' }, isActive: true },
            { app: { name: 'S8020', url: '/t9500/', id: 't9500' }, isActive: false },
            { app: { name: 'E4842', url: '/t9500/', id: 't9500' }, modal: { name: 'S8020' }, isActive: false },
            { app: { name: 'S8020', url: '/t9500/', id: 't9500' }, isActive: false },
            { app: { name: 'S8020', url: '/t9500/', id: 't9500' }, isActive: false },
            { app: { name: 'E4842', url: '/t9500/', id: 't9500' }, modal: { name: 'S8020' }, isActive: false }
        ];
        const appList = [
            { id: '0', name: 'Customer Information System', sub: [
                { name: 'Bkg General 0-0', url: '0-0', sub: null },
                { name: 'Bkg Detail 0-1', url: '0-1', sub: null },
                { name: 'Bkg Special 0-2', url: '0-2', sub: null }
            ]
            },

            { id: '1', name: 'Tender Management', sub: [
                { name: 'Bkg General 2-0', url: '2-0', sub: null },
                { name: 'Bkg Detail 2-1', url: '2-1', sub: null },
                { name: 'Bkg Special 2-2', url: '2-2', sub: [
                    { name: 'lorem ipsum 2-2-0', url: '2-2-0', sub: null },
                    { name: 'lorem ipsum 2-2-1', url: '2-2-1', sub: null },
                    { name: 'lorem ipsum 2-2-2', url: '2-2-2', sub: null },
                    { name: 'lorem ipsum 2-2-3', url: '2-2-3', sub: null },
                    { name: 'lorem ipsum 2-2-4', url: '2-2-4', sub: null },
                    { name: 'lorem ipsum 2-2-5', url: '2-2-5', sub: null },
                    { name: 'lorem ipsum 2-2-6', url: '2-2-6', sub: null }
                ] },
                { name: 'lorem ipsum 2-3', url: '2-3', sub: null },
                { name: 'lorem ipsum 2-4', url: '2-4', sub: null },
                { name: 'lorem ipsum 2-5', url: '2-5', sub: null }
            ]
            },
            { id: '2', divider: true },
            { id: '3', name: 'Process Exception Advice', sub: null },
            { id: '4', name: 'Tender Management', sub: null },
            { id: '5', name: 'Sales Pricing', sub: null },
            { id: '6', name: 'CS Booking', sub: null },
            { id: '7', name: 'CS Dokumentation', sub: null },
            { id: '8', name: 'Invoicing + Finance', sub: null },
            { id: '9', name: 'CS Import', sub: null },
            { id: '10', name: 'Archive', sub: null },
            { id: '11', name: 'System Maintenance', sub: null }
        ];
        const favouriteList = [
            { id: '0', name: 'Favourites Group', sub: [
                { name: 'Bkg General 0-0', url: '0-0', sub: null },
                { name: 'Bkg Detail 0-1', url: '0-1', sub: null },
                { name: 'Bkg Special 0-2', url: '0-2', sub: null }
            ]
            },
            { id: '1', divider: true },
            { id: '2', name: 'Tender Management', sub: null },
            { id: '3', name: 'Tender Management', sub: null },
            { id: '4', name: 'Sales Pricing', sub: null },
            { id: '5', name: 'CS Booking', sub: null },
            { id: '6', name: 'CS Dokumentation', sub: null },
            { id: '7', name: 'Invoicing + Finance', sub: null },
            { id: '8', name: 'CS Import', sub: null },
            { id: '9', name: 'Archive', sub: null },
            { id: '10', name: 'System Maintenance', sub: null },
            { id: '11', divider: true },
            { id: '12', name: 'Organize Favorites', className: 'edit-favs', sub: null },
        ];


        return (
            <FisClient openAppList={openAppList} appList={appList} favouriteList={favouriteList} user={user} >
                <form>
                    <FisPanel buttonGroupsTop={[			{ buttons: [
									{title: "Refresh", variant: "content-default"},
									{title: "Save", variant: "content-default"}
									] }
		]} buttonGroupsMiddle={[			{ buttons: [
									{title: "Excel Download", variant: "content-default"}
									] },
					{ buttons: [
									{title: "Change Log", variant: "content-default"}
									] },
					{ buttons: [
									{title: "Cust. Monitoring", variant: "content-default"},
									{title: "EU Customs", variant: "content-default"}
									] },
					{ buttons: [
									{title: "Inventory", variant: "content-default"},
									{title: "Combi", variant: "content-default"}
									] },
					{ buttons: [
									{title: "AutoDisp.Incl. ", variant: "content-default"},
									{title: "AutoDisp. Excl.", variant: "content-default"},
									{title: "Remove Depot", variant: "content-default"},
									{title: "Reset Routing", variant: "content-default"}
									] },
					{ buttons: [
									{title: "Link", variant: "content-default"},
									{title: "Link/Maint. WO", variant: "content-default"},
									{title: "Separate", variant: "content-default"},
									{title: "Temp. Separate", variant: "content-default"},
									{title: "WO Details", variant: "content-default"}
									] },
					{ buttons: [
									{title: "Confirm", variant: "content-default"}
									] }
		]} buttonGroupsBottom={[			{ buttons: [
									{title: "Close", variant: "content-default"},
									{title: "Cancel", variant: "content-default"}
									] }
		]}>
                        <FisStripe isOpened={true} buttonGroups={[			{ buttons: [
										{title: "Mass Select", variant: "content-default"}
										] }
			,
												{ buttons: [{title: "Clear", variant: "content-default"},{title: "Find", variant: "content-hero"}]}]}>
                            <FisContainerColumn colSpan={22} className="flex-box">
                                <div>
                                    <FisContainerRow>
                                        <FisContainerColumn colSpan={22} className="flex-box">
                                            <FisTabPanel tabData={[
											{
												title: "Geo-Hier",
												id: "1",
												content: (<div>
												<FisContainerRow>
													<FisContainerColumn colSpan={22} className="flex-box">
														<FisFieldset legend="Geo Hierarchies" >
														<FisContainerRow>
															<FisContainerColumn colSpan={9} >
																<FisInputGroup label=" ">
																	<FisInputGeoHierarchy />
																</FisInputGroup>
															</FisContainerColumn>
														</FisContainerRow>
														<FisContainerRow>
															<FisContainerColumn colSpan={9} >
																<FisInputGroup label=" ">
																	<FisInputGeoHierarchy />
																</FisInputGroup>
															</FisContainerColumn>
														</FisContainerRow>
														<FisContainerRow>
															<FisContainerColumn colSpan={9} >
																<FisInputGroup label=" ">
																	<FisInputGeoHierarchy />
																</FisInputGroup>
															</FisContainerColumn>
														</FisContainerRow>
														<FisContainerRow>
															<FisContainerColumn colSpan={9} >
																<FisInputGroup label=" ">
																	<FisInputGeoHierarchy />
																</FisInputGroup>
															</FisContainerColumn>
														</FisContainerRow>
														</FisFieldset>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={22} className="flex-box">
														<FisFieldset legend="Date Range" >
														<FisContainerRow>
															<FisContainerColumn colSpan={7} >
																<FisInputGroup label="From">
																	<FisInputOffsetDate />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={7} >
																<FisInputGroup label="To">
																	<FisInputOffsetDate />
																</FisInputGroup>
															</FisContainerColumn>
														</FisContainerRow>
														</FisFieldset>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={22} className="flex-box">
														<FisFieldset legend="Ports" >
														<FisContainerRow>
															<FisContainerColumn colSpan={4} >
																<FisInputGroup label=" ">
																	<FisInputBase type="text"
																			colSpan={4} />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={4} >
																<FisInputGroup label=" ">
																	<FisInputBase type="text"
																			colSpan={4} />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={4} >
																<FisInputGroup label=" ">
																	<FisInputBase type="text"
																			colSpan={4} />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={4} >
																<FisInputGroup label=" ">
																	<FisInputBase type="text"
																			colSpan={4} />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={4} >
																<FisInputGroup label=" ">
																	<FisInputBase type="text"
																			colSpan={4} />
																</FisInputGroup>
															</FisContainerColumn>
														</FisContainerRow>
														</FisFieldset>
													</FisContainerColumn>
												</FisContainerRow>
												</div>
												)
											},
											{
												title: "Place",
												id: "2",
												content: (<div>
												<FisContainerRow>
													<FisContainerColumn colSpan={222} className="flex-box">
														<FisContainerRow>
															<FisContainerColumn colSpan={8} className="flex-box">
																<FisInputGroup label="Matchcode">
																	<FisInputPickFlow colSpan={8}>
																		<FisInputMatchCode />
																	</FisInputPickFlow>
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={5} className="flex-box">
																<FisInputGroup label="Locode">
																	<FisInputPickFlow colSpan={5}>
																		<FisInputBase type="text"
																				colSpan={4} />
																	</FisInputPickFlow>
																</FisInputGroup>
															</FisContainerColumn>
														</FisContainerRow>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={22} className="flex-box">
														<FisContainerRow>
															<FisContainerColumn colSpan={18} className="flex-box">
																<FisCheckboxRadioGroup inputlist={[
																	{ label: "All Places" },
																	{ label: "Selection Place" },
																	{ label: "Depots" }
																]} opts={{type: "radio", label: "Take into account", stacked: false, mandatory: false, name: "ESelectPlaceTabAllPlaces_ShortInteger"}} />
															</FisContainerColumn>
														</FisContainerRow>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={22} className="flex-box">
														<FisFieldset legend="Date Range" >
														<FisContainerRow>
															<FisContainerColumn colSpan={7} >
																<FisInputGroup label="From">
																	<FisInputOffsetDate />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={7} >
																<FisInputGroup label="To">
																	<FisInputOffsetDate />
																</FisInputGroup>
															</FisContainerColumn>
														</FisContainerRow>
														</FisFieldset>
													</FisContainerColumn>
												</FisContainerRow>
												</div>
												)
											},
											{
												title: "From-To",
												id: "3",
												content: (<div>
												<FisContainerRow>
													<FisContainerColumn colSpan={22} className="flex-box">
														<FisContainerRow>
															<FisContainerColumn colSpan={11} className="flex-box">
																<FisFieldset legend="From" >
																<FisContainerRow>
																	<FisContainerColumn colSpan={8} >
																		<FisInputGroup label="Place">
																			<FisInputPickFlow colSpan={8}>
																				<FisInputMatchCode />
																			</FisInputPickFlow>
																		</FisInputGroup>
																	</FisContainerColumn>
																</FisContainerRow>
																<FisContainerRow>
																	<FisContainerColumn colSpan={5} >
																		<FisInputGroup label="Locode">
																			<FisInputPickFlow colSpan={5}>
																				<FisInputBase type="text"
																						colSpan={4} />
																			</FisInputPickFlow>
																		</FisInputGroup>
																	</FisContainerColumn>
																</FisContainerRow>
																<FisContainerRow>
																	<FisContainerColumn colSpan={9} >
																		<FisInputGroup label="Geo Hier.">
																			<FisInputGeoHierarchy />
																		</FisInputGroup>
																	</FisContainerColumn>
																</FisContainerRow>
																</FisFieldset>
															</FisContainerColumn>
															<FisContainerColumn colSpan={11} className="flex-box">
																<FisFieldset legend="To" >
																<FisContainerRow>
																	<FisContainerColumn colSpan={8} >
																		<FisInputPickFlow colSpan={8}>
																			<FisInputMatchCode />
																		</FisInputPickFlow>
																	</FisContainerColumn>
																</FisContainerRow>
																<FisContainerRow>
																	<FisContainerColumn colSpan={5} >
																		<FisInputPickFlow colSpan={5}>
																			<FisInputBase type="text"
																					colSpan={4} />
																		</FisInputPickFlow>
																	</FisContainerColumn>
																</FisContainerRow>
																<FisContainerRow>
																	<FisContainerColumn colSpan={9} >
																		<FisInputGroup label=" ">
																			<FisInputGeoHierarchy />
																		</FisInputGroup>
																	</FisContainerColumn>
																</FisContainerRow>
																</FisFieldset>
															</FisContainerColumn>
														</FisContainerRow>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={22} className="flex-box">
														<FisFieldset legend="Date Range" >
														<FisContainerRow>
															<FisContainerColumn colSpan={7} >
																<FisInputGroup label="From">
																	<FisInputOffsetDate />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={7} >
																<FisInputGroup label="To">
																	<FisInputOffsetDate />
																</FisInputGroup>
															</FisContainerColumn>
														</FisContainerRow>
														</FisFieldset>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={22} className="flex-box">
														<div>
														<FisContainerRow>
															<FisContainerColumn colSpan={11} className="flex-box">
																<FisFieldset legend="Pre Voyage" >
																<FisContainerRow>
																	<FisContainerColumn colSpan={5} >
																		<FisInputGroup label="DP Voyage">
																			<FisInputPickFlow colSpan={5}>
																				<FisInputBase type="number" colSpan={4}  />
																			</FisInputPickFlow>
																		</FisInputGroup>
																	</FisContainerColumn>
																	<FisContainerColumn colSpan={5} >
																		<FisInputGroup label="Locode">
																			<FisInputPickFlow colSpan={5}>
																				<FisInputBase type="text"
																						colSpan={4} />
																			</FisInputPickFlow>
																		</FisInputGroup>
																	</FisContainerColumn>
																</FisContainerRow>
																</FisFieldset>
															</FisContainerColumn>
															<FisContainerColumn colSpan={11} className="flex-box">
																<FisFieldset legend="Post Voyage" >
																<FisContainerRow>
																	<FisContainerColumn colSpan={5} >
																		<FisInputGroup label="DP Voyage">
																			<FisInputPickFlow colSpan={5}>
																				<FisInputBase type="number" colSpan={4}  />
																			</FisInputPickFlow>
																		</FisInputGroup>
																	</FisContainerColumn>
																	<FisContainerColumn colSpan={5} >
																		<FisInputGroup label="Locode">
																			<FisInputPickFlow colSpan={5}>
																				<FisInputBase type="text"
																						colSpan={4} />
																			</FisInputPickFlow>
																		</FisInputGroup>
																	</FisContainerColumn>
																</FisContainerRow>
																</FisFieldset>
															</FisContainerColumn>
														</FisContainerRow>
														</div>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={22} className="flex-box">
														<FisCheckboxRadioGroup colSpan={22} inputlist={[
															{ label: "Include all Voyages related to this Vessel Call", name: "ESelectFromtoInclAllVoyages_Flag" }
														]} opts={{type: "checkbox", label: "", stacked: true, name: "ESelectFromtoInclAllVoyages_FlagGroup" , standalone: true}} />
													</FisContainerColumn>
												</FisContainerRow>
												</div>
												)
											},
											{
												title: "Subcon",
												id: "4",
												content: (<div>
												<FisContainerRow>
													<FisContainerColumn colSpan={22} className="flex-box">
														<FisContainerRow>
															<FisContainerColumn colSpan={9} className="flex-box">
																<FisInputGroup label="WO Subcontractor">
																	<FisInputPickFlow colSpan={8}>
																		<FisInputMatchCode />
																	</FisInputPickFlow>
																</FisInputGroup>
															</FisContainerColumn>
														</FisContainerRow>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={22} className="flex-box">
														<FisFieldset legend="Date Range" >
														<FisContainerRow>
															<FisContainerColumn colSpan={7} >
																<FisInputGroup label="From">
																	<FisInputOffsetDate />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={7} >
																<FisInputGroup label="To">
																	<FisInputOffsetDate />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={7} >
																<FisCheckboxRadioGroup colSpan={7} inputlist={[
																	{ label: "of WO Creation", name: "ESelectSubconTabOfWoCreat_Flag" }
																]} opts={{type: "checkbox", label: "", stacked: false, name: "ESelectSubconTabOfWoCreat_FlagGroup" , standalone: true}} />
															</FisContainerColumn>
														</FisContainerRow>
														</FisFieldset>
													</FisContainerColumn>
												</FisContainerRow>
												</div>
												)
											},
											{
												title: "Date Range",
												id: "5",
												content: (<FisFieldset legend="Date Range" >
												<FisContainerRow>
													<FisContainerColumn colSpan={22} >
														<FisInputGroup label="From">
															<FisInputOffsetDate />
														</FisInputGroup>
													</FisContainerColumn>
													<FisContainerColumn colSpan={22} >
														<FisInputGroup label="To">
															<FisInputOffsetDate />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												</FisFieldset>
												)
											},
											{
												title: "Preadvice",
												id: "6",
												content: (<div>
												<FisContainerRow>
													<FisContainerColumn colSpan={22} className="flex-box">
														<FisFieldset legend="Preadvice" >
														<FisContainerRow>
															<FisContainerColumn colSpan={8} >
																<FisInputGroup label="Subcontractor">
																	<FisInputPickFlow colSpan={8}>
																		<FisInputMatchCode />
																	</FisInputPickFlow>
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={6} >
																<FisInputGroup label="Number">
																	<FisInputBase type="number" colSpan={6}  />
																</FisInputGroup>
															</FisContainerColumn>
														</FisContainerRow>
														</FisFieldset>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={22} className="flex-box">
														<FisFieldset legend="Date Range" >
														<FisContainerRow>
															<FisContainerColumn colSpan={7} >
																<FisInputGroup label="From">
																	<FisInputOffsetDate />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={7} >
																<FisInputGroup label="To">
																	<FisInputOffsetDate />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={7} >
																<FisCheckboxRadioGroup colSpan={7} inputlist={[
																	{ label: "of Preadvice", name: "ESelectPreadviceTabOfPreadv_Flag" }
																]} opts={{type: "checkbox", label: "", stacked: false, name: "ESelectPreadviceTabOfPreadv_FlagGroup" , standalone: true}} />
															</FisContainerColumn>
														</FisContainerRow>
														</FisFieldset>
													</FisContainerColumn>
												</FisContainerRow>
												</div>
												)
											},
											{
												title: "AutoDispatch",
												id: "7",
												content: (<FisFieldset legend="Auto Dispatch" >
												<FisContainerRow>
													<FisContainerColumn colSpan={22} >
														<FisContainerRow>
															<FisContainerColumn colSpan={6} >
																<FisInputGroup label="Task ID">
																	<FisInputBase type="number" colSpan={6}  />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={9} >
																<FisInputGroup label="Short Name">
																	<FisInputBase type="text"
																			colSpan={9} />
																</FisInputGroup>
															</FisContainerColumn>
														</FisContainerRow>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={22} >
														<FisFieldset legend="Triggered within Date Range" >
														<FisContainerRow>
															<FisContainerColumn colSpan={7} >
																<FisInputGroup label="From">
																	<FisInputOffsetDate />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={7} >
																<FisInputGroup label="To">
																	<FisInputOffsetDate />
																</FisInputGroup>
															</FisContainerColumn>
														</FisContainerRow>
														</FisFieldset>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={22} >
														<FisInputGroup label="Run">
															<FisInputBase type="number" colSpan={6}  />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												</FisFieldset>
												)
											},
											{
												title: "Single",
												id: "8",
												content: (<FisFieldset legend="Single Selection" >
												<FisContainerRow>
													<FisContainerColumn colSpan={22} >
														<FisInputGroup label="Shipment">
															<FisInputPickFlow colSpan={7}>
																<FisInputBase type="number" colSpan={6}  />
															</FisInputPickFlow>
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={22} >
														<FisInputGroup label="Work Order">
															<FisInputPickFlow colSpan={7}>
																<FisInputBase type="number" colSpan={6}  />
															</FisInputPickFlow>
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={22} >
														<FisInputGroup label="Container">
															<FisInputEquipmentNumber  />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={22} >
														<FisInputGroup label="MTD">
															<FisInputBase type="text"
																	colSpan={10} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={22} >
														<FisInputGroup label="PSV">
															<FisInputBase type="number" colSpan={6}  />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												</FisFieldset>
												)
											}
										]} />
                                        </FisContainerColumn>
                                    </FisContainerRow>
                                </div>
                            </FisContainerColumn>
                            <FisContainerColumn colSpan="" className="flex-box">
                                <div>
                                    <FisContainerRow>
                                        <FisContainerColumn colSpan="" className="flex-box">
                                            <FisContainerRow>
                                                <FisContainerColumn colSpan={21} className="flex-box">
                                                    <div>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={21} className="flex-box">
                                                                <FisFieldset legend="Transportation Request" >
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={17} >
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={8} >
                                                                                    <FisInputGroup label="Resp.TD Office">
                                                                                        <FisInputPickFlow colSpan={8}>
                                                                                            <FisInputMatchCode />
                                                                                        </FisInputPickFlow>
                                                                                    </FisInputGroup>
                                                                                </FisContainerColumn>
                                                                                <FisContainerColumn colSpan={7} >
                                                                                    <FisInputGroup label="Group">
                                                                                        <FisInputPickFlow colSpan={7}>
                                                                                            <FisInputBase type="text"
                                                                                                          colSpan={6} />
                                                                                        </FisInputPickFlow>
                                                                                    </FisInputGroup>
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={21} >
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={21} >
                                                                                    <FisCheckboxRadioGroup colSpan={20} inputlist={[
																	{ label: "Not announced", name: "EFilterNotAnnounced_Flag" },
																	{ label: "Withdrawn", name: "EFilterWithdrawn_Flag" },
																	{ label: "Announced", name: "EFilterAnnounced_Flag" },
																	{ label: "Confirmed", name: "EFilterConfirmed_Flag" }
																]} opts={{type: "checkbox", label: "", stacked: false, name: "EFilterNotAnnounced_FlagGroup"}} />
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={21} >
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={4} >
                                                                                    <FisFieldset legend="Include" >
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={3} >
                                                                                                <FisContainerRow>
                                                                                                    <FisContainerColumn colSpan={1} >
                                                                                                        <FisCheckboxRadioGroup colSpan={1} inputlist={[
																					{ label: "", name: "EFilterIncludeRed_Flag" }
																				]} opts={{type: "checkbox", label: "", stacked: false, name: "EFilterIncludeRed_FlagGroup" , standalone: true}} />
                                                                                                    </FisContainerColumn>
                                                                                                    <FisContainerColumn colSpan={2} >
                                                                                                        <FisIcon icon="fis-icon-applications" />
                                                                                                    </FisContainerColumn>
                                                                                                </FisContainerRow>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={3} >
                                                                                                <FisContainerRow>
                                                                                                    <FisContainerColumn colSpan={1} >
                                                                                                        <FisCheckboxRadioGroup colSpan={1} inputlist={[
																					{ label: "", name: "EFilterIncludeYellow_Flag" }
																				]} opts={{type: "checkbox", label: "", stacked: false, name: "EFilterIncludeYellow_FlagGroup" , standalone: true}} />
                                                                                                    </FisContainerColumn>
                                                                                                    <FisContainerColumn colSpan={2} >
                                                                                                        <FisIcon icon="fis-icon-applications" />
                                                                                                    </FisContainerColumn>
                                                                                                </FisContainerRow>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={3} >
                                                                                                <FisContainerRow>
                                                                                                    <FisContainerColumn colSpan={1} >
                                                                                                        <FisCheckboxRadioGroup colSpan={1} inputlist={[
																					{ label: "", name: "EFilterIncludeGreen_Flag" }
																				]} opts={{type: "checkbox", label: "", stacked: false, name: "EFilterIncludeGreen_FlagGroup" , standalone: true}} />
                                                                                                    </FisContainerColumn>
                                                                                                    <FisContainerColumn colSpan={2} >
                                                                                                        <FisIcon icon="fis-icon-applications" />
                                                                                                    </FisContainerColumn>
                                                                                                </FisContainerRow>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                    </FisFieldset>
                                                                                </FisContainerColumn>
                                                                                <FisContainerColumn colSpan={15} >
                                                                                    <div>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={15} >
                                                                                                <FisContainerRow>
                                                                                                    <FisContainerColumn colSpan={6} >
                                                                                                        <FisInputGroup label="MoT">
                                                                                                            <FisInputBase type="text"
                                                                                                                          colSpan={3} />
                                                                                                            <FisInputBase type="text"
                                                                                                                          colSpan={3} />
                                                                                                        </FisInputGroup>
                                                                                                    </FisContainerColumn>
                                                                                                    <FisContainerColumn colSpan={8} >
                                                                                                        <FisCheckboxRadioGroup colSpan={8} inputlist={[
																					{ label: "Import", name: "EFilterIncludeImport_Flag" },
																					{ label: "Export", name: "EFilterIncludeExport_Flag" }
																				]} opts={{type: "checkbox", label: "", stacked: false, name: "EFilterIncludeImport_FlagGroup" , standalone: true}} />
                                                                                                    </FisContainerColumn>
                                                                                                </FisContainerRow>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={8} >
                                                                                                <div>
                                                                                                    <FisContainerRow>
                                                                                                        <FisContainerColumn colSpan={8} >
                                                                                                            <FisCheckboxRadioGroup inputlist={[
																					{ label: "All" },
																					{ label: "Only" },
																					{ label: "Excl." }
																				]} opts={{type: "radio", label: " ", stacked: false, mandatory: false, name: "EFilterCustomerMove_ShortInteger"}} />
                                                                                                        </FisContainerColumn>
                                                                                                    </FisContainerRow>
                                                                                                    <FisContainerRow>
                                                                                                        <FisContainerColumn colSpan={8} >
                                                                                                            <FisCheckboxRadioGroup inputlist={[
																					{ label: "All" },
																					{ label: "Only" },
																					{ label: "Excl." }
																				]} opts={{type: "radio", label: " ", stacked: false, mandatory: false, name: "EFilterProductIndicator_ShortInteger"}} />
                                                                                                        </FisContainerColumn>
                                                                                                    </FisContainerRow>
                                                                                                    <FisContainerRow>
                                                                                                        <FisContainerColumn colSpan={8} >
                                                                                                            <FisCheckboxRadioGroup inputlist={[
																					{ label: "All" },
																					{ label: "Only" },
																					{ label: "Excl." }
																				]} opts={{type: "radio", label: " ", stacked: false, mandatory: false, name: "EFilterReleased_ShortInteger"}} />
                                                                                                        </FisContainerColumn>
                                                                                                    </FisContainerRow>
                                                                                                </div>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                    </div>
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                </FisFieldset>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={21} className="flex-box">
                                                                <FisFieldset legend="Pre Transportation Request" >
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={8} >
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={5} >
                                                                                    <FisInputGroup label="Start Location">
                                                                                        <FisInputPickFlow colSpan={5}>
                                                                                            <FisInputBase type="text"
                                                                                                          colSpan={4} />
                                                                                        </FisInputPickFlow>
                                                                                    </FisInputGroup>
                                                                                </FisContainerColumn>
                                                                                <FisContainerColumn colSpan={3} >
                                                                                    <FisInputGroup label="MoT">
                                                                                        <FisInputBase type="text"
                                                                                                      colSpan={3} />
                                                                                    </FisInputGroup>
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={15} >
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={15} >
                                                                                    <FisCheckboxRadioGroup colSpan={15} inputlist={[
																	{ label: "Arrived", name: "EFilterInclArrivedPreTreq_Flag" },
																	{ label: "Started", name: "EFilterInclStartedPreTreq_Flag" },
																	{ label: "Not Started", name: "EFilterInclNotStartPreTreq_Flag" }
																]} opts={{type: "checkbox", label: "", stacked: false, name: "EFilterInclArrivedPreTreq_FlagGroup" }} />
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={9} >
                                                                            <FisFieldset legend="Post Transportation Request" >
                                                                                <FisContainerRow>
                                                                                    <FisContainerColumn colSpan={5} >
                                                                                        <FisInputGroup label="End Location">
                                                                                            <FisInputPickFlow colSpan={5}>
                                                                                                <FisInputBase type="text"
                                                                                                              colSpan={4} />
                                                                                            </FisInputPickFlow>
                                                                                        </FisInputGroup>
                                                                                    </FisContainerColumn>
                                                                                    <FisContainerColumn colSpan={3} >
                                                                                        <FisInputGroup label="MoT">
                                                                                            <FisInputBase type="text"
                                                                                                          colSpan={3} />
                                                                                        </FisInputGroup>
                                                                                    </FisContainerColumn>
                                                                                </FisContainerRow>
                                                                            </FisFieldset>
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                </FisFieldset>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                    </div>
                                                </FisContainerColumn>
                                                <FisContainerColumn colSpan={27} className="flex-box">
                                                    <div>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={27} className="flex-box">
                                                                <FisContainerRow>
                                                                    <FisContainerColumn colSpan={12} className="flex-box">
                                                                        <div>
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={12} className="flex-box">
                                                                                    <FisFieldset legend="Shipment" >
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={11} >
                                                                                                <FisContainerRow>
                                                                                                    <FisContainerColumn colSpan={3} >
                                                                                                        <div>
                                                                                                            <FisContainerRow>
                                                                                                                <FisContainerColumn colSpan={3} >
                                                                                                                    <FisText >MT</FisText>
                                                                                                                </FisContainerColumn>
                                                                                                            </FisContainerRow>
                                                                                                            <FisContainerRow>
                                                                                                                <FisContainerColumn colSpan={3} >
                                                                                                                    <FisText >US Flag</FisText>
                                                                                                                </FisContainerColumn>
                                                                                                            </FisContainerRow>
                                                                                                        </div>
                                                                                                    </FisContainerColumn>
                                                                                                    <FisContainerColumn colSpan={8} >
                                                                                                        <div>
                                                                                                            <FisContainerRow>
                                                                                                                <FisContainerColumn colSpan={8} >
                                                                                                                    <FisCheckboxRadioGroup inputlist={[
																							{ label: "All" },
																							{ label: "Only" },
																							{ label: "Excl." }
																						]} opts={{type: "radio", label: " ", stacked: false, mandatory: false, name: "EFilterMtContainer_ShortInteger"}} />
                                                                                                                </FisContainerColumn>
                                                                                                            </FisContainerRow>
                                                                                                            <FisContainerRow>
                                                                                                                <FisContainerColumn colSpan={8} >
                                                                                                                    <FisCheckboxRadioGroup inputlist={[
																							{ label: "All" },
																							{ label: "Only" },
																							{ label: "Excl." }
																						]} opts={{type: "radio", label: " ", stacked: false, mandatory: false, name: "EFilterUsFlag_ShortInteger"}} />
                                                                                                                </FisContainerColumn>
                                                                                                            </FisContainerRow>
                                                                                                        </div>
                                                                                                    </FisContainerColumn>
                                                                                                </FisContainerRow>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                    </FisFieldset>
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={12} className="flex-box">
                                                                                    <FisFieldset legend="Equipment" >
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={11} >
                                                                                                <FisContainerRow>
                                                                                                    <FisContainerColumn colSpan={4} >
                                                                                                        <FisInputGroup label="Type">
                                                                                                            <FisInputPickFlow colSpan={4}>
                                                                                                                <FisInputBase type="text"
                                                                                                                              colSpan={3} />
                                                                                                            </FisInputPickFlow>
                                                                                                        </FisInputGroup>
                                                                                                    </FisContainerColumn>
                                                                                                    <FisContainerColumn colSpan={6} >
                                                                                                        <FisCheckboxRadioGroup colSpan={6} inputlist={[
																					{ label: "Stand.", name: "EFilterStandardContainer_Flag" },
																					{ label: "Non Stand.", name: "EFilterNonStandardContainer_Flag" }
																				]} opts={{type: "checkbox", label: "", stacked: false, name: "EFilterStandardContainer_FlagGroup" , standalone: true}} />
                                                                                                    </FisContainerColumn>
                                                                                                </FisContainerRow>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={8} >
                                                                                                <FisCheckboxRadioGroup inputlist={[
																			{ label: "All" },
																			{ label: "Only" },
																			{ label: "Excl." }
																		]} opts={{type: "radio", label: " ", stacked: false, mandatory: false, name: "EFilterCntrAssigned_ShortInteger"}} />
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={8} >
                                                                                                <FisCheckboxRadioGroup inputlist={[
																			{ label: "All" },
																			{ label: "Only" },
																			{ label: "Excl." }
																		]} opts={{type: "radio", label: " ", stacked: false, mandatory: false, name: "EFilterOog_ShortInteger"}} />
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                    </FisFieldset>
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
                                                                        </div>
                                                                    </FisContainerColumn>
                                                                    <FisContainerColumn colSpan={15} className="flex-box">
                                                                        <FisFieldset legend="Cargo" >
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={14} >
                                                                                    <FisContainerRow>
                                                                                        <FisContainerColumn colSpan={4} >
                                                                                            <div>
                                                                                                <FisContainerRow>
                                                                                                    <FisContainerColumn colSpan={4} >
                                                                                                        <FisText >DG</FisText>
                                                                                                    </FisContainerColumn>
                                                                                                </FisContainerRow>
                                                                                                <FisContainerRow>
                                                                                                    <FisContainerColumn colSpan={4} >
                                                                                                        <FisText >Temp.Contr.</FisText>
                                                                                                    </FisContainerColumn>
                                                                                                </FisContainerRow>
                                                                                            </div>
                                                                                        </FisContainerColumn>
                                                                                        <FisContainerColumn colSpan={6} >
                                                                                            <div>
                                                                                                <FisContainerRow>
                                                                                                    <FisContainerColumn colSpan={6} >
                                                                                                        <FisCheckboxRadioGroup inputlist={[
																					{ label: "All" },
																					{ label: "Only" },
																					{ label: "Excl." }
																				]} opts={{type: "radio", label: " ", stacked: false, mandatory: false, name: "EFilterDgCargo_ShortInteger"}} />
                                                                                                    </FisContainerColumn>
                                                                                                </FisContainerRow>
                                                                                                <FisContainerRow>
                                                                                                    <FisContainerColumn colSpan={6} >
                                                                                                        <FisCheckboxRadioGroup inputlist={[
																					{ label: "All" },
																					{ label: "Only" },
																					{ label: "Excl." }
																				]} opts={{type: "radio", label: " ", stacked: false, mandatory: false, name: "EFilterTemperatureControlled_ShortInteger"}} />
                                                                                                    </FisContainerColumn>
                                                                                                </FisContainerRow>
                                                                                            </div>
                                                                                        </FisContainerColumn>
                                                                                    </FisContainerRow>
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={8} >
                                                                                    <div>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={5} >
                                                                                                <FisContainerRow>
                                                                                                    <FisContainerColumn colSpan={5} >
                                                                                                        <FisInputGroup label="Min. Weight">
                                                                                                            <FisInputBase type="number" colSpan={5}  />
                                                                                                        </FisInputGroup>
                                                                                                    </FisContainerColumn>
                                                                                                </FisContainerRow>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={8} >
                                                                                                <FisContainerRow>
                                                                                                    <FisContainerColumn colSpan={8} >
                                                                                                        <FisInputGroup label="Max. Weight">
                                                                                                            <FisInputBase type="number" colSpan={5}  />
                                                                                                            <FisInputBase type="text"
                                                                                                                          colSpan={3} />
                                                                                                        </FisInputGroup>
                                                                                                    </FisContainerColumn>
                                                                                                </FisContainerRow>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                    </div>
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={14} >
                                                                                    <FisFieldset legend="Commodity" >
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={14} >
                                                                                                <FisCheckboxRadioGroup colSpan={14} inputlist={[
																			{ label: "Prohib.", name: "EFilterProhibitedCommodity_Flag" },
																			{ label: "Restr.", name: "EFilterRestrictedCommodity_Flag" },
																			{ label: "Indist.", name: "EFilterIndistinctCommodity_Flag" },
																			{ label: "No Restr.", name: "EFilterNotRestrCommodity_Flag" }
																		]} opts={{type: "checkbox", label: "", stacked: false, name: "EFilterProhibitedCommodity_FlagGroup"}} />
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                    </FisFieldset>
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
                                                                        </FisFieldset>
                                                                    </FisContainerColumn>
                                                                </FisContainerRow>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={27} className="flex-box">
                                                                <div>
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={27} className="flex-box">
                                                                            <FisFieldset legend="Auto Dispatch" >
                                                                                <FisContainerRow>
                                                                                    <FisContainerColumn colSpan={16} >
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={15} >
                                                                                                <FisCheckboxRadioGroup colSpan={21} inputlist={[
																			{ label: "Included ", name: "EFilterInclToAutoDispatch_Flag" },
																			{ label: "Excluded from Auto Dispatch ", name: "EFilterExclFromAutoDispatch_Flag" }
																		]} opts={{type: "checkbox", label: "", stacked: false, name: "EFilterInclToAutoDispatch_FlagGroup"}} />
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                    </FisContainerColumn>
                                                                                </FisContainerRow>
                                                                                <FisContainerRow>
                                                                                    <FisContainerColumn colSpan={15} >
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={15} >
                                                                                                <FisCheckboxRadioGroup inputlist={[
																			{ label: "All" },
																			{ label: "Linked" },
																			{ label: "Failed" },
																			{ label: "Pending" },
																			{ label: "Not Applicable" }
																		]} opts={{type: "radio", label: "Auto WO Status", stacked: false, mandatory: false, name: "EFilterAutoDispatchStatus_ShortInteger"}} />
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                    </FisContainerColumn>
                                                                                </FisContainerRow>
                                                                            </FisFieldset>
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={27} className="flex-box">
                                                                            <FisFieldset legend="Preplanning" >
                                                                                <FisContainerRow>
                                                                                    <FisContainerColumn colSpan={26} >
                                                                                        <FisCheckboxRadioGroup colSpan={24} inputlist={[
																	{ label: "Created", name: "EFilterPreadviceCreated_Flag" },
																	{ label: "Sent", name: "EFilterPreadviceSent_Flag" },
																	{ label: "Confirmed", name: "EFilterPreadviceConfirmed_Flag" },
																	{ label: "Rejected", name: "EFilterPreadviceRejected_Flag" },
																	{ label: "Not Preplanned", name: "EFilterPreadviceNotPlanned_Flag" }
																]} opts={{type: "checkbox", label: "", stacked: false, name: "EFilterPreadviceCreated_FlagGroup"}} />
                                                                                    </FisContainerColumn>
                                                                                </FisContainerRow>
                                                                            </FisFieldset>
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                </div>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                    </div>
                                                </FisContainerColumn>
                                            </FisContainerRow>
                                        </FisContainerColumn>
                                    </FisContainerRow>
                                    <FisContainerRow>
                                        <FisContainerColumn  className="flex-box">
                                            <FisContainerRow>
                                                <FisContainerColumn colSpan={21} className="flex-box">
                                                    <FisFieldset legend="Selection Options" >
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={18} >
                                                                <FisContainerRow>
                                                                    <FisContainerColumn colSpan={18} >
                                                                        <FisCheckboxRadioGroup colSpan={18} inputlist={[
															{ label: "Show all Pre-/On-Carriage Legs", name: "ESelOptionShowAllPreOnLeg_Flag" },
															{ label: "Shipment Grouping", name: "ESelOptionShipmentGrouping_Flag" }
														]} opts={{type: "checkbox", label: "", stacked: false, name: "ESelOptionShowAllPreOnLeg_FlagGroup"}} />
                                                                    </FisContainerColumn>
                                                                </FisContainerRow>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                    </FisFieldset>
                                                </FisContainerColumn>
                                                <FisContainerColumn colSpan={12} className="flex-box">
                                                    <FisFieldset legend="Show" >
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={11} >
                                                                <FisCheckboxRadioGroup inputlist={[
													{ label: "Gross" },
													{ label: "Cargo Weight" }
												]} opts={{type: "radio", label: " ", stacked: false, mandatory: false, name: "EShowGrossOrCargoWeight_ShortInteger"}} />
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                    </FisFieldset>
                                                </FisContainerColumn>
                                                <FisContainerColumn colSpan={15} className="flex-box">
                                                    <FisFieldset legend="Mass Selection" >
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={5} >
                                                                <FisContainerRow>
                                                                    <FisContainerColumn colSpan={3} >
                                                                        <FisCheckboxRadioGroup colSpan={2} inputlist={[
															{ label: "On", name: "EMassSelectOn_Flag" }
														]} opts={{type: "checkbox", label: "", stacked: false, name: "EMassSelectOn_FlagGroup" , standalone: true}} />
                                                                    </FisContainerColumn>
                                                                    <FisContainerColumn colSpan={2} >
                                                                        <FisIcon icon="fis-icon-applications" />
                                                                    </FisContainerColumn>
                                                                </FisContainerRow>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                    </FisFieldset>
                                                </FisContainerColumn>
                                            </FisContainerRow>
                                        </FisContainerColumn>
                                    </FisContainerRow>
                                </div>
                            </FisContainerColumn>
                        </FisStripe>
                        <FisStripe isOpened={true} buttonGroups={[]}>
                            <FisContainerColumn className="flex-box" colSpan={100}>
                                <div>
                                    <FisContainerRow>
                                        <FisContainerColumn colSpan={100} className="flex-box">
                                            <div>
                                                <FisContainerRow>
                                                    <FisContainerColumn colSpan={100} className="flex-box">
                                                        <FisDataGrid2 columnDefs={[{ headerName:"", field:"", width: 63 },{ headerName:"S!T", field:"s!t", width: 48 },{ headerName:"Tmp!Sep.", field:"tmp!sep.", width: 62 },{ headerName:"Tran!Ex", field:"tran!ex", width: 62 },{ headerName:"Sh!Ty", field:"sh!ty", width: 53 },{ headerName:"Shipment", field:"shipment", width: 96 },{ headerName:"Seq", field:"seq", width: 59 },{ headerName:"MoT", field:"mot", width: 67 },{ headerName:"Cntr!Type", field:"cntr!type", width: 68 },{ headerName:"Weight", field:"weight", width: 89 },{ headerName:"Unit", field:"unit", width: 61 },{ headerName:"Rem", field:"rem", width: 53 },{ headerName:"Prod.!Ind.", field:"prod.!ind.", width: 68 },{ headerName:"Temp![C]", field:"temp![c]", width: 61 },{ headerName:"DG", field:"dg", width: 53 },{ headerName:"DG!Acc", field:"dg!acc", width: 54 },{ headerName:"OD", field:"od", width: 53 },{ headerName:"US! Flg", field:"us! flg", width: 57 },{
											headerName: "Proh/Restr",
											children: [
												{ headerName:"P/R/I", field:"p/r/i", width: 66 },
												{ headerName:"for", field:"for", width: 51 }
											]
										},{ headerName:"Aut!Excl", field:"aut!excl", width: 53 },{ headerName:"Aut!ST", field:"aut!st", width: 54 },{ headerName:"M!S", field:"m!s", width: 53 },{ headerName:"I/E", field:"i/e", width: 53 },{
											headerName: "Start ",
											children: [
												{ headerName:"Locode", field:"locode", width: 73 },
												{ headerName:"Place", field:"place", width: 113 },
												{ headerName:"Date", field:"date", width: 110 },
												{ headerName:"Time", field:"time", width: 93 }
											]
										},{
											headerName: "Pos/Del",
											children: [
												{ headerName:"Locode", field:"locode", width: 73 },
												{ headerName:"Place", field:"place", width: 113 },
												{ headerName:"Postcode", field:"postcode", width: 89 },
												{ headerName:"City", field:"city", width: 133 },
												{ headerName:"Date", field:"date", width: 110 },
												{ headerName:"Time Sign", field:"time sign", width: 47 },
												{ headerName:"Time", field:"time", width: 93 }
											]
										},{
											headerName: "End",
											children: [
												{ headerName:"Locode", field:"locode", width: 73 },
												{ headerName:"Place", field:"place", width: 113 },
												{ headerName:"Date", field:"date", width: 110 },
												{ headerName:"Time", field:"time", width: 93 }
											]
										},{
											headerName: "Container",
											children: [
												{ headerName:"Number", field:"number", width: 133 },
												{ headerName:"Rel.", field:"rel.", width: 51 }
											]
										},{ headerName:"Handover!Reference", field:"handover!reference", width: 113 },{ headerName:"Last!Free Date", field:"last!free date", width: 113 },{
											headerName: "Preadvice",
											children: [
												{ headerName:"Number", field:"number", width: 113 },
												{ headerName:"Subcon", field:"subcon", width: 113 },
												{ headerName:"ST", field:"st", width: 68 }
											]
										},{ headerName:"Grouping!No.", field:"grouping!no.", width: 88 },{
											headerName: "Work Order",
											children: [
												{ headerName:"Number", field:"number", width: 103 },
												{ headerName:"Subcon", field:"subcon", width: 113 }
											]
										}
								]} rowData={[{ "index": "1", "": " ",
								"s!t": " ",
								"tmp!sep.": " ",
								"tran!ex": " ",
								"sh!ty": " ",
								"shipment": " ",
								"seq": " ",
								"mot": " ",
								"cntr!type": " ",
								"weight": " ",
								"unit": " ",
								"rem": " ",
								"prod.!ind.": " ",
								"temp![c]": " ",
								"dg": " ",
								"dg!acc": " ",
								"od": " ",
								"us! flg": " ",
								"p/r/i": " ",
								"for": " ",
								"aut!excl": " ",
								"aut!st": " ",
								"m!s": " ",
								"i/e": " ",
								"locode": " ",
								"place": " ",
								"date": " ",
								"time": " ",
								"locode": " ",
								"place": " ",
								"postcode": " ",
								"city": " ",
								"date": " ",
								"time sign": " ",
								"time": " ",
								"locode": " ",
								"place": " ",
								"date": " ",
								"time": " ",
								"number": " ",
								"rel.": " ",
								"handover!reference": " ",
								"last!free date": " ",
								"number": " ",
								"subcon": " ",
								"st": " ",
								"grouping!no.": " ",
								"number": " ",
								"subcon": " "
								 },
								{ "index": "2", "": " ",
								"s!t": " ",
								"tmp!sep.": " ",
								"tran!ex": " ",
								"sh!ty": " ",
								"shipment": " ",
								"seq": " ",
								"mot": " ",
								"cntr!type": " ",
								"weight": " ",
								"unit": " ",
								"rem": " ",
								"prod.!ind.": " ",
								"temp![c]": " ",
								"dg": " ",
								"dg!acc": " ",
								"od": " ",
								"us! flg": " ",
								"p/r/i": " ",
								"for": " ",
								"aut!excl": " ",
								"aut!st": " ",
								"m!s": " ",
								"i/e": " ",
								"locode": " ",
								"place": " ",
								"date": " ",
								"time": " ",
								"locode": " ",
								"place": " ",
								"postcode": " ",
								"city": " ",
								"date": " ",
								"time sign": " ",
								"time": " ",
								"locode": " ",
								"place": " ",
								"date": " ",
								"time": " ",
								"number": " ",
								"rel.": " ",
								"handover!reference": " ",
								"last!free date": " ",
								"number": " ",
								"subcon": " ",
								"st": " ",
								"grouping!no.": " ",
								"number": " ",
								"subcon": " "
								 },
								{ "index": "3", "": " ",
								"s!t": " ",
								"tmp!sep.": " ",
								"tran!ex": " ",
								"sh!ty": " ",
								"shipment": " ",
								"seq": " ",
								"mot": " ",
								"cntr!type": " ",
								"weight": " ",
								"unit": " ",
								"rem": " ",
								"prod.!ind.": " ",
								"temp![c]": " ",
								"dg": " ",
								"dg!acc": " ",
								"od": " ",
								"us! flg": " ",
								"p/r/i": " ",
								"for": " ",
								"aut!excl": " ",
								"aut!st": " ",
								"m!s": " ",
								"i/e": " ",
								"locode": " ",
								"place": " ",
								"date": " ",
								"time": " ",
								"locode": " ",
								"place": " ",
								"postcode": " ",
								"city": " ",
								"date": " ",
								"time sign": " ",
								"time": " ",
								"locode": " ",
								"place": " ",
								"date": " ",
								"time": " ",
								"number": " ",
								"rel.": " ",
								"handover!reference": " ",
								"last!free date": " ",
								"number": " ",
								"subcon": " ",
								"st": " ",
								"grouping!no.": " ",
								"number": " ",
								"subcon": " "
								 },
								{ "index": "4", "": " ",
								"s!t": " ",
								"tmp!sep.": " ",
								"tran!ex": " ",
								"sh!ty": " ",
								"shipment": " ",
								"seq": " ",
								"mot": " ",
								"cntr!type": " ",
								"weight": " ",
								"unit": " ",
								"rem": " ",
								"prod.!ind.": " ",
								"temp![c]": " ",
								"dg": " ",
								"dg!acc": " ",
								"od": " ",
								"us! flg": " ",
								"p/r/i": " ",
								"for": " ",
								"aut!excl": " ",
								"aut!st": " ",
								"m!s": " ",
								"i/e": " ",
								"locode": " ",
								"place": " ",
								"date": " ",
								"time": " ",
								"locode": " ",
								"place": " ",
								"postcode": " ",
								"city": " ",
								"date": " ",
								"time sign": " ",
								"time": " ",
								"locode": " ",
								"place": " ",
								"date": " ",
								"time": " ",
								"number": " ",
								"rel.": " ",
								"handover!reference": " ",
								"last!free date": " ",
								"number": " ",
								"subcon": " ",
								"st": " ",
								"grouping!no.": " ",
								"number": " ",
								"subcon": " "
								 }
								]} colSpan={100} />
                                                    </FisContainerColumn>
                                                </FisContainerRow>
                                            </div>
                                        </FisContainerColumn>
                                    </FisContainerRow>
                                    <FisContainerRow>
                                        <FisContainerColumn colSpan={100} className="flex-box">
                                            <FisContainerRow>
                                                <FisContainerColumn colSpan={50} className="flex-box">
                                                    <FisFieldset legend="Quick Routing Update" >
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={11} >
                                                                <FisContainerRow>
                                                                    <FisContainerColumn colSpan={3} >
                                                                        <FisInputGroup label="MoT">
                                                                            <FisInputBase type="text"
                                                                                          colSpan={3} />
                                                                        </FisInputGroup>
                                                                    </FisContainerColumn>
                                                                    <FisContainerColumn colSpan={8} >
                                                                        <FisInputGroup label="Depot">
                                                                            <FisInputPickFlow colSpan={8}>
                                                                                <FisInputMatchCode />
                                                                            </FisInputPickFlow>
                                                                        </FisInputGroup>
                                                                    </FisContainerColumn>
                                                                </FisContainerRow>
                                                            </FisContainerColumn>
                                                            <FisContainerColumn colSpan={26} >
                                                                <FisContainerRow>
                                                                    <FisContainerColumn colSpan={8} >
                                                                        <FisInputGroup label="RA/WW Terminal">
                                                                            <FisInputPickFlow colSpan={8}>
                                                                                <FisInputMatchCode />
                                                                            </FisInputPickFlow>
                                                                        </FisInputGroup>
                                                                    </FisContainerColumn>
                                                                    <FisContainerColumn colSpan={3} >
                                                                        <FisCheckboxRadioGroup colSpan={3} inputlist={[
													{ label: "Split", name: "EQuickUpdateCrCwSplitFlag_Flag" }
												]} opts={{type: "checkbox", label: "", stacked: false, name: "EQuickUpdateCrCwSplitFlag_FlagGroup" , standalone: true}} />
                                                                    </FisContainerColumn>
                                                                    <FisContainerColumn colSpan={3} >
                                                                        <FisInputGroup label=" ">
                                                                            <FisInputBase type="text"
                                                                                          colSpan={3} />
                                                                        </FisInputGroup>
                                                                    </FisContainerColumn>
                                                                    <FisContainerColumn colSpan={1} >
                                                                        <FisText>/</FisText>
                                                                    </FisContainerColumn>
                                                                    <FisContainerColumn colSpan={3} >
                                                                        <FisInputGroup label=" ">
                                                                            <FisInputBase type="text"
                                                                                          colSpan={3} />
                                                                        </FisInputGroup>
                                                                    </FisContainerColumn>
                                                                    <FisContainerColumn colSpan={8} >
                                                                        <FisCheckboxRadioGroup colSpan={8} inputlist={[
													{ label: "CR/CW Term. required", name: "ETerminalRequiredFlag_Flag" }
												]} opts={{type: "checkbox", label: "", stacked: false, name: "ETerminalRequiredFlag_FlagGroup" , standalone: true}} />
                                                                    </FisContainerColumn>
                                                                </FisContainerRow>
                                                            </FisContainerColumn>
                                                            <FisContainerColumn colSpan={12} >
                                                                <FisContainerRow>
                                                                    <FisContainerColumn colSpan={12} >
                                                                        <FisInputGroup label="Routing Ref. Rule">
                                                                            <FisInputPickFlow colSpan={7}>
                                                                                <FisInputBase type="number" colSpan={6}  />
                                                                            </FisInputPickFlow>
                                                                            <FisInputBase type="text"
                                                                                          readOnly
                                                                                          colSpan={5} />
                                                                        </FisInputGroup>
                                                                    </FisContainerColumn>
                                                                </FisContainerRow>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                    </FisFieldset>
                                                </FisContainerColumn>
                                                <FisContainerColumn colSpan={20} className="flex-box">
                                                    <FisFieldset legend="Quick Work Order Handling" >
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={15} >
                                                                <FisContainerRow>
                                                                    <FisContainerColumn colSpan={8} >
                                                                        <FisInputGroup label="Subcontractor">
                                                                            <FisInputPickFlow colSpan={8}>
                                                                                <FisInputMatchCode />
                                                                            </FisInputPickFlow>
                                                                        </FisInputGroup>
                                                                    </FisContainerColumn>
                                                                    <FisContainerColumn colSpan={7} >
                                                                        <FisInputGroup label="Link WO">
                                                                            <FisInputPickFlow colSpan={7}>
                                                                                <FisInputBase type="number"
                                                                                              colSpan={6}
                                                                                              infoflow />
                                                                            </FisInputPickFlow>
                                                                        </FisInputGroup>
                                                                    </FisContainerColumn>
                                                                </FisContainerRow>
                                                            </FisContainerColumn>
                                                            <FisContainerColumn colSpan={4} >
                                                                <FisButtonGroup colSpan={4}>
                                                                    <FisButton variant="content-default" colSpan={4}>Clear Fields</FisButton>
                                                                </FisButtonGroup>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                    </FisFieldset>
                                                </FisContainerColumn>
                                            </FisContainerRow>
                                        </FisContainerColumn>
                                    </FisContainerRow>
                                </div>
                            </FisContainerColumn>
                        </FisStripe>
                        <FisStripe isOpened={true} buttonGroups={[]}>
                            <FisContainerColumn className="flex-box">
                                <FisContainerRow>
                                    <FisContainerColumn className="flex-box">
                                        <div>
                                            <FisContainerRow>
                                                <FisContainerColumn colSpan={44} className="flex-box">
                                                    <FisTabPanel tabData={[
													{
														title: "Routing",
														id: "1",
														content: (<FisContainerRow>
															<FisContainerColumn colSpan={100} className="flex-box">
																<FisDataGrid2 columnDefs={[{ headerName:"Link", field:"link", width: 53 },{ headerName:"State", field:"state", width: 65 },{ headerName:"Type", field:"type", width: 63 },{ headerName:"MoT", field:"mot", width: 67 },{ headerName:"E/F", field:"e/f", width: 54 },{ headerName:"Locode", field:"locode", width: 78 },{ headerName:"Place", field:"place", width: 119 },{ headerName:"Postcode", field:"postcode", width: 103 },{ headerName:"City", field:"city", width: 183 },{ headerName:"Arrival Date", field:"arrival date", width: 113 },{ headerName:"Arrival Time", field:"arrival time", width: 103 },{ headerName:"Departure Date", field:"departure date", width: 113 },{ headerName:"Departure Time", field:"departure time", width: 103 },{ headerName:"DP Voyage", field:"dp voyage", width: 113 },{ headerName:"CutOff/PickUp", field:"cutoff/pickup", width: 113 },{ headerName:"Date", field:"date", width: 113 },{ headerName:"Time", field:"time", width: 103 },{ headerName:"DODAAC", field:"dodaac", width: 85 },{ headerName:"Work Order ", field:"work order ", width: 100 },{ headerName:"Last WO", field:"last wo", width: 100 }
																]} rowData={[{ "index": "1", "link": " ",
																"state": " ",
																"type": " ",
																"mot": " ",
																"e/f": " ",
																"locode": " ",
																"place": " ",
																"postcode": " ",
																"city": " ",
																"arrival date": " ",
																"arrival time": " ",
																"departure date": " ",
																"departure time": " ",
																"dp voyage": " ",
																"cutoff/pickup": " ",
																"date": " ",
																"time": " ",
																"dodaac": " ",
																"work order ": " ",
																"last wo": " "
																 },
																{ "index": "2", "link": " ",
																"state": " ",
																"type": " ",
																"mot": " ",
																"e/f": " ",
																"locode": " ",
																"place": " ",
																"postcode": " ",
																"city": " ",
																"arrival date": " ",
																"arrival time": " ",
																"departure date": " ",
																"departure time": " ",
																"dp voyage": " ",
																"cutoff/pickup": " ",
																"date": " ",
																"time": " ",
																"dodaac": " ",
																"work order ": " ",
																"last wo": " "
																 },
																{ "index": "3", "link": " ",
																"state": " ",
																"type": " ",
																"mot": " ",
																"e/f": " ",
																"locode": " ",
																"place": " ",
																"postcode": " ",
																"city": " ",
																"arrival date": " ",
																"arrival time": " ",
																"departure date": " ",
																"departure time": " ",
																"dp voyage": " ",
																"cutoff/pickup": " ",
																"date": " ",
																"time": " ",
																"dodaac": " ",
																"work order ": " ",
																"last wo": " "
																 },
																{ "index": "4", "link": " ",
																"state": " ",
																"type": " ",
																"mot": " ",
																"e/f": " ",
																"locode": " ",
																"place": " ",
																"postcode": " ",
																"city": " ",
																"arrival date": " ",
																"arrival time": " ",
																"departure date": " ",
																"departure time": " ",
																"dp voyage": " ",
																"cutoff/pickup": " ",
																"date": " ",
																"time": " ",
																"dodaac": " ",
																"work order ": " ",
																"last wo": " "
																 }
																]} colSpan={100} />
															</FisContainerColumn>
														</FisContainerRow>
														)
													},
													{
														title: "Shipment",
														id: "2",
														content: (<FisContainerRow>
															<FisContainerColumn colSpan={19} className="flex-box">
																<FisFieldset legend="Transport" infoflow>
																<FisContainerRow>
																	<FisContainerColumn colSpan={15} >
																		<FisContainerRow>
																			<FisContainerColumn colSpan={10} >
																				<FisInputGroup label="Type">
																						<FisInputBase type="text"
																								readOnly
																								colSpan={2} />
																						<FisInputBase type="text"
																								readOnly
																								colSpan={3} />
																						<FisInputBase type="text"
																								readOnly
																								colSpan={3} />
																						<FisInputBase type="text"
																								readOnly
																								colSpan={2} />
																				</FisInputGroup>
																			</FisContainerColumn>
																			<FisContainerColumn colSpan={5} >
																				<FisCheckboxRadioGroup colSpan={5} inputlist={[
																					{ label: "US Flag", readOnly: true, name: "EShipmentFolderTreq_DaUsRestricted" }
																				]} opts={{type: "checkbox", label: "", stacked: false, name: "EShipmentFolderTreq_DaUsRestrictedGroup" , standalone: true}} />
																			</FisContainerColumn>
																		</FisContainerRow>
																	</FisContainerColumn>
																</FisContainerRow>
																<FisContainerRow>
																	<FisContainerColumn colSpan={11} >
																		<FisContainerRow>
																			<FisContainerColumn colSpan={5} >
																				<FisInputGroup label="DP Voyage">
																					<FisInputBase type="number"
																						colSpan={5}
																						readOnly
																						infoflow />
																				</FisInputGroup>
																			</FisContainerColumn>
																			<FisContainerColumn colSpan={6} >
																				<FisInputGroup label="Schedule Voy.">
																					<FisInputBase type="text"
																							readOnly
																							colSpan={6} />
																				</FisInputGroup>
																			</FisContainerColumn>
																		</FisContainerRow>
																	</FisContainerColumn>
																</FisContainerRow>
																<FisContainerRow>
																	<FisContainerColumn colSpan={18} >
																		<FisInputGroup label="Vessel">
																			<FisInputBase type="text"
																					readOnly
																					colSpan={18} />
																		</FisInputGroup>
																	</FisContainerColumn>
																</FisContainerRow>
																</FisFieldset>
															</FisContainerColumn>
															<FisContainerColumn colSpan={12} className="flex-box">
																<div>
																<FisContainerRow>
																	<FisContainerColumn colSpan={12} className="flex-box">
																		<FisFieldset legend="Customer Service" >
																		<FisContainerRow>
																			<FisContainerColumn colSpan={5} >
																				<FisInputGroup label="Contact User-ID">
																					<FisInputBase type="text"
																							readOnly
																							colSpan={5} />
																				</FisInputGroup>
																			</FisContainerColumn>
																		</FisContainerRow>
																		<FisContainerRow>
																			<FisContainerColumn colSpan={7} >
																				<FisInputGroup label="Contact Office">
																					<FisInputMatchCode />
																				</FisInputGroup>
																			</FisContainerColumn>
																		</FisContainerRow>
																		</FisFieldset>
																	</FisContainerColumn>
																</FisContainerRow>
																<FisContainerRow>
																	<FisContainerColumn colSpan={12} className="flex-box">
																		<FisFieldset legend="Customer" >
																		<FisContainerRow>
																			<FisContainerColumn colSpan={7} >
																				<FisInputGroup label="Matchcode">
																					<FisInputMatchCode />
																				</FisInputGroup>
																			</FisContainerColumn>
																		</FisContainerRow>
																		<FisContainerRow>
																			<FisContainerColumn colSpan={11} >
																				<FisInputGroup label="Short Name">
																					<FisInputBase type="text"
																							readOnly
																							colSpan={11} />
																				</FisInputGroup>
																			</FisContainerColumn>
																		</FisContainerRow>
																		</FisFieldset>
																	</FisContainerColumn>
																</FisContainerRow>
																</div>
															</FisContainerColumn>
														</FisContainerRow>
														)
													},
													{
														title: "Cargo/Container",
														id: "3",
														content: (<FisContainerRow>
															<FisContainerColumn colSpan={33} className="flex-box">
																<FisDataGrid2 columnDefs={[{ headerName:"Description", field:"description", width: 250 },{ headerName:"HS Code", field:"hs code", width: 106 },{ headerName:"Commodity", field:"commodity", width: 113 },{ headerName:"DG", field:"dg", width: 53 }
																]} rowData={[{ "index": "1", "description": " ",
																"hs code": " ",
																"commodity": " ",
																"dg": " "
																 },
																{ "index": "2", "description": " ",
																"hs code": " ",
																"commodity": " ",
																"dg": " "
																 },
																{ "index": "3", "description": " ",
																"hs code": " ",
																"commodity": " ",
																"dg": " "
																 },
																{ "index": "4", "description": " ",
																"hs code": " ",
																"commodity": " ",
																"dg": " "
																 }
																]} colSpan={33} />
															</FisContainerColumn>
															<FisContainerColumn colSpan={11} className="flex-box">
																<div>
																<FisContainerRow>
																	<FisContainerColumn colSpan={11} className="flex-box">
																		<FisFieldset legend="Container" >
																		<FisContainerRow>
																			<FisContainerColumn colSpan={9} >
																				<FisCheckboxRadioGroup colSpan={9} inputlist={[
																					{ label: "HL Controlled", readOnly: true, name: "EDetailContainerTareWeight_HlControlled" }
																				]} opts={{type: "checkbox", label: "", stacked: true, name: "EDetailContainerTareWeight_HlControlledGroup" , standalone: true}} />
																			</FisContainerColumn>
																		</FisContainerRow>
																		<FisContainerRow>
																			<FisContainerColumn colSpan={11} >
																				<FisContainerRow>
																					<FisContainerColumn colSpan={11} >
																						<FisInputGroup label="Container Gross Weight">
																								<FisInputBase type="number" colSpan={6} readOnly />
																								<FisInputBase type="text"
																										readOnly
																										colSpan={3} />
																						</FisInputGroup>
																					</FisContainerColumn>
																				</FisContainerRow>
																			</FisContainerColumn>
																		</FisContainerRow>
																		<FisContainerRow>
																			<FisContainerColumn colSpan={11} >
																				<FisContainerRow>
																					<FisContainerColumn colSpan={11} >
																						<FisInputGroup label="Container Tare Weight">
																								<FisInputBase type="number" colSpan={6} readOnly />
																								<FisInputBase type="text"
																										readOnly
																										colSpan={3} />
																						</FisInputGroup>
																					</FisContainerColumn>
																				</FisContainerRow>
																			</FisContainerColumn>
																		</FisContainerRow>
																		</FisFieldset>
																	</FisContainerColumn>
																</FisContainerRow>
																<FisContainerRow>
																	<FisContainerColumn colSpan={11} className="flex-box">
																		<FisFieldset legend="Cargo" >
																		<FisContainerRow>
																			<FisContainerColumn colSpan={10} >
																				<FisInputGroup label="Total Cargo Weight">
																						<FisInputBase type="number" colSpan={6} readOnly />
																						<FisInputBase type="text"
																								readOnly
																								colSpan={3} />
																				</FisInputGroup>
																			</FisContainerColumn>
																		</FisContainerRow>
																		</FisFieldset>
																	</FisContainerColumn>
																</FisContainerRow>
																</div>
															</FisContainerColumn>
														</FisContainerRow>
														)
													},
													{
														title: "Remarks",
														id: "4",
														content: (<div>
														<FisContainerRow>
															<FisContainerColumn colSpan={39} className="flex-box">
																<FisFieldset legend="Shipment Remarks" >
																<FisContainerRow>
																	<FisContainerColumn colSpan={36} >
																		<FisInputGroup label=" ">
																			<FisTextArea rows={4}
																					colSpan={36}
																					readOnly />
																		</FisInputGroup>
																	</FisContainerColumn>
																	<FisContainerColumn colSpan={2} >
																		<div>
																		<FisContainerRow>
																			<FisContainerColumn colSpan={2} >
																				<FisButtonIcon icon="fis-icon fis-icon-arrow-up" />
																			</FisContainerColumn>
																		</FisContainerRow>
																		<FisContainerRow>
																			<FisContainerColumn colSpan={2} >
																				<FisButtonIcon icon="fis-icon fis-icon-arrow-down" />
																			</FisContainerColumn>
																		</FisContainerRow>
																		</div>
																	</FisContainerColumn>
																</FisContainerRow>
																</FisFieldset>
															</FisContainerColumn>
														</FisContainerRow>
														<FisContainerRow>
															<FisContainerColumn colSpan={19} className="flex-box">
																<FisInputGroup label="Customer Load Reference">
																	<FisInputBase type="text"
																			readOnly
																			colSpan={19} />
																</FisInputGroup>
															</FisContainerColumn>
														</FisContainerRow>
														</div>
														)
													},
													{
														title: "Overdimension",
														id: "5",
														content: (<FisContainerRow>
															<FisContainerColumn colSpan={8} className="flex-box">
																<FisFieldset legend="Oversize" >
																<FisContainerRow>
																	<FisContainerColumn colSpan={7} >
																		<FisContainerRow>
																			<FisContainerColumn colSpan={7} >
																				<FisInputGroup label="Length Front">
																						<FisInputBase type="number" colSpan={4} readOnly />
																						<FisInputBase type="text"
																								readOnly
																								colSpan={3} />
																				</FisInputGroup>
																			</FisContainerColumn>
																		</FisContainerRow>
																	</FisContainerColumn>
																</FisContainerRow>
																<FisContainerRow>
																	<FisContainerColumn colSpan={7} >
																		<FisContainerRow>
																			<FisContainerColumn colSpan={7} >
																				<FisInputGroup label="Length Rear">
																						<FisInputBase type="number" colSpan={4} readOnly />
																						<FisInputBase type="text"
																								readOnly
																								colSpan={3} />
																				</FisInputGroup>
																			</FisContainerColumn>
																		</FisContainerRow>
																	</FisContainerColumn>
																</FisContainerRow>
																<FisContainerRow>
																	<FisContainerColumn colSpan={7} >
																		<FisContainerRow>
																			<FisContainerColumn colSpan={7} >
																				<FisInputGroup label="Width Left">
																						<FisInputBase type="number" colSpan={4} readOnly />
																						<FisInputBase type="text"
																								readOnly
																								colSpan={3} />
																				</FisInputGroup>
																			</FisContainerColumn>
																		</FisContainerRow>
																	</FisContainerColumn>
																</FisContainerRow>
																<FisContainerRow>
																	<FisContainerColumn colSpan={7} >
																		<FisContainerRow>
																			<FisContainerColumn colSpan={7} >
																				<FisInputGroup label="Width Right">
																						<FisInputBase type="number" colSpan={4} readOnly />
																						<FisInputBase type="text"
																								readOnly
																								colSpan={3} />
																				</FisInputGroup>
																			</FisContainerColumn>
																		</FisContainerRow>
																	</FisContainerColumn>
																</FisContainerRow>
																<FisContainerRow>
																	<FisContainerColumn colSpan={7} >
																		<FisContainerRow>
																			<FisContainerColumn colSpan={7} >
																				<FisInputGroup label="Over Height">
																						<FisInputBase type="number" colSpan={4} readOnly />
																						<FisInputBase type="text"
																								readOnly
																								colSpan={3} />
																				</FisInputGroup>
																			</FisContainerColumn>
																		</FisContainerRow>
																	</FisContainerColumn>
																</FisContainerRow>
																</FisFieldset>
															</FisContainerColumn>
															<FisContainerColumn colSpan={8} className="flex-box">
																<FisFieldset legend="Total" >
																<FisContainerRow>
																	<FisContainerColumn colSpan={7} >
																		<FisContainerRow>
																			<FisContainerColumn colSpan={7} >
																				<FisInputGroup label="Length">
																						<FisInputBase type="number" colSpan={4} readOnly />
																						<FisInputBase type="text"
																								readOnly
																								colSpan={3} />
																				</FisInputGroup>
																			</FisContainerColumn>
																		</FisContainerRow>
																	</FisContainerColumn>
																</FisContainerRow>
																<FisContainerRow>
																	<FisContainerColumn colSpan={7} >
																		<FisContainerRow>
																			<FisContainerColumn colSpan={7} >
																				<FisInputGroup label="Width">
																						<FisInputBase type="number" colSpan={4} readOnly />
																						<FisInputBase type="text"
																								readOnly
																								colSpan={3} />
																				</FisInputGroup>
																			</FisContainerColumn>
																		</FisContainerRow>
																	</FisContainerColumn>
																</FisContainerRow>
																<FisContainerRow>
																	<FisContainerColumn colSpan={7} >
																		<FisContainerRow>
																			<FisContainerColumn colSpan={7} >
																				<FisInputGroup label="Height">
																						<FisInputBase type="number" colSpan={4} readOnly />
																						<FisInputBase type="text"
																								readOnly
																								colSpan={3} />
																				</FisInputGroup>
																			</FisContainerColumn>
																		</FisContainerRow>
																	</FisContainerColumn>
																</FisContainerRow>
																</FisFieldset>
															</FisContainerColumn>
														</FisContainerRow>
														)
													},
													{
														title: "Details",
														id: "6",
														content: (<FisContainerRow>
															<FisContainerColumn colSpan={26} className="flex-box">
																<div>
																<FisContainerRow>
																	<FisContainerColumn colSpan={25} className="flex-box">
																		<FisInputGroup label="Subcontractor Reference">
																			<FisInputBase type="text"
																					colSpan={19} />
																		</FisInputGroup>
																	</FisContainerColumn>
																</FisContainerRow>
																<FisContainerRow>
																	<FisContainerColumn colSpan={26} className="flex-box">
																		<FisFieldset legend="Auto Dispatch " >
																		<FisContainerRow>
																			<FisContainerColumn colSpan={6} >
																				<FisInputGroup label="Task ID">
																					<FisInputBase type="number"
																						colSpan={6}
																						readOnly
																						infoflow />
																				</FisInputGroup>
																			</FisContainerColumn>
																		</FisContainerRow>
																		<FisContainerRow>
																			<FisContainerColumn colSpan={22} >
																				<FisContainerRow>
																					<FisContainerColumn colSpan={6} >
																						<FisInputGroup label="Latest Run ID">
																							<FisInputBase type="number"
																								colSpan={6}
																								readOnly
																								infoflow />
																						</FisInputGroup>
																					</FisContainerColumn>
																					<FisContainerColumn colSpan={16} >
																						<FisInputGroup label="Message">
																							<FisInputBase type="text"
																									readOnly
																									colSpan={16} />
																						</FisInputGroup>
																					</FisContainerColumn>
																				</FisContainerRow>
																			</FisContainerColumn>
																		</FisContainerRow>
																		</FisFieldset>
																	</FisContainerColumn>
																</FisContainerRow>
																</div>
															</FisContainerColumn>
															<FisContainerColumn colSpan={26} className="flex-box">
																<div>
																<FisContainerRow>
																	<FisContainerColumn colSpan={26} className="flex-box">
																		<FisFieldset legend="Capacity Management" >
																		<FisContainerRow>
																			<FisContainerColumn colSpan={25} >
																				<FisContainerRow>
																					<FisContainerColumn colSpan={25} >
																						<FisInputGroup label="Latest PSV">
																								<FisInputBase type="number"
																									colSpan={6}
																									readOnly
																									infoflow />
																								<FisInputBase type="text"
																										readOnly
																										colSpan={16} />
																								<FisInputBase type="text"
																										readOnly
																										colSpan={3} />
																						</FisInputGroup>
																					</FisContainerColumn>
																				</FisContainerRow>
																			</FisContainerColumn>
																		</FisContainerRow>
																		</FisFieldset>
																	</FisContainerColumn>
																</FisContainerRow>
																<FisContainerRow>
																	<FisContainerColumn colSpan={26} className="flex-box">
																		<FisFieldset legend="Steering" >
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={4} >
                                                                                    <FisInputGroup label="Selection Date">
                                                                                        <FisInputDate colSpan={4} readOnly />
                                                                                    </FisInputGroup>
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={4} >
                                                                                    <FisInputGroup label="Build Rule">
                                                                                        <FisInputBase type="number"
                                                                                            colSpan={4}
                                                                                            readOnly
                                                                                            infoflow />
                                                                                    </FisInputGroup>
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
																		</FisFieldset>
																	</FisContainerColumn>
																</FisContainerRow>
																</div>
															</FisContainerColumn>
														</FisContainerRow>
														)
													}
												]} />
                                                </FisContainerColumn>
                                            </FisContainerRow>
                                        </div>
                                    </FisContainerColumn>
                                    <FisContainerColumn colSpan={27} className="flex-box">
                                        <div>
                                            <FisContainerRow>
                                                <FisContainerColumn colSpan={27} className="flex-box">
                                                    <FisTabPanel tabData={[
													{
														title: "Preplanning",
														id: "1",
														content: (<div>
														<FisContainerRow>
															<FisContainerColumn colSpan={28} className="flex-box">
																<div>
																<FisContainerRow>
																	<FisContainerColumn colSpan={14} className="flex-box">
																		<FisContainerRow>
																			<FisContainerColumn colSpan={8} className="flex-box">
																				<FisInputGroup label="Subcontractor">
																					<FisInputPickFlow colSpan={8}>
																						<FisInputMatchCode />
																					</FisInputPickFlow>
																				</FisInputGroup>
																			</FisContainerColumn>
																			<FisContainerColumn colSpan={6} className="flex-box">
																				<FisInputGroup label="Number">
																					<FisInputBase type="number"
																						colSpan={6}
																						infoflow />
																				</FisInputGroup>
																			</FisContainerColumn>
																		</FisContainerRow>
																	</FisContainerColumn>
																</FisContainerRow>
																<FisContainerRow>
																	<FisContainerColumn colSpan={9} className="flex-box">
																		<FisContainerRow>
																			<FisContainerColumn colSpan={2} className="flex-box">
																				<FisInputGroup label="Prepl. Status">
																					<FisInputBase type="text"
																							colSpan={2} />
																				</FisInputGroup>
																			</FisContainerColumn>
																			<FisContainerColumn colSpan={7} className="flex-box">
																				<FisButtonGroup colSpan={7}>
																					<FisButton variant="content-default" colSpan={7}>Set Status</FisButton>
																				</FisButtonGroup>
																			</FisContainerColumn>
																		</FisContainerRow>
																	</FisContainerColumn>
																</FisContainerRow>
																<FisContainerRow>
																	<FisContainerColumn colSpan={27} className="flex-box">
																		<FisContainerRow>
																			<FisContainerColumn colSpan={27} className="flex-box">
																				<FisButtonGroup colSpan={27}>
																					<FisButton variant="content-default" colSpan={7}>Create PA</FisButton>
																					<FisButton variant="content-default" colSpan={6}>Link PA</FisButton>
																					<FisButton variant="content-default" colSpan={8}>Separate PA</FisButton>
																					<FisButton variant="content-default" colSpan={6}>Send PA</FisButton>
																				</FisButtonGroup>
																			</FisContainerColumn>
																		</FisContainerRow>
																	</FisContainerColumn>
																</FisContainerRow>
																</div>
															</FisContainerColumn>
														</FisContainerRow>
														<FisContainerRow>
															<FisContainerColumn colSpan={24} className="flex-box">
																<FisFieldset legend="Reply Upload" >
																<FisContainerRow>
																	<FisContainerColumn colSpan={7} >
																		<FisInputGroup label="Upload File">
																			<FisInputPickFlow colSpan={7}>
																				<FisInputBase type="text"
																						colSpan={6} />
																			</FisInputPickFlow>
																		</FisInputGroup>
																	</FisContainerColumn>
																	<FisContainerColumn colSpan={16} >
																		<FisButtonGroup colSpan={16}>
																			<FisButton variant="content-default" colSpan={8}>Upload Reply</FisButton>
																			<FisButton variant="content-default" colSpan={8}>Show Prepl.</FisButton>
																		</FisButtonGroup>
																	</FisContainerColumn>
																</FisContainerRow>
																</FisFieldset>
															</FisContainerColumn>
														</FisContainerRow>
														<FisContainerRow>
															<FisContainerColumn colSpan={9} className="flex-box">
																<FisContainerRow>
																	<FisContainerColumn colSpan={9} className="flex-box">
																		<FisButtonGroup colSpan={9}>
																			<FisButton variant="content-default" colSpan={8}>Clear Fields</FisButton>
																		</FisButtonGroup>
																	</FisContainerColumn>
																</FisContainerRow>
															</FisContainerColumn>
														</FisContainerRow>
														</div>
														)
													},
													{
														title: "BlockTrain",
														id: "2",
														content: (<div>
														<FisContainerRow>
															<FisContainerColumn colSpan={11} className="flex-box">
																<FisInputGroup label="Internal Number">
																	<FisInputBase type="text"
																			colSpan={11} />
																</FisInputGroup>
															</FisContainerColumn>
														</FisContainerRow>
														<FisContainerRow>
															<FisContainerColumn colSpan={11} className="flex-box">
																<FisInputGroup label="External Number">
																	<FisInputBase type="text"
																			colSpan={11} />
																</FisInputGroup>
															</FisContainerColumn>
														</FisContainerRow>
														</div>
														)
													}
												]} />
                                                </FisContainerColumn>
                                            </FisContainerRow>
                                        </div>
                                    </FisContainerColumn>
                                </FisContainerRow>
                            </FisContainerColumn>
                        </FisStripe>
                    </FisPanel>
                </form>
            </FisClient>
        );
    }
}
