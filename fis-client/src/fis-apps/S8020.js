/* eslint-disable */
function renderApp() {
    var FisText = Fis.FisText;
    var FisButtonIcon = Fis.FisButtonIcon;
    var FisStripe = Fis.FisStripe;
    var FisPanel = Fis.FisPanel;
    var FisButton = Fis.FisButton;
    var FisContainerRow = Fis.FisContainerRow;
    var FisInputBase = Fis.FisInputBase;
    var FisContainerColumn = Fis.FisContainerColumn;
    var FisInputDate = Fis.FisInputDate;
    var FisTextArea = Fis.FisTextArea;
    var FisInputMatchCode = Fis.FisInputMatchCode;
    var FisButtonGroup = Fis.FisButtonGroup;
    var FisInputHarmonizedSystemCode = Fis.FisInputHarmonizedSystemCode;
    var FisInputGroup = Fis.FisInputGroup;
    var FisFieldset = Fis.FisFieldset;
    var FisCheckboxRadioGroup = Fis.FisCheckboxRadioGroup;
    var FisInputPickFlow = Fis.FisInputPickFlow;
    var FisAppHeader = Fis.FisAppHeader;

    return React.createElement(
        FisPanel,
        { buttonGroupsTop: [{ buttons: [{ title: "Refresh", variant: "content-default" }] }], buttonGroupsMiddle: [{ buttons: [{ title: "Revenues", variant: "content-default" }, { title: "Create MTDs", variant: "content-default" }, { title: "BB &lt;-&gt; MT", variant: "content-default" }, { title: "Distribute BC", variant: "content-default" }, { title: "Bkg-Detail", variant: "content-default" }, { title: "Clear", variant: "content-default" }] }], buttonGroupsBottom: [{ buttons: [{ title: "Close", variant: "content-default" }, { title: "Cancel", variant: "content-default" }] }] },
        React.createElement(
            FisStripe,
            { isOpened: true, buttonGroups: [{ buttons: [{ title: "eBkg Exceptions", variant: "content-default" }, { title: "RA HS Code", variant: "content-default" }, { title: "EU Customs", variant: "content-default" }, { title: "Proposal", variant: "content-default" }, { title: "ScheduLing", variant: "content-default" }, { title: "Announce Sel.", variant: "content-default" }, { title: "Ann. Equip.", variant: "content-default" }, { title: "Ann. Compl.", variant: "content-default" }, { title: "Copy", variant: "content-default" }, { title: "Save", variant: "content-default" }] }] },
            React.createElement(
                FisContainerColumn,
                { className: "flex-box", colSpan: "58" },
                React.createElement(
                    "div",
                    null,
                    React.createElement(
                        FisContainerRow,
                        null,
                        React.createElement(
                            FisContainerColumn,
                            { colSpan: "58", className: "flex-box" },
                            React.createElement(
                                FisContainerRow,
                                null,
                                React.createElement(
                                    FisContainerColumn,
                                    { colSpan: "18", className: "flex-box" },
                                    React.createElement(
                                        FisFieldset,
                                        { legend: "Shipment", infoflow: true },
                                        React.createElement(
                                            FisContainerRow,
                                            null,
                                            React.createElement(
                                                FisContainerColumn,
                                                { colSpan: "15" },
                                                React.createElement(
                                                    FisContainerRow,
                                                    null,
                                                    React.createElement(
                                                        FisContainerColumn,
                                                        { colSpan: "6" },
                                                        React.createElement(
                                                            "div",
                                                            null,
                                                            React.createElement(
                                                                FisContainerRow,
                                                                null,
                                                                React.createElement(
                                                                    FisContainerColumn,
                                                                    { colSpan: "6" },
                                                                    React.createElement(
                                                                        FisInputGroup,
                                                                        { label: "Number" },
                                                                        React.createElement(
                                                                            FisInputPickFlow,
                                                                            { colSpan: "6" },
                                                                            React.createElement(FisInputBase, { type: "number",
                                                                            colSpan: "5"
                                                                        })
                                                                    )
                                                                )
                                                            )
                                                        ),
                                                        React.createElement(
                                                            FisContainerRow,
                                                            null,
                                                            React.createElement(
                                                                FisContainerColumn,
                                                                { colSpan: "3" },
                                                                React.createElement(
                                                                    FisInputGroup,
                                                                    { label: "Restriction Class" },
                                                                    React.createElement(FisInputBase, { type: "text",
                                                                    infoflow: true,
                                                                    colSpan: "3" })
                                                                )
                                                            )
                                                        )
                                                    )
                                                ),
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "4" },
                                                    React.createElement(
                                                        "div",
                                                        null,
                                                        React.createElement(
                                                            FisContainerRow,
                                                            null,
                                                            React.createElement(
                                                                FisContainerColumn,
                                                                { colSpan: "3" },
                                                                React.createElement(
                                                                    FisInputGroup,
                                                                    { label: "Type" },
                                                                    React.createElement(FisInputBase, { type: "text",
                                                                    colSpan: "3" })
                                                                )
                                                            )
                                                        ),
                                                        React.createElement(
                                                            FisContainerRow,
                                                            null,
                                                            React.createElement(
                                                                FisContainerColumn,
                                                                { colSpan: "4" },
                                                                React.createElement(
                                                                    FisInputGroup,
                                                                    { label: "Terms" },
                                                                    React.createElement(FisInputBase, { type: "text",
                                                                    colSpan: "4" })
                                                                )
                                                            )
                                                        )
                                                    )
                                                ),
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "5" },
                                                    React.createElement(FisCheckboxRadioGroup, { colSpan: "5", inputlist: [{ label: "PT-Cons", name: "EShipment_PartnerCons" }], opts: { type: "checkbox", label: "", stacked: false, name: "EShipment_PartnerConsGroup", standalone: true } })
                                                )
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "14" },
                                            React.createElement(
                                                FisContainerRow,
                                                null,
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "9" },
                                                    React.createElement(
                                                        FisInputGroup,
                                                        { label: "Rate Agreement" },
                                                        React.createElement(
                                                            FisInputPickFlow,
                                                            { colSpan: "9" },
                                                            React.createElement(FisInputBase, { type: "text",
                                                            infoflow: true,
                                                            colSpan: "8" })
                                                        )
                                                    )
                                                ),
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "5" },
                                                    React.createElement(FisCheckboxRadioGroup, { colSpan: "5", inputlist: [{ label: "Remarks", readOnly: true, name: "ERateAgreementService_BookingRelRemark" }], opts: { type: "checkbox", label: "", stacked: false, name: "ERateAgreementService_BookingRelRemarkGroup", standalone: true } })
                                                )
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "17" },
                                            React.createElement(
                                                FisContainerRow,
                                                null,
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "17" },
                                                    React.createElement(
                                                        FisInputGroup,
                                                        { label: "Status" },
                                                        React.createElement(
                                                            FisInputPickFlow,
                                                            { colSpan: "17" },
                                                            React.createElement(FisInputBase, { type: "text",
                                                            readOnly: true,
                                                            colSpan: "16" })
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "16" },
                                            React.createElement(
                                                FisInputGroup,
                                                { label: "Internal Ref." },
                                                React.createElement(FisInputBase, { type: "text",
                                                colSpan: "16" })
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "9" },
                                            React.createElement(
                                                FisInputGroup,
                                                { label: "Local Bkg. Ref." },
                                                React.createElement(FisInputBase, { type: "text",
                                                colSpan: "9" })
                                            )
                                        )
                                    )
                                )
                            ),
                            React.createElement(
                                FisContainerColumn,
                                { colSpan: "27", className: "flex-box" },
                                React.createElement(
                                    FisFieldset,
                                    { legend: "Customer" },
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "12" },
                                            React.createElement(
                                                FisInputGroup,
                                                { label: "Matchcode" },
                                                React.createElement(
                                                    FisInputPickFlow,
                                                    { colSpan: "12" },
                                                    React.createElement(FisInputMatchCode, { infoflow: true }),
                                                    React.createElement(FisInputBase, { type: "text",
                                                    colSpan: "4" })
                                                )
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "14" },
                                            React.createElement(
                                                FisInputGroup,
                                                { label: "Reference" },
                                                React.createElement(FisInputBase, { type: "text",
                                                colSpan: "14" })
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "14" },
                                            React.createElement(
                                                FisInputGroup,
                                                { label: "Contact" },
                                                React.createElement(FisInputBase, { type: "text",
                                                colSpan: "14" })
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "14" },
                                            React.createElement(
                                                FisInputGroup,
                                                { label: "Telephone" },
                                                React.createElement(FisInputBase, { type: "text",
                                                colSpan: "14" })
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "14" },
                                            React.createElement(
                                                FisInputGroup,
                                                { label: "Mail Address 1" },
                                                React.createElement(FisInputBase, { type: "email", colSpan: "14" })
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "26" },
                                            React.createElement(
                                                FisInputGroup,
                                                { label: "Mail Address 2" },
                                                React.createElement(FisInputBase, { type: "email", colSpan: "14" }),
                                                React.createElement(
                                                    FisInputPickFlow,
                                                    { colSpan: "12" },
                                                    React.createElement(FisCheckboxRadioGroup, { colSpan: "2", inputlist: [{ label: "", readOnly: true, name: "EMoreBcReceiver_Flag" }], opts: { type: "checkbox", label: "", stacked: false, name: "EMoreBcReceiver_Flag", standalone: true } }),
                                                    React.createElement(
                                                        FisText,
                                                        null,
                                                        "More BC Receiver"
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            ),
                            React.createElement(
                                FisContainerColumn,
                                { colSpan: "13", className: "flex-box" },
                                React.createElement(
                                    FisFieldset,
                                    { legend: "Party Functions", infoflow: true },
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "12" },
                                            React.createElement(
                                                FisInputGroup,
                                                { label: "MR" },
                                                React.createElement(
                                                    FisInputPickFlow,
                                                    { colSpan: "12" },
                                                    React.createElement(FisInputMatchCode, { infoflow: true }),
                                                    React.createElement(FisInputBase, { type: "text",
                                                    colSpan: "4" })
                                                )
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "12" },
                                            React.createElement(
                                                FisInputGroup,
                                                { label: "EP" },
                                                React.createElement(
                                                    FisInputPickFlow,
                                                    { colSpan: "12" },
                                                    React.createElement(FisInputMatchCode, { infoflow: true }),
                                                    React.createElement(FisInputBase, { type: "text",
                                                    colSpan: "4" })
                                                )
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "12" },
                                            React.createElement(
                                                FisInputGroup,
                                                { label: "IS" },
                                                React.createElement(
                                                    FisInputPickFlow,
                                                    { colSpan: "12" },
                                                    React.createElement(FisInputMatchCode, { infoflow: true }),
                                                    React.createElement(FisInputBase, { type: "text",
                                                    colSpan: "4" })
                                                )
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "12" },
                                            React.createElement(
                                                FisInputGroup,
                                                { label: "FF" },
                                                React.createElement(
                                                    FisInputPickFlow,
                                                    { colSpan: "12" },
                                                    React.createElement(FisInputMatchCode, { infoflow: true }),
                                                    React.createElement(FisInputBase, { type: "text",
                                                    colSpan: "4" })
                                                )
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "12" },
                                            React.createElement(
                                                FisInputGroup,
                                                { label: "SH" },
                                                React.createElement(
                                                    FisInputPickFlow,
                                                    { colSpan: "12" },
                                                    React.createElement(FisInputMatchCode, { infoflow: true }),
                                                    React.createElement(FisInputBase, { type: "text",
                                                    colSpan: "4" })
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                ),
                React.createElement(
                    FisContainerRow,
                    null,
                    React.createElement(
                        FisContainerColumn,
                        { colSpan: "49", className: "flex-box" },
                        React.createElement(
                            FisContainerRow,
                            null,
                            React.createElement(
                                FisContainerColumn,
                                { colSpan: "23", className: "flex-box" },
                                React.createElement(
                                    FisFieldset,
                                    { legend: "Routing", infoflow: true },
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "12" },
                                            React.createElement(
                                                FisContainerRow,
                                                null,
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "6" },
                                                    React.createElement(
                                                        FisInputGroup,
                                                        { label: "Export" },
                                                        React.createElement(FisInputBase, { type: "text",
                                                        colSpan: "3" }),
                                                        React.createElement(FisInputBase, { type: "text",
                                                        colSpan: "3" })
                                                    )
                                                ),
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "6" },
                                                    React.createElement(
                                                        FisInputGroup,
                                                        { label: "Import" },
                                                        React.createElement(FisInputBase, { type: "text",
                                                        colSpan: "3" }),
                                                        React.createElement(FisInputBase, { type: "text",
                                                        colSpan: "3" })
                                                    )
                                                )
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "16" },
                                            React.createElement(
                                                FisContainerRow,
                                                null,
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "16" },
                                                    React.createElement(
                                                        FisInputGroup,
                                                        { label: "Start" },
                                                        React.createElement(
                                                            FisInputPickFlow,
                                                            { colSpan: "5" },
                                                            React.createElement(FisInputBase, { type: "text",
                                                            colSpan: "4" })
                                                        ),
                                                        React.createElement(
                                                            FisInputPickFlow,
                                                            { colSpan: "8" },
                                                            React.createElement(FisInputMatchCode, { infoflow: true })
                                                        ),
                                                        React.createElement(FisInputBase, { type: "text",
                                                        colSpan: "3" })
                                                    )
                                                )
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "16" },
                                            React.createElement(
                                                FisContainerRow,
                                                null,
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "16" },
                                                    React.createElement(
                                                        FisInputGroup,
                                                        { label: "Via" },
                                                        React.createElement(
                                                            FisInputPickFlow,
                                                            { colSpan: "5" },
                                                            React.createElement(FisInputBase, { type: "text",
                                                            colSpan: "4" })
                                                        ),
                                                        React.createElement(
                                                            FisInputPickFlow,
                                                            { colSpan: "8" },
                                                            React.createElement(FisInputMatchCode, { infoflow: true })
                                                        ),
                                                        React.createElement(FisInputBase, { type: "text",
                                                        colSpan: "3" })
                                                    )
                                                )
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "19" },
                                            React.createElement(
                                                FisContainerRow,
                                                null,
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "16" },
                                                    React.createElement(
                                                        FisInputGroup,
                                                        { label: "Via" },
                                                        React.createElement(
                                                            FisInputPickFlow,
                                                            { colSpan: "5" },
                                                            React.createElement(FisInputBase, { type: "text",
                                                            colSpan: "4" })
                                                        ),
                                                        React.createElement(
                                                            FisInputPickFlow,
                                                            { colSpan: "8" },
                                                            React.createElement(FisInputMatchCode, { infoflow: true })
                                                        ),
                                                        React.createElement(FisInputBase, { type: "text",
                                                        colSpan: "3" })
                                                    )
                                                ),
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "3" },
                                                    React.createElement(FisCheckboxRadioGroup, { colSpan: "3", inputlist: [{ label: "More", name: "EMorePrecarrVias_Flag" }], opts: { type: "checkbox", label: "", stacked: false, name: "EMorePrecarrVias_FlagGroup", standalone: true } })
                                                )
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "16" },
                                            React.createElement(
                                                FisContainerRow,
                                                null,
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "16" },
                                                    React.createElement(
                                                        FisInputGroup,
                                                        { label: "POL" },
                                                        React.createElement(
                                                            FisInputPickFlow,
                                                            { colSpan: "5" },
                                                            React.createElement(FisInputBase, { type: "text",
                                                            colSpan: "4" })
                                                        ),
                                                        React.createElement(
                                                            FisInputPickFlow,
                                                            { colSpan: "8" },
                                                            React.createElement(FisInputMatchCode, { infoflow: true })
                                                        ),
                                                        React.createElement(FisInputBase, { type: "text",
                                                        readOnly: true,
                                                        colSpan: "3" })
                                                    )
                                                )
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "11" },
                                            React.createElement(
                                                FisContainerRow,
                                                null,
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "11" },
                                                    React.createElement(
                                                        FisInputGroup,
                                                        { label: "DP Voy." },
                                                        React.createElement(
                                                            FisInputPickFlow,
                                                            { colSpan: "11" },
                                                            React.createElement(FisInputBase, { type: "number",
                                                            colSpan: "4"
                                                        }),
                                                        React.createElement(FisInputBase, { type: "text",
                                                        readOnly: true,
                                                        placeholder: "Schedule Voyage",
                                                        colSpan: "6" })
                                                    )
                                                )
                                            )
                                        )
                                    )
                                ),
                                React.createElement(
                                    FisContainerRow,
                                    null,
                                    React.createElement(
                                        FisContainerColumn,
                                        { colSpan: "17" },
                                        React.createElement(
                                            FisInputGroup,
                                            { label: "Vessel" },
                                            React.createElement(FisInputBase, { type: "text",
                                            readOnly: true,
                                            colSpan: "17" })
                                        )
                                    )
                                ),
                                React.createElement(
                                    FisContainerRow,
                                    null,
                                    React.createElement(
                                        FisContainerColumn,
                                        { colSpan: "16" },
                                        React.createElement(
                                            FisContainerRow,
                                            null,
                                            React.createElement(
                                                FisContainerColumn,
                                                { colSpan: "16" },
                                                React.createElement(
                                                    FisInputGroup,
                                                    { label: "POD" },
                                                    React.createElement(
                                                        FisInputPickFlow,
                                                        { colSpan: "5" },
                                                        React.createElement(FisInputBase, { type: "text",
                                                        colSpan: "4" })
                                                    ),
                                                    React.createElement(
                                                        FisInputPickFlow,
                                                        { colSpan: "8" },
                                                        React.createElement(FisInputMatchCode, { infoflow: true })
                                                    ),
                                                    React.createElement(FisInputBase, { type: "text",
                                                    colSpan: "3" })
                                                )
                                            )
                                        )
                                    )
                                ),
                                React.createElement(
                                    FisContainerRow,
                                    null,
                                    React.createElement(
                                        FisContainerColumn,
                                        { colSpan: "19" },
                                        React.createElement(
                                            FisContainerRow,
                                            null,
                                            React.createElement(
                                                FisContainerColumn,
                                                { colSpan: "16" },
                                                React.createElement(
                                                    FisInputGroup,
                                                    { label: "Via" },
                                                    React.createElement(
                                                        FisInputPickFlow,
                                                        { colSpan: "5" },
                                                        React.createElement(FisInputBase, { type: "text",
                                                        colSpan: "4" })
                                                    ),
                                                    React.createElement(
                                                        FisInputPickFlow,
                                                        { colSpan: "8" },
                                                        React.createElement(FisInputMatchCode, { infoflow: true })
                                                    ),
                                                    React.createElement(FisInputBase, { type: "text",
                                                    colSpan: "3" })
                                                )
                                            ),
                                            React.createElement(
                                                FisContainerColumn,
                                                { colSpan: "3" },
                                                React.createElement(FisCheckboxRadioGroup, { colSpan: "3", inputlist: [{ label: "More", name: "EMoreOncarrVias_Flag" }], opts: { type: "checkbox", label: "", stacked: false, name: "EMoreOncarrVias_FlagGroup", standalone: true } })
                                            )
                                        )
                                    )
                                ),
                                React.createElement(
                                    FisContainerRow,
                                    null,
                                    React.createElement(
                                        FisContainerColumn,
                                        { colSpan: "13" },
                                        React.createElement(
                                            FisContainerRow,
                                            null,
                                            React.createElement(
                                                FisContainerColumn,
                                                { colSpan: "13" },
                                                React.createElement(
                                                    FisInputGroup,
                                                    { label: "End" },
                                                    React.createElement(
                                                        FisInputPickFlow,
                                                        { colSpan: "5" },
                                                        React.createElement(FisInputBase, { type: "text",
                                                        colSpan: "4" })
                                                    ),
                                                    React.createElement(
                                                        FisInputPickFlow,
                                                        { colSpan: "8" },
                                                        React.createElement(FisInputMatchCode, { infoflow: true })
                                                    )
                                                )
                                            )
                                        )
                                    )
                                ),
                                React.createElement(
                                    FisContainerRow,
                                    null,
                                    React.createElement(
                                        FisContainerColumn,
                                        { colSpan: "22" },
                                        React.createElement(FisCheckboxRadioGroup, { colSpan: "22", inputlist: [{ label: "Schedule US Flag Restricted Vessel Only", name: "EScheduleUsFlagOnly_Flag" }], opts: { type: "checkbox", label: "", stacked: true, name: "EScheduleUsFlagOnly_FlagGroup", standalone: true } })
                                    )
                                )
                            )
                        ),
                        React.createElement(
                            FisContainerColumn,
                            { colSpan: "26", className: "flex-box" },
                            React.createElement(
                                "div",
                                null,
                                React.createElement(
                                    FisContainerRow,
                                    null,
                                    React.createElement(
                                        FisContainerColumn,
                                        { colSpan: "26", className: "flex-box" },
                                        React.createElement(
                                            FisFieldset,
                                            { legend: "Equipment", infoflow: true },
                                            React.createElement(
                                                FisContainerRow,
                                                null,
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "20" },
                                                    React.createElement(
                                                        FisContainerRow,
                                                        null,
                                                        React.createElement(
                                                            FisContainerColumn,
                                                            { colSpan: "13" },
                                                            React.createElement(
                                                                FisInputGroup,
                                                                { label: "Depot" },
                                                                React.createElement(
                                                                    FisInputPickFlow,
                                                                    { colSpan: "5" },
                                                                    React.createElement(FisInputBase, { type: "text",
                                                                    colSpan: "4" })
                                                                ),
                                                                React.createElement(
                                                                    FisInputPickFlow,
                                                                    { colSpan: "8" },
                                                                    React.createElement(FisInputMatchCode, { infoflow: true })
                                                                )
                                                            )
                                                        ),
                                                        React.createElement(
                                                            FisContainerColumn,
                                                            { colSpan: "7" },
                                                            React.createElement(FisCheckboxRadioGroup, { colSpan: "7", inputlist: [{ label: "More Depots", readOnly: true, name: "EMoreDepotsExists_Flag" }], opts: { type: "checkbox", label: "", stacked: false, name: "EMoreDepotsExists_FlagGroup", standalone: true } })
                                                        )
                                                    )
                                                )
                                            ),
                                            React.createElement(
                                                FisContainerRow,
                                                null,
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "25" },
                                                    React.createElement(
                                                        FisContainerRow,
                                                        null,
                                                        React.createElement(
                                                            FisContainerColumn,
                                                            { colSpan: "3" },
                                                            React.createElement(
                                                                "div",
                                                                null,
                                                                React.createElement(
                                                                    FisContainerRow,
                                                                    null,
                                                                    React.createElement(
                                                                        FisContainerColumn,
                                                                        { colSpan: "3" },
                                                                        React.createElement(
                                                                            FisInputGroup,
                                                                            { label: "22GP" },
                                                                            React.createElement(FisInputBase, { type: "number",
                                                                            colSpan: "3",
                                                                            infoflow: true })
                                                                        )
                                                                    )
                                                                ),
                                                                React.createElement(
                                                                    FisContainerRow,
                                                                    null,
                                                                    React.createElement(
                                                                        FisContainerColumn,
                                                                        { colSpan: "3" },
                                                                        React.createElement(
                                                                            FisInputGroup,
                                                                            { label: "42GP" },
                                                                            React.createElement(FisInputBase, { type: "number",
                                                                            colSpan: "3",
                                                                            infoflow: true })
                                                                        )
                                                                    )
                                                                ),
                                                                React.createElement(
                                                                    FisContainerRow,
                                                                    null,
                                                                    React.createElement(
                                                                        FisContainerColumn,
                                                                        { colSpan: "3" },
                                                                        React.createElement(
                                                                            FisInputGroup,
                                                                            { label: "45GP" },
                                                                            React.createElement(FisInputBase, { type: "number",
                                                                            colSpan: "3",
                                                                            infoflow: true })
                                                                        )
                                                                    )
                                                                )
                                                            )
                                                        ),
                                                        React.createElement(
                                                            FisContainerColumn,
                                                            { colSpan: "3" },
                                                            React.createElement(
                                                                "div",
                                                                null,
                                                                React.createElement(
                                                                    FisContainerRow,
                                                                    null,
                                                                    React.createElement(
                                                                        FisContainerColumn,
                                                                        { colSpan: "3" },
                                                                        React.createElement(
                                                                            FisInputGroup,
                                                                            { label: "22RT" },
                                                                            React.createElement(FisInputBase, { type: "number",
                                                                            colSpan: "3",
                                                                            infoflow: true })
                                                                        )
                                                                    )
                                                                ),
                                                                React.createElement(
                                                                    FisContainerRow,
                                                                    null,
                                                                    React.createElement(
                                                                        FisContainerColumn,
                                                                        { colSpan: "3" },
                                                                        React.createElement(
                                                                            FisInputGroup,
                                                                            { label: "42UT" },
                                                                            React.createElement(FisInputBase, { type: "number",
                                                                            colSpan: "3",
                                                                            infoflow: true })
                                                                        )
                                                                    )
                                                                ),
                                                                React.createElement(
                                                                    FisContainerRow,
                                                                    null,
                                                                    React.createElement(
                                                                        FisContainerColumn,
                                                                        { colSpan: "3" },
                                                                        React.createElement(
                                                                            FisInputGroup,
                                                                            { label: "45RT" },
                                                                            React.createElement(FisInputBase, { type: "number",
                                                                            colSpan: "3",
                                                                            infoflow: true })
                                                                        )
                                                                    )
                                                                )
                                                            )
                                                        ),
                                                        React.createElement(
                                                            FisContainerColumn,
                                                            { colSpan: "3" },
                                                            React.createElement(
                                                                "div",
                                                                null,
                                                                React.createElement(
                                                                    FisContainerRow,
                                                                    null,
                                                                    React.createElement(
                                                                        FisContainerColumn,
                                                                        { colSpan: "3" },
                                                                        React.createElement(
                                                                            FisInputGroup,
                                                                            { label: "22VH" },
                                                                            React.createElement(FisInputBase, { type: "number",
                                                                            colSpan: "3",
                                                                            infoflow: true })
                                                                        )
                                                                    )
                                                                ),
                                                                React.createElement(
                                                                    FisContainerRow,
                                                                    null,
                                                                    React.createElement(
                                                                        FisContainerColumn,
                                                                        { colSpan: "3" },
                                                                        React.createElement(
                                                                            FisInputGroup,
                                                                            { label: "22UP" },
                                                                            React.createElement(FisInputBase, { type: "number",
                                                                            colSpan: "3",
                                                                            infoflow: true })
                                                                        )
                                                                    )
                                                                ),
                                                                React.createElement(
                                                                    FisContainerRow,
                                                                    null,
                                                                    React.createElement(
                                                                        FisContainerColumn,
                                                                        { colSpan: "3" },
                                                                        React.createElement(
                                                                            FisInputGroup,
                                                                            { label: "42UP" },
                                                                            React.createElement(FisInputBase, { type: "number",
                                                                            colSpan: "3",
                                                                            infoflow: true })
                                                                        )
                                                                    )
                                                                )
                                                            )
                                                        ),
                                                        React.createElement(
                                                            FisContainerColumn,
                                                            { colSpan: "16" },
                                                            React.createElement(
                                                                "div",
                                                                null,
                                                                React.createElement(
                                                                    FisContainerRow,
                                                                    null,
                                                                    React.createElement(
                                                                        FisContainerColumn,
                                                                        { colSpan: "7" },
                                                                        React.createElement(
                                                                            FisContainerRow,
                                                                            null,
                                                                            React.createElement(
                                                                                FisContainerColumn,
                                                                                { colSpan: "7" },
                                                                                React.createElement(
                                                                                    FisInputGroup,
                                                                                    { label: "Others" },
                                                                                    React.createElement(
                                                                                        FisInputPickFlow,
                                                                                        { colSpan: "4" },
                                                                                        React.createElement(FisInputBase, { type: "text",
                                                                                        colSpan: "3" })
                                                                                    ),
                                                                                    React.createElement(FisInputBase, { type: "number",
                                                                                    colSpan: "3",
                                                                                    infoflow: true })
                                                                                )
                                                                            )
                                                                        )
                                                                    )
                                                                ),
                                                                React.createElement(
                                                                    FisContainerRow,
                                                                    null,
                                                                    React.createElement(
                                                                        FisContainerColumn,
                                                                        { colSpan: "16" },
                                                                        React.createElement(
                                                                            FisContainerRow,
                                                                            null,
                                                                            React.createElement(
                                                                                FisContainerColumn,
                                                                                { colSpan: "7" },
                                                                                React.createElement(
                                                                                    FisInputGroup,
                                                                                    { label: "Others" },
                                                                                    React.createElement(
                                                                                        FisInputPickFlow,
                                                                                        { colSpan: "4" },
                                                                                        React.createElement(FisInputBase, { type: "text",
                                                                                        colSpan: "3" })
                                                                                    ),
                                                                                    React.createElement(FisInputBase, { type: "number",
                                                                                    colSpan: "3",
                                                                                    infoflow: true })
                                                                                )
                                                                            ),
                                                                            React.createElement(
                                                                                FisContainerColumn,
                                                                                { colSpan: "9" },
                                                                                React.createElement(FisCheckboxRadioGroup, { colSpan: "9", inputlist: [{ label: "SOW-Container(s)", readOnly: true, name: "ESowContainerFound_Flag" }], opts: { type: "checkbox", label: "", stacked: false, name: "ESowContainerFound_FlagGroup", standalone: true } })
                                                                            )
                                                                        )
                                                                    )
                                                                ),
                                                                React.createElement(
                                                                    FisContainerRow,
                                                                    null,
                                                                    React.createElement(
                                                                        FisContainerColumn,
                                                                        { colSpan: "14" },
                                                                        React.createElement(
                                                                            FisContainerRow,
                                                                            null,
                                                                            React.createElement(
                                                                                FisContainerColumn,
                                                                                { colSpan: "7" },
                                                                                React.createElement(
                                                                                    FisInputGroup,
                                                                                    { label: "Others" },
                                                                                    React.createElement(
                                                                                        FisInputPickFlow,
                                                                                        { colSpan: "4" },
                                                                                        React.createElement(FisInputBase, { type: "text",
                                                                                        colSpan: "3" })
                                                                                    ),
                                                                                    React.createElement(FisInputBase, { type: "number",
                                                                                    colSpan: "3",
                                                                                    infoflow: true })
                                                                                )
                                                                            ),
                                                                            React.createElement(
                                                                                FisContainerColumn,
                                                                                { colSpan: "7" },
                                                                                React.createElement(FisCheckboxRadioGroup, { colSpan: "7", inputlist: [{ label: "Substitution", readOnly: true, name: "ESubstitutedContainerFound_Flag" }], opts: { type: "checkbox", label: "", stacked: false, name: "ESubstitutedContainerFound_FlagGroup", standalone: true } })
                                                                            )
                                                                        )
                                                                    )
                                                                )
                                                            )
                                                        )
                                                    )
                                                )
                                            ),
                                            React.createElement(
                                                FisContainerRow,
                                                null,
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "15" },
                                                    React.createElement(
                                                        FisContainerRow,
                                                        null,
                                                        React.createElement(
                                                            FisContainerColumn,
                                                            { colSpan: "9" },
                                                            React.createElement(
                                                                FisInputGroup,
                                                                { label: "Pos.Date/Time" },
                                                                React.createElement(FisInputDate, { colSpan: "4" }),
                                                                React.createElement(FisInputBase, { type: "text",
                                                                colSpan: "2" }),
                                                                React.createElement(FisInputBase, { type: "time", colSpan: "3" })
                                                            )
                                                        ),
                                                        React.createElement(
                                                            FisContainerColumn,
                                                            { colSpan: "6" },
                                                            React.createElement(FisCheckboxRadioGroup, { colSpan: "6", inputlist: [{ label: "Diff. Pos.", readOnly: true, name: "EDifferentPositioningInfo_Flag" }], opts: { type: "checkbox", label: "", stacked: false, name: "EDifferentPositioningInfo_FlagGroup", standalone: true } })
                                                        )
                                                    )
                                                )
                                            ),
                                            React.createElement(
                                                FisContainerRow,
                                                null,
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "20" },
                                                    React.createElement(
                                                        FisContainerRow,
                                                        null,
                                                        React.createElement(
                                                            FisContainerColumn,
                                                            { colSpan: "9" },
                                                            React.createElement(
                                                                FisInputGroup,
                                                                { label: "Inland Cut-Off  " },
                                                                React.createElement(FisInputDate, { colSpan: "4" }),
                                                                React.createElement(FisInputBase, { type: "time", colSpan: "3" })
                                                            )
                                                        ),
                                                        React.createElement(
                                                            FisContainerColumn,
                                                            { colSpan: "11" },
                                                            React.createElement(
                                                                FisButtonGroup,
                                                                { colSpan: "11" },
                                                                React.createElement(
                                                                    FisButton,
                                                                    { variant: "content-default", colSpan: "11" },
                                                                    "EQ Request Detail"
                                                                )
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    )
                                ),
                                React.createElement(
                                    FisContainerRow,
                                    null,
                                    React.createElement(
                                        FisContainerColumn,
                                        { colSpan: "25", className: "flex-box" },
                                        React.createElement(
                                            FisFieldset,
                                            { legend: "Commodity", infoflow: true },
                                            React.createElement(
                                                FisContainerRow,
                                                null,
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "19" },
                                                    React.createElement(
                                                        FisInputGroup,
                                                        { label: "Short Description" },
                                                        React.createElement(FisInputBase, { type: "text",
                                                        colSpan: "19" })
                                                    )
                                                )
                                            ),
                                            React.createElement(
                                                FisContainerRow,
                                                null,
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "13" },
                                                    React.createElement(
                                                        FisContainerRow,
                                                        null,
                                                        React.createElement(
                                                            FisContainerColumn,
                                                            { colSpan: "7" },
                                                            React.createElement(
                                                                FisInputGroup,
                                                                { label: "HS Code" },
                                                                React.createElement(
                                                                    FisInputPickFlow,
                                                                    { colSpan: "7" },
                                                                    React.createElement(FisInputHarmonizedSystemCode, null)
                                                                )
                                                            )
                                                        ),
                                                        React.createElement(
                                                            FisContainerColumn,
                                                            { colSpan: "6" },
                                                            React.createElement(FisCheckboxRadioGroup, { colSpan: "6", inputlist: [{ label: "More Cargo", readOnly: true, name: "EMoreCargo_Flag" }], opts: { type: "checkbox", label: "", stacked: false, name: "EMoreCargo_FlagGroup", standalone: true } })
                                                        )
                                                    )
                                                )
                                            ),
                                            React.createElement(
                                                FisContainerRow,
                                                null,
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "24" },
                                                    React.createElement(
                                                        FisContainerRow,
                                                        null,
                                                        React.createElement(
                                                            FisContainerColumn,
                                                            { colSpan: "10" },
                                                            React.createElement(
                                                                "div",
                                                                null,
                                                                React.createElement(
                                                                    FisContainerRow,
                                                                    null,
                                                                    React.createElement(
                                                                        FisContainerColumn,
                                                                        { colSpan: "10" },
                                                                        React.createElement(
                                                                            FisContainerRow,
                                                                            null,
                                                                            React.createElement(
                                                                                FisContainerColumn,
                                                                                { colSpan: "10" },
                                                                                React.createElement(
                                                                                    FisInputGroup,
                                                                                    { label: "Total Gross Wt" },
                                                                                    React.createElement(FisInputBase, { type: "number",
                                                                                    colSpan: "7"
                                                                                }),
                                                                                React.createElement(FisInputBase, { type: "text",
                                                                                colSpan: "3" })
                                                                            )
                                                                        )
                                                                    )
                                                                )
                                                            ),
                                                            React.createElement(
                                                                FisContainerRow,
                                                                null,
                                                                React.createElement(
                                                                    FisContainerColumn,
                                                                    { colSpan: "9" },
                                                                    React.createElement(
                                                                        FisContainerRow,
                                                                        null,
                                                                        React.createElement(
                                                                            FisContainerColumn,
                                                                            { colSpan: "9" },
                                                                            React.createElement(
                                                                                FisInputGroup,
                                                                                { label: "Total Gross Vol" },
                                                                                React.createElement(FisInputBase, { type: "number",
                                                                                colSpan: "6"
                                                                            }),
                                                                            React.createElement(FisInputBase, { type: "text",
                                                                            colSpan: "3" })
                                                                        )
                                                                    )
                                                                )
                                                            )
                                                        )
                                                    )
                                                ),
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "6" },
                                                    React.createElement(FisCheckboxRadioGroup, { inputlist: [{ label: "Total" }, { label: "Per Cntr" }], opts: { type: "radio", label: " ", stacked: true, mandatory: false, name: "ETotalSingleWeightVolume_Integer" } })
                                                ),
                                                React.createElement(
                                                    FisContainerColumn,
                                                    { colSpan: "8" },
                                                    React.createElement(
                                                        FisContainerRow,
                                                        null,
                                                        React.createElement(
                                                            FisContainerColumn,
                                                            { colSpan: "4" },
                                                            React.createElement(
                                                                "div",
                                                                null,
                                                                React.createElement(
                                                                    FisContainerRow,
                                                                    null,
                                                                    React.createElement(
                                                                        FisContainerColumn,
                                                                        { colSpan: "4" },
                                                                        React.createElement(FisCheckboxRadioGroup, { colSpan: "4", inputlist: [{ label: "DG", readOnly: true, name: "EShipment_DaDgContained" }, { label: "Temp", readOnly: true, name: "EShipment_DaTempContained" }], opts: { type: "checkbox", label: "", stacked: true, name: "EShipment_DaDgContainedGroup" } })
                                                                    )
                                                                )
                                                            )
                                                        ),
                                                        React.createElement(
                                                            FisContainerColumn,
                                                            { colSpan: "4" },
                                                            React.createElement(
                                                                "div",
                                                                null,
                                                                React.createElement(
                                                                    FisContainerRow,
                                                                    null,
                                                                    React.createElement(
                                                                        FisContainerColumn,
                                                                        { colSpan: "4" },
                                                                        React.createElement(FisCheckboxRadioGroup, { colSpan: "4", inputlist: [{ label: "Vent", readOnly: true, name: "EShipment_DaVentContained" }, { label: "OOG", readOnly: true, name: "EShipment_DaOogContained" }], opts: { type: "checkbox", label: "", stacked: true, name: "EShipment_DaVentContainedGroup" } })
                                                                    )
                                                                )
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
        )
    ),
    React.createElement(
        FisContainerRow,
        null,
        React.createElement(
            FisContainerColumn,
            { colSpan: "45", className: "flex-box" },
            React.createElement(
                FisFieldset,
                { legend: "Customs References" },
                React.createElement(
                    FisContainerRow,
                    null,
                    React.createElement(
                        FisContainerColumn,
                        { colSpan: "31" },
                        React.createElement(
                            "div",
                            null,
                            React.createElement(
                                FisContainerRow,
                                null,
                                React.createElement(
                                    FisContainerColumn,
                                    { colSpan: "31" },
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "6" },
                                            React.createElement(
                                                FisInputGroup,
                                                { label: " " },
                                                React.createElement(FisInputBase, { type: "text",
                                                colSpan: "6" })
                                            )
                                        ),
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "4" },
                                            React.createElement(
                                                FisInputGroup,
                                                { label: "US-AES" },
                                                React.createElement(FisInputBase, { type: "text",
                                                colSpan: "4" })
                                            )
                                        ),
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "10" },
                                            React.createElement(FisCheckboxRadioGroup, { colSpan: "10", inputlist: [{ label: "More Customs Info", readOnly: true, name: "EMoreCustomsInfos_Flag" }], opts: { type: "checkbox", label: "", stacked: false, name: "EMoreCustomsInfos_FlagGroup", standalone: true } })
                                        ),
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "11" },
                                            React.createElement(FisCheckboxRadioGroup, { inputlist: [{ label: "Cntr" }, { label: "Cargo" }, { label: "Shipment" }], opts: { type: "radio", label: " ", stacked: false, mandatory: false, name: "ECustomsLevel_Integer" } })
                                        )
                                    )
                                )
                            ),
                            React.createElement(
                                FisContainerRow,
                                null,
                                React.createElement(
                                    FisContainerColumn,
                                    { colSpan: "26" },
                                    React.createElement(
                                        FisContainerRow,
                                        null,
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "19" },
                                            React.createElement(
                                                FisInputGroup,
                                                { label: " " },
                                                React.createElement(FisInputBase, { type: "text",
                                                colSpan: "19" })
                                            )
                                        ),
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "3" },
                                            React.createElement(
                                                FisInputGroup,
                                                { label: "Part No" },
                                                React.createElement(FisInputBase, { type: "text",
                                                colSpan: "3" })
                                            )
                                        ),
                                        React.createElement(
                                            FisContainerColumn,
                                            { colSpan: "4" },
                                            React.createElement(
                                                FisInputGroup,
                                                { label: "SCAC" },
                                                React.createElement(FisInputBase, { type: "text",
                                                colSpan: "4" })
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    React.createElement(
                        FisContainerColumn,
                        { colSpan: "2" },
                        React.createElement(
                            "div",
                            null,
                            React.createElement(
                                FisContainerRow,
                                null,
                                React.createElement(
                                    FisContainerColumn,
                                    { colSpan: "2" },
                                    React.createElement(FisButtonIcon, { icon: "fis-icon fis-icon-arrow-up" })
                                )
                            ),
                            React.createElement(
                                FisContainerRow,
                                null,
                                React.createElement(
                                    FisContainerColumn,
                                    { colSpan: "2" },
                                    React.createElement(FisButtonIcon, { icon: "fis-icon fis-icon-arrow-down" })
                                )
                            )
                        )
                    ),
                    React.createElement(
                        FisContainerColumn,
                        { colSpan: "11" },
                        React.createElement(
                            "div",
                            null,
                            React.createElement(
                                FisContainerRow,
                                null,
                                React.createElement(
                                    FisContainerColumn,
                                    { colSpan: "11" },
                                    React.createElement(
                                        FisInputGroup,
                                        { label: "CR" },
                                        React.createElement(FisTextArea, { rows: 4,
                                            colSpan: "11",
                                            readOnly: true })
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
        ),
        React.createElement(
            FisContainerRow,
            null,
            React.createElement(
                FisContainerColumn,
                { colSpan: "35", className: "flex-box" },
                React.createElement(
                    FisFieldset,
                    { legend: "External References" },
                    React.createElement(
                        FisContainerRow,
                        null,
                        React.createElement(
                            FisContainerColumn,
                            { colSpan: "32" },
                            React.createElement(
                                "div",
                                null,
                                React.createElement(
                                    FisContainerRow,
                                    null,
                                    React.createElement(
                                        FisContainerColumn,
                                        { colSpan: "32" },
                                        React.createElement(
                                            FisContainerRow,
                                            null,
                                            React.createElement(
                                                FisContainerColumn,
                                                { colSpan: "4" },
                                                React.createElement(
                                                    FisInputGroup,
                                                    { label: " " },
                                                    React.createElement(FisInputBase, { type: "text",
                                                    colSpan: "4" })
                                                )
                                            ),
                                            React.createElement(
                                                FisContainerColumn,
                                                { colSpan: "9" },
                                                React.createElement(FisCheckboxRadioGroup, { colSpan: "9", inputlist: [{ label: "More References", readOnly: true, name: "EMoreDiffReferenceValues_Flag" }], opts: { type: "checkbox", label: "", stacked: false, name: "EMoreDiffReferenceValues_FlagGroup", standalone: true } })
                                            ),
                                            React.createElement(
                                                FisContainerColumn,
                                                { colSpan: "19" },
                                                React.createElement(FisCheckboxRadioGroup, { inputlist: [{ label: "Shipment" }, { label: "Container" }, { label: "Cargo" }, { label: "Cargo Item" }], opts: { type: "radio", label: " ", stacked: false, mandatory: false, name: "EReferenceLevel_Integer" } })
                                            )
                                        )
                                    )
                                ),
                                React.createElement(
                                    FisContainerRow,
                                    null,
                                    React.createElement(
                                        FisContainerColumn,
                                        { colSpan: "19" },
                                        React.createElement(
                                            FisContainerRow,
                                            null,
                                            React.createElement(
                                                FisContainerColumn,
                                                { colSpan: "19" },
                                                React.createElement(
                                                    FisInputGroup,
                                                    { label: " " },
                                                    React.createElement(FisInputBase, { type: "text",
                                                    colSpan: "19" })
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        ),
                        React.createElement(
                            FisContainerColumn,
                            { colSpan: "2" },
                            React.createElement(
                                "div",
                                null,
                                React.createElement(
                                    FisContainerRow,
                                    null,
                                    React.createElement(
                                        FisContainerColumn,
                                        { colSpan: "2" },
                                        React.createElement(FisButtonIcon, { icon: "fis-icon fis-icon-arrow-up" })
                                    )
                                ),
                                React.createElement(
                                    FisContainerRow,
                                    null,
                                    React.createElement(
                                        FisContainerColumn,
                                        { colSpan: "2" },
                                        React.createElement(FisButtonIcon, { icon: "fis-icon fis-icon-arrow-down" })
                                    )
                                )
                            )
                        )
                    )
                )
            )
        ),
        React.createElement(
            FisContainerRow,
            null,
            React.createElement(
                FisContainerColumn,
                { colSpan: "47", className: "flex-box" },
                React.createElement(
                    FisFieldset,
                    { legend: "Remarks", infoflow: true },
                    React.createElement(
                        FisContainerRow,
                        null,
                        React.createElement(
                            FisContainerColumn,
                            { colSpan: "46" },
                            React.createElement(
                                FisContainerRow,
                                null,
                                React.createElement(
                                    FisContainerColumn,
                                    { colSpan: "8" },
                                    React.createElement(
                                        "div",
                                        null,
                                        React.createElement(
                                            FisContainerRow,
                                            null,
                                            React.createElement(
                                                FisContainerColumn,
                                                { colSpan: "3" },
                                                React.createElement(
                                                    FisInputGroup,
                                                    { label: "Type" },
                                                    React.createElement(FisInputBase, { type: "text",
                                                    colSpan: "3" })
                                                )
                                            )
                                        ),
                                        React.createElement(
                                            FisContainerRow,
                                            null,
                                            React.createElement(
                                                FisContainerColumn,
                                                { colSpan: "8" },
                                                React.createElement(FisCheckboxRadioGroup, { colSpan: "8", inputlist: [{ label: "More Remarks", readOnly: true, name: "EMoreRemarks_Flag" }], opts: { type: "checkbox", label: "", stacked: true, name: "EMoreRemarks_FlagGroup", standalone: true } })
                                            )
                                        )
                                    )
                                ),
                                React.createElement(
                                    FisContainerColumn,
                                    { colSpan: "36" },
                                    React.createElement(
                                        FisInputGroup,
                                        { label: "Text" },
                                        React.createElement(FisTextArea, { rows: 4,
                                            colSpan: "36"
                                        })
                                    )
                                ),
                                React.createElement(
                                    FisContainerColumn,
                                    { colSpan: "2" },
                                    React.createElement(
                                        "div",
                                        null,
                                        React.createElement(
                                            FisContainerRow,
                                            null,
                                            React.createElement(
                                                FisContainerColumn,
                                                { colSpan: "2" },
                                                React.createElement(FisButtonIcon, { icon: "fis-icon fis-icon-arrow-up" })
                                            )
                                        ),
                                        React.createElement(
                                            FisContainerRow,
                                            null,
                                            React.createElement(
                                                FisContainerColumn,
                                                { colSpan: "2" },
                                                React.createElement(FisButtonIcon, { icon: "fis-icon fis-icon-arrow-down" })
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
        )
    )
)
)
);
}
