import { OPEN_NEW_APP, CLOSE_APP, SET_CURRENT_APP } from '../actionTypes';
import { guid } from '../../components/helpers/util';

let currentId = null;
let arrByID = null;
const dashboardID = guid();
const initial = {
    currentApp: {
        id: dashboardID
    },
    apps: [
        {
            id: dashboardID,
            app: { name: 'Dashboard', icon: 'fis-icon-glyphicons-home', url: '/app/dashboard' }, noClose: true, className: 'homeBtn' }
    ] };
export default function openAppsList(
    state = initial, action) {
    switch (action.type) {
        case OPEN_NEW_APP:
            // console.log('openAppsList reducer OPEN_NEW_APP:', action);
            currentId = guid();
            return {
                ...state,
                currentApp: { id: currentId },
                apps: [
                    ...state.apps,
                    {
                        id: currentId,
                        app: { name: action.payload.app.name, url: action.payload.app.url }
                    }
                ]
            };
        case SET_CURRENT_APP:
            console.log('openAppsList reducer SET_CURRENT_APP:', action);
            return {
                ...state,
                currentApp: { id: action.payload.app.id }
            };
        case CLOSE_APP:
            console.log('openAppsList reducer CLOSE_APP:', action);
            // closeable app.id
            currentId = action.payload.app.id;
            console.log('openAppsList reducer CLOSE_APP currentId:', currentId);
            // delete (via filter) obj from applist where id == closeable app.id
            arrByID = state.apps.filter((obj) => {
                // console.log('util filterOpen', obj, currentId);
                let val;
                if ('id' in obj && obj.id !== currentId) {
                    val = true;
                } else {
                    val = false;
                }
                return val;
            });

            return {
                ...state,
                apps: arrByID
            };
        default:
            return state;
    }
}
