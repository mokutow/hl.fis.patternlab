import { LOGIN_USER_LOADING } from '../actionTypes';

export default function globalloading(state = false, action) {
    switch (action.type) {
        case LOGIN_USER_LOADING:
            return true;
        default:
            return state;
    }
}
