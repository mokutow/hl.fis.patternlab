import { LOGIN_USER_SUCCESS, LOGIN_USER_ERROR } from '../actionTypes';

export default function user(state = null, action) {
    switch (action.type) {
        case LOGIN_USER_SUCCESS:
            return action.payload.body;
        case LOGIN_USER_ERROR:
            return null;
        default:
            return state;
    }
}
