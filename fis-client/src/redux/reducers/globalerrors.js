import { LOGIN_USER_ERROR } from '../actionTypes';

export default function user(state = {}, action) {
    switch (action.type) {
        case LOGIN_USER_ERROR:
            return action.payload;
        default:
            return state;
    }
}
