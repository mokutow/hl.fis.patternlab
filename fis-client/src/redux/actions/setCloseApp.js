export default function setCloseApp(app) {
    console.log('action SET_CLOSE_APP was called', app);
    return {
        type: 'CLOSE_APP',
        payload: {
            app
        }
    };
}
