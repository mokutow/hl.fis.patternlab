// in contextual and alphabetic order please...
export const CLOSE_APP = 'CLOSE_APP';
export const OPEN_NEW_APP = 'OPEN_NEW_APP';
export const SET_CURRENT_APP = 'SET_CURRENT_APP';

export const LOGIN_USER_ERROR = 'LOGIN_USER_ERROR';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_LOADING = 'LOGIN_USER_LOADING';

export const LOGOUT_USER = 'LOGOUT_USER';
