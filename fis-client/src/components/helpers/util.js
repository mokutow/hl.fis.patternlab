import { push } from 'react-router-redux';
import setOpenNewApp from '../../redux/actions/openApps';
import setCurrentApp from '../../redux/actions/setCurrentApp';
import setCloseApp from '../../redux/actions/setCloseApp';


/**
 * Created by koenig on 15.03.16.
 */
export function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

export function findIndexByKeyValue(arraytosearch, key, valuetosearch) {
    for (let i = 0; i < arraytosearch.length; i++) {
        console.log('findIndexByKeyValue', arraytosearch, key, valuetosearch);
        if (arraytosearch[i][key] === valuetosearch) {
            return i;
        }
    }
    return null;
}


export function handleOpenApp(dispatch, app, callback, openAppList) {
    // console.log('util openApp', app, openAppList);
    // ToDo: other cool stuff like saving current app state, whatever you do next.

    let newApp = null;
    let indexInOpenApplist = null;
    switch (callback) {
        case setOpenNewApp:
            console.log('util setOpenNewApp', app);

            // copy current app
            newApp = app;
            // set new path
            newApp.url = '/app/' + app.url;

            // dispatch + push
            dispatch(callback(newApp));
            dispatch(push(newApp.url));
            break;
        case setCurrentApp:
            console.log('util setCurrentApp', app);
            // dispatch + push
            dispatch(callback(app));
            dispatch(push(app.url));
            break;
        case setCloseApp:
            // remove app from redux state store openapplist
            dispatch(setCloseApp(app));
            // ToDo: find right app to be openend
            // check if removed app == current or last app and  then open app that has index -1 / dashboard if last app
            if (app.id === openAppList.currentApp.id) {
                // find the index of app.id in openAppList

                indexInOpenApplist = findIndexByKeyValue(openAppList.apps, 'id', app.id);
                console.log('util setCloseApp', indexInOpenApplist, openAppList.apps[indexInOpenApplist - 1]);
                newApp = openAppList.apps[indexInOpenApplist - 1];
                newApp = { id: newApp.id, url: newApp.app.url };
                handleOpenApp(dispatch, newApp, setCurrentApp);
            }
            // else if do nothing, because no other app has to be openend
            break;
        case 'redirectAferterLogin':
            dispatch(push(app.url));
            break;
        default:
            // do nothing
            break;
    }
}

export { guid, handleOpenApp };
