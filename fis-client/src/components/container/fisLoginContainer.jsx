import { connect } from 'react-redux';
import loginUser from '../../redux/actions/user';


import FisLogin from '../../../../fis-ui-components/components/03-organisms/fis-login/index.jsx';

const mapStateToProps = (state) => {
    return {
        loading: state.globalloading,
        error: state.globalerrors
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        loginUser: (loginData) => {
            dispatch(loginUser(loginData));
        },
    };
};

const FisLoginContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(FisLogin);

export default FisLoginContainer;
