import { connect } from 'react-redux';
import { handleOpenApp } from '../../components/helpers/util';
import setOpenNewApp from '../../redux/actions/openApps';
import setCurrentApp from '../../redux/actions/setCurrentApp';
import setCloseApp from '../../redux/actions/setCloseApp';
import FisClient from '../../../../fis-ui-components/components/03-organisms/fis-client/index.jsx';


const mapStateToProps = (state) => {
    // console.log('Container:', state.user);
    return {
        user: (state.user && state.user.userInfo) ? state.user.userInfo : null,
        openAppList: state.openAppsList,
        applicationMenu: (state.user && state.user.applicationMenu) ? state.user.applicationMenu : null,
        // TODO: remove dummy fav-Menu and use real one
        favouriteMenu: (state.user && state.user.favouriteMenu) ? state.user.favouriteMenu : null,
        loading: state.globalloading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setOpenNewApp: (app) => {
            handleOpenApp(dispatch, app, setOpenNewApp);
        },
        setCurrentApp: (app) => {
            handleOpenApp(dispatch, app, setCurrentApp);
        },
        setCloseApp: (app, openAppList) => {
            handleOpenApp(dispatch, app, setCloseApp, openAppList);
        },
        redirectAferterLogin: (app) => {
            handleOpenApp(dispatch, app, 'redirectAferterLogin');
        }
    };
};

const FisClientContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(FisClient);

export default FisClientContainer;
