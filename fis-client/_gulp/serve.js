/* eslint-disable */
var fs = require("fs"),
path = require("path"),
    url = require("url");



const proxyMiddleware = require('http-proxy-middleware');

module.exports = (context) => {
    return () => {
        const proxy = proxyMiddleware('http://159.122.97.83:9080/fis/api/auth/login');
        // The default file if the file/path is not found
        var defaultFile = "index.html"

        // I had to resolve to the previous folder, because this task lives inside a ./tasks folder
        // If that's not your case, just use `__dirname`
        var folder = path.resolve(__dirname, "../build");


        context.browserSync.init({
            debug: true,
            notify: false,
            open: false,
            ui: false,
            server: {
                baseDir: context.fisclient.target,
                middleware: [proxy, (req, res, next) => {
                    var fileName = url.parse(req.url);
                    fileName = fileName.href.split(fileName.search).join("");
                    const fileExists = fs.existsSync(folder + fileName);
                    if (!fileExists && fileName.indexOf("browser-sync-client") < 0) {
                        req.url = '/' + defaultFile;
                    }
                    return next();
                }]
            }
        });

        context.gulp.watch(
            [
                context.fisclient.src + '/src/**/*.jsx',
                context.fisclient.src + '/src/**/*.js',
                context.fisuicomponents.src + '/components/**/*.jsx',
                context.fisuicomponents.src + '/components/**/*.js'
            ],
            ['fis:client:js:build']);
    };
};
