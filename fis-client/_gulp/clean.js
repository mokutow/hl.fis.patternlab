const del = require('del');

module.exports = (context) => {
    return () => {
        // The glob pattern ** matches all children and the parent.
        del.sync([`${context.fisclient.target}/**`, `!${context.fisclient.target}`], { force: true });
    };
};
