module.exports = (context) => {
    return () => {
        return context.gulp.src([
            `${context.fisclient.src}/src/index.html`,
            `${context.fisclient.src}/src/fis-apps/**/*`,
            `${context.fisuicomponents.target}/**/*`])
            .pipe(context.gulp.dest(context.fisclient.target));
    };
};
