module.exports = (context) => {
    return () => {
        const dir = context.fisclient.src;
        return context.gulp.src([`${dir}/**/*.js`, `${dir}/**/*.jsx`])
            .pipe(context.gulpPlugins.cached('fis:client:js:lint'))
            .pipe(context.gulpPlugins.eslint())
            .pipe(context.gulpPlugins.eslint.format())
            .pipe(context.gulpPlugins.if(!context.browserSync.active, context.gulpPlugins.eslint.failAfterError()));
    };
};
