module.exports = (context) => {
    return () => {
        const FIS_GUI_ASSETS_GLOB = [`${context.fisuicomponents.src}/assets/**/*`/* , '!' + FIS_GUI_PATH + '/assets/svg/{raw,raw/**}'*/];
        return context.gulp.src(FIS_GUI_ASSETS_GLOB)
            .pipe(context.gulp.dest(`${context.fisuicomponents.target}/assets`));
    };
};
