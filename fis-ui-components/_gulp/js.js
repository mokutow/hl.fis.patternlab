const path = require('path');
const named = require('vinyl-named');
const webpack = require('webpack-stream');

module.exports = (context) => {
    return () => {
        const WEBPACK_OPTIONS = {
            output: {
                filename: 'fisui.js',
                library: 'Fis',
                libraryTarget: 'umd',
                umdNamedDefine: true
            },
            resolve: {
                root: [
                    path.resolve(`${context.fisuicomponents.src}/components`),
                    path.resolve(`${context.fisuicomponents.src}/components/00-globals`),
                    path.resolve(`${context.fisuicomponents.src}/components/01-atoms`),
                    path.resolve(`${context.fisuicomponents.src}/components/02-molecules`),
                    path.resolve(`${context.fisuicomponents.src}/components/03-organisms`),
                    path.resolve(`${context.fisuicomponents.src}/components/04-business`)
                ],
                extensions: ['', '.js', '.jsx']
            },
            module: {
                loaders: [
                    {
                        test: /\.jsx?$/,
                        loaders: ['babel'],
                        exclude: /node_modules/
                    },
                    { test: require.resolve('react'), loader: 'expose?React' },
                    { test: require.resolve('react-dom'), loader: 'expose?ReactDOM' }
                ]
            },
            devtool: 'cheap-module-source-map'
        };

        context.gulp.src(`${context.fisuicomponents.src}/components/index.js`)
            .pipe(named())
            .pipe(webpack(WEBPACK_OPTIONS))
            .on('error', context.gulpNotify.onError('<%= error.message %>'))
            .pipe(context.gulp.dest(`${context.fisuicomponents.target}/js`));
    };
};
