module.exports = (context) => {
    return () => {
        const svgGlob = 'assets/svg/raw/**/*.svg';
        return context.gulp.src(svgGlob, { cwd: context.fisuicomponents.src })
        .pipe(context.gulpPlugins.svgSprite({
            mode: {
                css: {
                    spacing: {
                        padding: 5
                    },
                    dest: './',
                    layout: 'diagonal',
                    sprite: `../${context.fisuicomponents.target}/assets/svg/build/sprite.svg`,
                    bust: false,
                    render: {
                        scss: {
                            dest: 'components/00-globals/_svgsprite.scss',
                            template: `${context.fisuicomponents.src}/components/00-globals/svgsprite-template.scss`
                        }
                    }
                }
            }
        }))
        .on('error', context.gulpNotify.onError('<%= error.message %>'))
        .pipe(context.gulp.dest(context.fisuicomponents.src));
    };
};
