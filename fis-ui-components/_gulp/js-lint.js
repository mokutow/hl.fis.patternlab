module.exports = (context) => {
    return () => {
        const dir = context.fisuicomponents.src;
        return context.gulp.src([`${dir}/components/**/*.js`, `${dir}/components/**/*.jsx`])
            .pipe(context.gulpPlugins.cached('fis:ui:js:lint'))
            .pipe(context.gulpPlugins.eslint())
            .pipe(context.gulpPlugins.eslint.format())
            .pipe(context.gulpPlugins.if(!context.browserSync.active, context.gulpPlugins.eslint.failAfterError()));
    };
};
