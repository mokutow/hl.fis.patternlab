const del = require('del');

module.exports = (context) => {
    return () => {
        // The glob pattern ** matches all children and the parent
        del.sync([`${context.fisuicomponents.target}/**`, `!${context.fisuicomponents.target}`], { force: true });
        del.sync([`${context.fisuicomponents.src}/assets/svg/build/**`], { force: true });
    };
};
