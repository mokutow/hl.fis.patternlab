import React, { Component } from 'react';

import FisInputBase from 'fis-input-base';
import FisContainerRow from 'fis-container-row';
import FisContainerColumn from 'fis-container-column';

export default class FisInputSalesHierarchy extends Component {
    static defaultProps = {
        colSpan: 9
    };

    render() {
        return (
            <FisContainerRow>
                <FisContainerColumn colSpan="2">
                    <FisInputBase type="text" colSpan="2" />
                </FisContainerColumn>
                <FisContainerColumn colSpan="2">
                    <FisInputBase type="text" colSpan="2" />
                </FisContainerColumn>
                <FisContainerColumn colSpan="2">
                    <FisInputBase type="text" colSpan="2" />
                </FisContainerColumn>
                <FisContainerColumn colSpan="3">
                    <FisInputBase type="text" colSpan="3" />
                </FisContainerColumn>
            </FisContainerRow>
        );
    }
}
