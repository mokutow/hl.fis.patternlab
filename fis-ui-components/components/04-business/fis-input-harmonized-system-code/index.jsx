import React, { Component } from 'react';

import FisInputBase from 'fis-input-base';
import FisContainerRow from 'fis-container-row';
import FisContainerColumn from 'fis-container-column';

export default class FisInputHarmonizedSystemCode extends Component {
    static defaultProps = {
        colSpan: 6
    };

    render() {
        return (
            <FisContainerRow>
                <FisContainerColumn colSpan="2">
                    <FisInputBase type="number" colSpan="2" />
                </FisContainerColumn>
                <FisContainerColumn colSpan="2">
                    <FisInputBase type="number" colSpan="2" />
                </FisContainerColumn>
                <FisContainerColumn colSpan="2">
                    <FisInputBase type="number" colSpan="2" />
                </FisContainerColumn>
            </FisContainerRow>
        );
    }
}
