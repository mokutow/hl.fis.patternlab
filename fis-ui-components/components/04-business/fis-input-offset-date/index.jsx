import React, { Component } from 'react';

import FisInputBase from 'fis-input-base';
import FisInputDate from 'fis-input-date';
import FisContainerRow from 'fis-container-row';
import FisContainerColumn from 'fis-container-column';

export default class FisInputOffsetDate extends Component {
    static defaultProps = {
        colSpan: 7
    };

    render() {
        return (
            <FisContainerRow>
                <FisContainerColumn colSpan="4">
                    <FisInputDate colSpan="4" />
                </FisContainerColumn>
                <FisContainerColumn colSpan="3">
                    <FisInputBase type="number" colSpan="3" />
                </FisContainerColumn>
            </FisContainerRow>
        );
    }
}
