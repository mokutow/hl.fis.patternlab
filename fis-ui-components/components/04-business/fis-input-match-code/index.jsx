import React, { Component } from 'react';

import FisInputBase from 'fis-input-base';
import FisContainerRow from 'fis-container-row';
import FisContainerColumn from 'fis-container-column';

export default class FisInputMatchCode extends Component {
    static defaultProps = {
        colSpan: 7
    };

    render() {
        return (
            <FisContainerRow>
                <FisContainerColumn colSpan="4">
                    <FisInputBase type="text" colSpan="4" />
                </FisContainerColumn>
                <FisContainerColumn colSpan="3">
                    <FisInputBase type="number" colSpan="3" />
                </FisContainerColumn>
            </FisContainerRow>
        );
    }
}
