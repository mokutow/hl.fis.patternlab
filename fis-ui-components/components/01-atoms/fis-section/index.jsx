import React, { Component } from 'react';
import classnames from 'classnames';

export default class FisSection extends Component {

    static propTypes = {
        className: React.PropTypes.string,
        children: React.PropTypes.oneOfType([React.PropTypes.arrayOf(React.PropTypes.node), React.PropTypes.node])
    };

    render() {
        const classes = classnames('fisSection', this.props.className);
        return (
            <section className={classes}>
                {this.props.children}
            </section>
        );
    }
}
