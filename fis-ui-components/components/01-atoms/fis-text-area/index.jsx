import React from 'react';
import FisInputBase from 'fis-input-base';

export default class FisTextArea extends FisInputBase {
    static defaultProps = {
        rows: 3,
        tag: 'textarea'
    };

    render() {
        return (<FisInputBase {...this.props} />);
    }
}
