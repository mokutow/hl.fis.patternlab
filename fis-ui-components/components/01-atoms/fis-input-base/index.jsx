import React, { Component } from 'react';
import classnames from 'classnames';
import FisButtonIcon from 'fis-button-icon';
import FisIcon from 'fis-icon';

export default class FisInputBase extends Component {

    static propTypes = {
        className: React.PropTypes.string,
        tag: React.PropTypes.string,
        defaultValue: React.PropTypes.oneOfType(
            [
                React.PropTypes.string,
                React.PropTypes.number
            ]),
        disabled: React.PropTypes.bool,
        checked: React.PropTypes.bool,
        isInvalid: React.PropTypes.bool,
        id: React.PropTypes.string,
        errormessage: React.PropTypes.string,
        mandatory: React.PropTypes.bool,
        placeholder: React.PropTypes.string,
        readOnly: React.PropTypes.bool,
        type: React.PropTypes.oneOf(['text', 'number', 'email', 'password', 'search', 'tel', 'url', 'radio', 'checkbox', 'time']),
        focusError: React.PropTypes.func,
        hideError: React.PropTypes.func,
        mouseOverError: React.PropTypes.func,
        mouseOutError: React.PropTypes.func,
        handleChange: React.PropTypes.func,
        prefix: React.PropTypes.string,
        suffix: React.PropTypes.string,
        suffixIcon: React.PropTypes.string,
        tooltip: React.PropTypes.string,
        tooltipPlace: React.PropTypes.string,
        tooltipFor: React.PropTypes.string,
        name: React.PropTypes.string,
        infoflow: React.PropTypes.oneOfType(
            [
                React.PropTypes.string,
                React.PropTypes.bool
            ])
    };

    static defaultProps = {
        type: 'text',
        tag: 'input',
        tooltipFor: 'fis-tooltip',
        handleChange: () => {}
    };

    state = {
        focused: '',
        value: ''
    };

    handleChange = () => {
        // this.setState({ value: event });
        const element = this.refs['fisInputBase-' + this.props.name];
        console.log('fis-input-base handleChange: ', this.props.name);
        this.props.handleChange(this.props.name, element.value);
    };

    handleFocus = () => {
        if (this.props.errormessage) {
            this.props.focusError(this.props);
        }
    };

    handleBlur = () => {
        this.props.hideError(this.props);
        this.handleChange();
    };

    handleMouseOver = () => {
        if (this.props.errormessage) {
            this.props.mouseOverError(this.props);
        }
    };

    handleMouseOut = () => {
        if (this.props.mouseOutError) {
            this.props.mouseOutError(this.props);
        }
    };

    render() {
        const InputComponent = this.props.tag;
        const props = this.props;
        const opts = {};
        opts.type = this.props.type;

        const classes = classnames('input-base', {
            'info-flow': this.props.infoflow,
            'prefixed': this.props.prefix,
            'suffixed': this.props.suffix,
            'is-invalid': this.props.errormessage || this.props.isInvalid,
            [`has-${this.props.tag}`]: this.props.tag,
            [`has-type-${this.props.type}`]: this.props.type

        });

        opts.className = classnames(this.props.className, {
            'is-invalid-input': this.props.errormessage || this.props.isInvalid
        });

        const iconClass = classnames('suffix-icon', this.props.suffixIcon);


        if (this.props.checked) {
            opts.defaultChecked = 'checked';
        }
        if (this.props.disabled) {
            opts.disabled = 'disabled';
        }
        if (this.props.readOnly) {
            opts.readOnly = 'readOnly';
        }
        if (this.props.defaultValue) {
            opts.defaultValue = this.props.defaultValue;
        }
        if (this.props.placeholder) {
            opts.placeholder = this.props.placeholder;
        }
        if (this.props.name) {
            opts.name = this.props.name;
        }
        if (this.props.id) {
            opts.id = this.props.id;
        }
        if (this.props.mandatory) {
            opts.required = 'required';
        }


        return (
            <span className={classes}>
                <span className="prefix" display-if={this.props.prefix}>{this.props.prefix}</span>
                <span className="input-group-field">
                    <InputComponent {...props} {...opts}
                        onFocus={ this.handleFocus}
                        onMouseOver={ this.handleMouseOver}
                        onMouseOut={ this.handleMouseOut}
                        onBlur={ this.handleBlur }
                        data-tip={this.props.tooltip}
                        data-for={this.props.tooltipFor}
                        data-place={this.props.tooltipPlace}
                        ref={'fisInputBase-' + this.props.name}
                    />
                    <i
                        className="fis-icon radio-checkbox-icon"
                        display-if={this.props.type === 'radio' || this.props.type === 'checkbox'}
                    />
                    <div
                        className="input-group-button"
                        display-if={this.props.infoflow && this.props.type !== 'radio' && this.props.type !== 'checkbox'}
                    >
                        <FisButtonIcon tooltip="open Info-Flow" icon="fis-icon-infoflow" />
                    </div>
                </span>
                <span className="suffix" display-if={this.props.suffix}>{this.props.suffix}</span>
                <FisIcon icon={iconClass} display-if={this.props.suffixIcon} />
            </span>
        );
    }
}
