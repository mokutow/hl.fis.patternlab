import React, { Component } from 'react';
import classnames from 'classnames';

export default class FisText extends Component {

    static propTypes = {
        className: React.PropTypes.string,
        children: React.PropTypes.string,
        standalone: React.PropTypes.bool
    };

    render() {
        const classes = classnames('fis-text', this.props.className, {
            'standalone': this.props.standalone
        });

        return (
            <p className={classes}>
                {this.props.children}
            </p>
        );
    }
}
