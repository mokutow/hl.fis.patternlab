import React, { Component } from 'react';
import FisContainerColumn from 'fis-container-column';
import classnames from 'classnames';

export default class FisContainerRow extends Component {

    static propTypes = {
        className: React.PropTypes.string,
        style: React.PropTypes.object,
        children: React.PropTypes.oneOfType([

            React.PropTypes.shape({
                type: React.PropTypes.oneOf([FisContainerColumn])
            }),
            React.PropTypes.arrayOf(
                React.PropTypes.shape({
                    type: React.PropTypes.oneOf([FisContainerColumn])
                })
            )
        ])
    };

    render() {
        const classes = classnames('fis-grid-row', this.props.className);
        return (
            <div className={classes} style={this.props.style}>
                {this.props.children}
            </div>
        );
    }
}
