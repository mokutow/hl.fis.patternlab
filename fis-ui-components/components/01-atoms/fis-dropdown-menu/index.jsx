import React, { Component } from 'react';
import Menu, { SubMenu, MenuItem, Divider } from 'rc-menu';
import classnames from 'classnames';
import FisIcon from 'fis-icon';
import FisButtonIcon from 'fis-button-icon';
import FisButton from 'fis-button';

export default class FisDropDownMenu extends Component {

    static propTypes = {
        className: React.PropTypes.string,
        buttontext: React.PropTypes.string,
        icon: React.PropTypes.string,
        tooltip: React.PropTypes.string,
        tooltipPlace: React.PropTypes.string,
        id: React.PropTypes.string,
        list: React.PropTypes.array,
        openKeys: React.PropTypes.array,
        handleClick: React.PropTypes.func
    };

    state = {
        openKeys: [] // open Dropdowns
    };

    onHandleOnclick = (event) => {
        event.preventDefault();
        const item = event.currentTarget.dataset;
        const app = { name: item.name, url: item.url };
        this.props.handleClick(app);
    }

    emptyOpenKeys = () => {
        this.setState({
            openKeys: []
        });
    }

    syncOpenKeys = (e) => {
        this.setState({
            openKeys: e.openKeys
        });
    }

    renderSubMenus = (listItem, i, parentKey) => {
        return (
            <SubMenu
                className={listItem.className}
                title={listItem.name}
                key={`SubMenu-${parentKey}-${i}`}
            >
                {this.renderMenus(listItem.sub, i)}
            </SubMenu>
        );
    }
    renderMenuItems = (listItem, i, parentKey) => {
        return (
            <MenuItem
                title={listItem.name}
                className={listItem.className}
                key={`MenuItem-${parentKey}-${i}`}
            >
                <a onClick={this.onHandleOnclick} data-url={listItem.url} data-name={listItem.name} href="#">
                    {listItem.name}
                </a>
            </MenuItem>
        );
    }

    renderMenus = (list, parentKey) => {
        return (
            list.map((listItem, i) => {
                let item;
                if (listItem.divider) {
                    item = <Divider />;
                } else {
                    if (!listItem.sub || listItem.sub === null) {
                        item = this.renderMenuItems(listItem, i, parentKey);
                    } else {
                        item = this.renderSubMenus(listItem, i, parentKey);
                    }
                }

                return item;
            })
        );
    }

    render() {
        const menuClass = classnames('fis-dropdown-menu', {
            'icon-menu': this.props.icon
        });

        return (

            <Menu
                className={menuClass}
                onOpen={this.syncOpenKeys}
                onClose={this.syncOpenKeys}
                openKeys={this.state.openKeys}
                onClick={this.emptyOpenKeys}
                openSubMenuOnMouseEnter={false}
                closeSubMenuOnMouseLeave
                mode="horizontal"
                key={this.props.id}
            >
                <SubMenu className="first" title={<div>
                <FisButtonIcon
                    display-if={this.props.icon}
                    icon={this.props.icon}
                    tooltip={this.props.tooltip}
                    tooltipPlace={this.props.tooltipPlace}
                >
                    <FisIcon icon="fis-icon-arrow-down" />
                </FisButtonIcon>
                <FisButton
                    display-if={this.props.buttontext}
                >
                    {this.props.buttontext}
                </FisButton></div>}
                >
                    {this.renderMenus(this.props.list, this.props.id)}
                </SubMenu>
            </Menu>
        );
    }
}
