import React, { Component } from 'react';
import classnames from 'classnames';

export default class FisButton extends Component {

    static propTypes = {
        children: React.PropTypes.oneOfType(
            [React.PropTypes.string,
                React.PropTypes.node,
                React.PropTypes.arrayOf(React.PropTypes.node)
            ]).isRequired,
        className: React.PropTypes.string,
        href: React.PropTypes.string,
        onClick: React.PropTypes.func,
        tag: React.PropTypes.string,
        tooltip: React.PropTypes.string,
        tooltipFor: React.PropTypes.string,
        tooltipPlace: React.PropTypes.string,
        variant: React.PropTypes.oneOf(['default', 'actionbar-default',
            'actionbar-constructive',
            'actionbar-destructive',
            'content-default',
            'content-hero'])
    };

    static defaultProps = {
        variant: 'content-default',
        type: 'button',
        tooltipFor: 'fis-tooltip'
    };

    render() {
        const ButtonComponent = this.props.tag || 'button';
        const opts = {};

        // if tag = a we should have reference
        opts.href = ButtonComponent === 'a' ? this.props.href : null;

        opts.className = classnames('button', this.props.variant, this.props.className);


        return (
            <ButtonComponent
                {...this.props}
                {...opts}
                onClick={this.props.onClick}
                data-tip={this.props.tooltip}
                data-for={this.props.tooltipFor}
                data-place={this.props.tooltipPlace}
            >
                {this.props.children}
            </ButtonComponent>
        );
    }
}
