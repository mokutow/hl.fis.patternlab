import React, { Component } from 'react';
import classnames from 'classnames';

import FisInputBase from 'fis-input-base';
import FisLabel from 'fis-label';
import FisFieldset from 'fis-fieldset';

export default class FisCheckboxRadioGroup extends Component {

    static propTypes = {
        className: React.PropTypes.string,
        label: React.PropTypes.string,
        opts: React.PropTypes.object,
        inputlist: React.PropTypes.array,
        readOnly: React.PropTypes.bool,
        labelRight: React.PropTypes.bool
    };
    static defaultProps = {
        opts: {}
    };

    render() {
        const opts = this.props.opts;


        // set disabled as readonly-state because HTML checkboxes dont' handle readOnly
        if (opts.readOnly) {
            opts.disabled = true;
        }

        // default type
        if (!opts.type) {
            opts.type = 'checkbox';
        }


        const classes = classnames('radio-checkbox-wrap', this.props.className, {
            'stacked': opts.stacked,
            'standalone': opts.standalone, // TODO: tbd
        });

        const legendClass = classnames('label-legend', {
            'is-invalid-label': opts.isInvalid
        });

        const inputNodes = this.props.inputlist.map((input, index) => {
            const inputOpts = opts;
            inputOpts.value = input.value;
            inputOpts.suffixIcon = input.suffixIcon;
            inputOpts.tooltip = input.tooltip;
            return (
                <FisLabel key={`item_${index}`} label={input.label} labelRight className="checkbox-radio-label">
                    <FisInputBase {...inputOpts} />
                </FisLabel>
            );
        });

        return (
            <FisFieldset
                className={classes}
                legend={opts.label}
                mandatory={opts.mandatory}
                legendClass={legendClass}
                infoflow={opts.infoflow}
            >
                <span className="input-wrap">
                    {inputNodes}
                    <span display-if={opts.errormessage} className="alert label">{opts.errormessage}</span>
                </span>
            </FisFieldset>
        );
    }
}
