import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import classnames from 'classnames';

export default class FisInputDate extends Component {

    static propTypes = {
        className: React.PropTypes.string,
        disabled: React.PropTypes.bool,
        placeholder: React.PropTypes.string,
        readOnly: React.PropTypes.bool,
        value: React.PropTypes.string,
        mandatory: React.PropTypes.bool,
    };

    render() {
        const opts = {};
        if (this.props.disabled) {
            opts.disabled = true;
        }
        if (this.props.readOnly) {
            opts.readOnly = 'readonly';
        }
        if (this.props.value) {
            opts.value = this.props.value;
        }
        if (this.props.placeholder) {
            opts.placeholderText = this.props.placeholder;
        }
        if (this.props.mandatory) {
            opts.required = 'required';
        }
        opts.className = classnames(this.props.className);

        return (
            <DatePicker onChange={this.handleChange} {...opts} />
        );
    }
}
