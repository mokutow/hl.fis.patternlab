import React from 'react';
import classnames from 'classnames';
import FisButton from 'fis-button';

export default class FisButtonIcon extends FisButton {

    static propTypes = {
        icon: React.PropTypes.string.isRequired,
        tooltip: React.PropTypes.string,
        children: React.PropTypes.oneOfType(
            [
                React.PropTypes.string,
                React.PropTypes.element
            ])
    };
    render() {
        const opts = {};
        const iconClass = classnames('fis-icon', this.props.icon);


        if (!this.props.standalone) {
            opts.className = 'btn-icon';
        } else {
            opts.className = 'btn-icon-styled';
        }

        opts.className = classnames(this.props.className, {
            'btn-icon-styled': this.props.standalone,
            'btn-icon': !this.props.standalone
        });

        return (
            <FisButton {...this.props} {...opts} >
                <span className="show-for-sr" aria-label={this.props.tooltip}>{this.props.tooltip}</span>
                <span aria-hidden="true" ><i className={iconClass}></i></span>
                <span display-if={this.props.children}>{this.props.children}</span>

            </FisButton>
        );
    }
}
