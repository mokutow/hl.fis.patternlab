import React, { Component } from 'react';
import classnames from 'classnames';

export default class FisButtonGroup extends Component {
    static propTypes = {
        className: React.PropTypes.string,
        children: React.PropTypes.oneOfType([React.PropTypes.arrayOf(React.PropTypes.element), React.PropTypes.element])
    };

    render() {
        const classes = classnames('button-group', this.props.className);

        return (
            <div className={classes}>
                {this.props.children}
            </div>
        );
    }
}
