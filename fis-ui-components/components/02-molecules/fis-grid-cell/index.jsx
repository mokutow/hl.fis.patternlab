import React, { Component } from 'react';
import FisInputDate from 'fis-input-date';
import FisInputBase from 'fis-input-base';
import FisInputPickflow from 'fis-input-pickflow';

export default class FisGridCell extends Component {
    static propTypes = {
        type: React.PropTypes.oneOf(['date', 'number', 'pickflow']),
        value: React.PropTypes.string
    }
    state = {
        editMode: false,
        value: this.props.value
    }
    handleKey(e) {
        if (e.key === 'Enter') {
            this.setState({
                editMode: false
            });
        }
    }
    handleClick() {
        this.setState({
            editMode: true
        });
    }
    handleChange(e) {
        this.setState({
            value: e.target.value
        });
    }
    handleBlur() {
        this.setState({
            editMode: false
        });
    }
    render() {
        return (
            <div
                onChange={(e) => { this.handleChange(e); } }
                onClick={() => { this.handleClick(); } }
                onKeyPress={(e) => { this.handleKey(e); } }
                onBlur={() => { this.handleBlur(); } }
            >
                <span className="cell-value" display-if={!this.state.editMode}>{this.state.value}</span>
                    {(() => {
                        switch (this.props.type) {
                            case 'pickflow':
                                return <FisInputPickflow value={this.state.value} display-if={this.state.editMode} />;
                            case 'date':
                                return <FisInputDate value={this.state.value} display-if={this.state.editMode} />;
                            case 'number':
                                return <FisInputBase defaultValue={this.state.value} type="number" display-if={this.state.editMode} />;
                            default:
                                return <FisInputBase defaultValue={this.state.value} display-if={this.state.editMode} />;
                        }
                    })()}
              </div>
        );
    }
}
