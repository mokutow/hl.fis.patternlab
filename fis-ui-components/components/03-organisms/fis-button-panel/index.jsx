import React, { Component } from 'react';
import classnames from 'classnames';

import FisButtonGroup from 'fis-button-group';
import FisButton from 'fis-button';

export default class FisButtonPanel extends Component {

    static propTypes = {
        className: React.PropTypes.string,
        children: React.PropTypes.arrayOf(React.PropTypes.element),
        buttonGroups: React.PropTypes.array
    };

    renderButtons = (buttons) => {
        return (
            buttons.map((button, index) => {
                return (
                    <FisButton
                        key={`button_${index}`}
                        className={button.class} id={button.id} variant={button.variant} action={button.action} tooltip={button.tooltip}
                    >
                        {button.title}
                    </FisButton>
                );
            }));
    };

    renderButtonGroups = () => {
        return (
            this.props.buttonGroups.map((buttonGroup, index) => {
                return (
                    <FisButtonGroup key={`buttongroup_${index}`}>
                        {this.renderButtons(buttonGroup.buttons)}
                    </FisButtonGroup>
                );
            })
        );
    };

    render() {
        const classes = classnames('fis-button-panel', this.props.className);
        return (
            <div className={classes}>
                {this.renderButtonGroups()}
            </div>
        );
    }
}
