import React, { Component } from 'react';
import DataGrid from 'react-datagrid';
import sorty from 'sorty';

// FIXME: DELETE WHEN DATA GRID IS REALLY USED
/* eslint-disable */

let SELECTED_ID = {};
let SORT_INFO = [{ name: 'country', dir: 'asc' }];
export default class FisDataGrid extends Component {
    constructor(...args) {
        super(...args);

        this.onSelectionChange = this.onSelectionChange.bind(this);
        this.onSortChange = this.onSortChange.bind(this);
        this.onColumnOrderChange = this.onColumnOrderChange.bind(this);
        this.onColumnResize = this.onColumnResize.bind(this);
        this.onColumnVisibilityChange = this.onColumnVisibilityChange.bind(this);
    }
    onSelectionChange(newSelection) {
        SELECTED_ID = newSelection;
         console.log(SELECTED_ID);
        const selected = [];

        Object.keys(newSelection).forEach(function pushSel(id) {
            selected.push(newSelection[id].firstName);
        });
         console.log(selected);
        this.setState({});
    }
    // Sorting Data
    onSortChange(sortInfo) {
        SORT_INFO = sortInfo;
        let data = [].concat(this.props.dataSource);
        data =  sorty(SORT_INFO, data);
        this.setState({});
    }

    // change col width
    onColumnResize(col, size) {
        col.width = size;
        this.setState({});
    }

    // change visibility of columns
    onColumnVisibilityChange(col, visible) {
        col.visible = visible;
        this.setState({});
    }

    // change column order
    onColumnOrderChange(index, dropIndex) {
        const columns = this.props.columns;
        const col = columns[index];
        //delete from index, 1 item
        columns.splice(index, 1);
        columns.splice(dropIndex, 0, col);
        this.setState({});
    }

    render() {
        const opts = {};
        if (this.props.idProperty) {
            opts.idProperty = this.props.idProperty;
        }
        if (this.props.dataSource) {
            opts.dataSource = this.props.dataSource;
        }
        if (this.props.pagination) {
            opts.pagination = this.props.pagination;
        }
        if (this.props.columns) {
            opts.columns = this.props.columns;
        }
        if (this.props.style) {
            opts.style = this.props.style;
        }
        if (this.props.defaultPageSize) {
            opts.defaultPageSize = this.props.defaultPageSize;
        }


        opts.showCellBorders = true;
        opts.sortInfo = { SORT_INFO };
        return (
            <DataGrid {...opts}
              pagination={false}
              onSortChange={this.onSortChange}
              onColumnOrderChange={this.onColumnOrderChange}
              selected={SELECTED_ID}
              onSelectionChange={this.onSelectionChange}
              onColumnResize={this.onColumnResize}
              onColumnVisibilityChange={this.onColumnVisibilityChange}
            />
        );
    }
}
FisDataGrid.propTypes = {

};
