import React, { Component } from 'react';


export default class FisModal extends Component {
    static propTypes = {
        header: React.PropTypes.any,
        body: React.PropTypes.any,
        footer: React.PropTypes.any,

    };

    componentDidMount = () => {
        // Initialize the modal, once we have the DOM node
        // TODO: Pass these in via props
        this.getDOMNode().modal({ background: true, keyboard: true, show: false });
    };
    componentWillUnmount = () => {
        this.getDOMNode().off('hidden');
    };
    // This was the key fix --- stop events from bubbling
    handleClick = (e) => {
        e.stopPropagation();
    };
    render() {
        const Header = this.props.header;
        const Body = this.props.body;
        const Footer = this.props.footer;
        return (
            <div onClick={this.handleClick} className="modal fade" role="dialog" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <Header className="modal-header" />
                        <Body className="modal-content" />
                        <Footer className="modal-footer" />
                    </div>
                </div>
            </div>
        );
    }
}
