import React, { Component } from 'react';
import FisModal from 'fis-modal';
export default class FisModalTrigger extends Component {
    static propTypes = {
        header: React.PropTypes.any,
        body: React.PropTypes.any,
        footer: React.PropTypes.any,
        trigger: React.PropTypes.any,
        children: React.PropTypes.any
    };


    handleClick = () => {
        this.refs.payload.getDOMNode().modal();
    }
    render() {
        const Trigger = this.props.trigger;
        return (<div onClick={ this.handleClick }>
            <Trigger />
            <FisModal
                ref="payload"
                header={this.props.header}
                body={this.props.body}
                footer={this.props.footer}
            >
                {this.props.children}
            </FisModal>;
        </div>);
    }
}
