import React, { Component } from 'react';
import ReactTooltip from 'react-tooltip';
import FisSection from 'fis-section';
import FisButtonPanel from 'fis-button-panel';


export default class FisPanel extends Component {

    static propTypes = {
        title: React.PropTypes.string,
        isOpened: React.PropTypes.bool,
        children: React.PropTypes.oneOfType([React.PropTypes.arrayOf(React.PropTypes.node), React.PropTypes.node]),
        buttonGroupsTop: React.PropTypes.array,
        buttonGroupsMiddle: React.PropTypes.array,
        buttonGroupsBottom: React.PropTypes.array
    };

    state = {
        isOpened: this.props.isOpened
    };

    render() {
        return (
            <FisSection className="fis-panel" >
                <FisSection className={"panel-content"}>
                    {this.props.children}
                </FisSection>
                <aside className="panel-buttons">
                    <FisButtonPanel buttonGroups={this.props.buttonGroupsTop} className="" />
                    <div className="scroll-indicator top"></div>
                    <FisButtonPanel buttonGroups={this.props.buttonGroupsMiddle} className="panel-buttons-scroll" />
                    <div className="scroll-indicator bottom"></div>
                    <FisButtonPanel buttonGroups={this.props.buttonGroupsBottom} className="" />
                </aside>

                <ReactTooltip
                    id="fis-tooltip"
                    effect="solid"
                    html={false}
                    class="fis-tooltip"
                    delayShow={500}
                    delayhide={100}
                />

            </FisSection>
        );
    }
}
