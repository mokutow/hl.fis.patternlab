import React, { Component } from 'react';
import classnames from 'classnames';
import ReactTooltip from 'react-tooltip';

import FisIcon from 'fis-icon';
import FisButtonIcon from 'fis-button-icon';
import FisButton from 'fis-button';
import FisInputSuggest from 'fis-input-suggest';
import FisDropDownMenu from 'fis-dropdown-menu';

export default class FisClientHeader extends Component {

    static propTypes = {
        appsList: React.PropTypes.object,
        applicationMenu: React.PropTypes.array,
        favouriteMenu: React.PropTypes.array,
        user: React.PropTypes.object,
        setOpenNewApp: React.PropTypes.func,
        setCurrentApp: React.PropTypes.func,
        setCloseApp: React.PropTypes.func
    };

    static defaultProps = {
        user: null
    };


    setCurrentApp = (event) => {
        event.preventDefault();
        const item = event.currentTarget.dataset;
        console.log('fis-client-header setCurrentApp', item);
        const app = { id: item.id, url: item.url };
        this.props.setCurrentApp(app);
    };

    closeTab = (event) => {
        event.preventDefault();
        const item = event.currentTarget.dataset;
        console.log('fis-client-header closeTab id:', item.id);
        const app = { id: item.id };
        this.props.setCloseApp(app, this.props.appsList);
    };

    renderList = (list) => {
        list.map((listItem) => {
            return (
                <li>
                    <a href="">{listItem.name}</a>
                </li>
            );
        });
    };

    renderTooltip = (listItem) => {
        let tooltopHTML = '<dl>' +
            '<dt><strong>APP: </strong></dt><dd>' + listItem.app.name + '</dd>';
        if (listItem.modal) {
            tooltopHTML = tooltopHTML + '<dt>open Modal: </dt><dd>' + listItem.modal.name + '</dd>';
        }
        if (listItem.app.params) {
            tooltopHTML = tooltopHTML + '<dt>Params: </dt><dd>' + listItem.app.params + '</dd>';
        }
        tooltopHTML = tooltopHTML + '</dl>';

        return tooltopHTML;
    }

    renderTabList = (list) => {
        return (
            list.map((listItem, index) => {
                const tabClass = classnames('tabs-tablistItem', listItem.className, {
                    'is-active': listItem.id === this.props.appsList.currentApp.id,
                    'has-modal': listItem.modal
                });
                const tabInnerClass = classnames('tabs-tabInner', {
                    'is-active': listItem.id === this.props.appsList.currentApp.id
                });

                const tooltopHTML = this.renderTooltip(listItem);

                return (
                    <li className={tabClass} key={`headertab-tablistItem_${index}`}>
                        <div className="tabs-tab">
                            <div
                                className={tabInnerClass}
                                data-tip={tooltopHTML}
                                data-for="header-tooltip"
                            >
                                <a
                                    href="#"
                                    display-if={!listItem.app.icon}
                                    data-id={listItem.id}
                                    data-url={listItem.app.url}
                                    onClick={this.setCurrentApp}
                                >
                                    {listItem.app.name}
                                </a>
                                <FisButtonIcon
                                    display-if={listItem.app.icon}
                                    icon={listItem.app.icon}
                                    data-id={listItem.id}
                                    data-url={listItem.app.url}
                                    onClick={this.setCurrentApp}
                                />
                                <span display-if={listItem.modal}>
                                    &nbsp;- [with dialog: {listItem.modal.name}]
                                </span>
                            </div>
                        </div>
                        <FisButton
                            display-if={!listItem.noClose}
                            className="close"
                            data-id={listItem.id}
                            onClick={this.closeTab}
                        >
                            <span>&times;</span>
                        </FisButton>
                    </li>
                );
            })
        );
    };

    render() {
        return (
            <header className="fis-clientheader">
                <a href="#" className="fis-logo">
                    <FisIcon icon="fis-icon-fis-logo" />
                </a>
                <FisInputSuggest display-if={this.props.user} />
                <nav className="app-navigation" display-if={this.props.user}>
                    <ul className="icon-list">
                        <li display-if={this.props.applicationMenu}>
                            <FisDropDownMenu
                                icon="fis-icon-applications"
                                tooltip="open all FIS Applications"
                                tooltipPlace="bottom"
                                list={this.props.applicationMenu}
                                id="fis-app-drop"
                                handleClick={this.props.setOpenNewApp}
                            />
                        </li>
                        <li display-if={this.props.favouriteMenu}>
                            <FisDropDownMenu
                                icon="fis-icon-star"
                                tooltip="open favourite FIS Applications"
                                tooltipPlace="bottom"
                                list={this.props.favouriteMenu}
                                id="fis-fav-drop"
                                handleClick={this.props.setOpenNewApp}
                            />

                        </li>
                    </ul>
                </nav>
                <div className="devider" display-if={this.props.user} />
                <nav className="fis-tabpanel open-app-navigation" display-if={this.props.user}>
                    <ul className="tabs-tablist">{this.renderTabList(this.props.appsList.apps)}</ul>
                </nav>
                <div className="devider" display-if={this.props.user} />
                <section className="userarea" display-if={this.props.user}>
                    <ul className="icon-list">
                        <li>
                            <FisButtonIcon
                                icon="fis-icon-info"
                                tooltip="open FIS Informations"
                                tooltipPlace="bottom"
                            >
                                12
                            </FisButtonIcon>
                        </li>
                        <li>
                            <FisButtonIcon icon="fis-icon-inbox" tooltip="open FIS Inbox" tooltipPlace="bottom">
                                3
                            </FisButtonIcon>
                        </li>
                        <li>
                            <FisButtonIcon icon="fis-icon-message" tooltip="open FIS Messages" tooltipPlace="bottom">
                                25
                            </FisButtonIcon>
                        </li>
                        <li>
                            <FisButtonIcon icon="fis-icon-help" tooltip="open FIS Help" tooltipPlace="bottom" />
                        </li>
                        <li>
                            <span className="user-icon">
                                <img display-if={this.props.user.avatarData} src={'data:image/jpeg;base64,' + this.props.user.avatarData} title={this.props.user.username} />
                            </span>
                            <FisIcon icon="fis-icon-arrow-down" />
                        </li>
                    </ul>

                </section>
                <ReactTooltip id="header-tooltip" class="fis-tooltip" effect="solid" place="bottom" html delayShow={500} delayhide={100} />
            </header>
        );
    }
}
