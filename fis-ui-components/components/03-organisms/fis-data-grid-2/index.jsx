import React, { Component } from 'react';
import { AgGridReact } from 'ag-grid-react-component';
import FisButtonIcon from 'fis-button-icon';

// FIXME: DELETE WHEN DATA GRID IS REALLY USED
/* eslint-disable */
export default class FisDataGrid2 extends Component {
    constructor(...args) {
        super(...args);
    }
    state = {
        quickFilterText: null,
        showGrid: true,
        showToolPanel: false,
        columnDefs: this.props.columnDefs,
        rowData: null,
        icons: {
            columnRemoveFromGroup: '<i class="fa fa-remove"/>',
            filter: '<i class="fa fa-filter"/>',
            sortAscending: '<i class="fa fa-long-arrow-down"/>',
            sortDescending: '<i class="fa fa-long-arrow-up"/>',
            groupExpanded: '<i class="fa fa-minus-square-o"/>',
            groupContracted: '<i class="fa fa-plus-square-o"/>',
            columnGroupOpened: '<i class="fa fa-minus-square-o"/>',
            columnGroupClosed: '<i class="fa fa-plus-square-o"/>'
        }
    };

    // the grid options are optional, because you can provide every property
    // to the grid via standard React properties. however, the react interface
    // doesn't block you from using the standard JavaScript interface if you
    // wish. Maybe you have the gridOptions stored as JSON on your server? If
    // you do, the providing the gridOptions as a standalone object is just
    // what you want!
     static gridOptions = {
        // this is how you listen for events using gridOptions
        onModelUpdated: function() {
            console.log('event onModelUpdated received');
        },
        // this is a simple property
        rowBuffer: 10 // no need to set this, the default is fine for almost all scenarios
    };


    getJSON(url) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(status);
                }
            };
            xhr.send();
        });
    };


    onShowGrid = (show) => {
        this.setState({
            showGrid: show
        });
    };

    onToggleToolPanel = (event) => {
        this.setState({showToolPanel: event.target.checked});
    };

    onReady = (params) => {
        this.api = params.api;
        this.columnApi = params.columnApi;

    };

    selectAll = () =>{
        this.api.selectAll();
    };

    deselectAll = () => {
        this.api.deselectAll();
    };

    setCountryVisible = (visible) => {
        this.columnApi.setColumnVisible('country', visible);
    };

    onQuickFilterText = (event) => {
        this.setState({quickFilterText: event.target.value});
    };

    onCellClicked = (event) => {
        console.log('onCellClicked: ' + event.data.name + ', col ' + event.colIndex);
    };

    onRowSelected = (event) => {
        console.log('onRowSelected: ' + event.node.data.name);
    };

    onRefreshData = () => {
        let rowData = this.getJSON(this.state.dataUrl).then(function(jsonData) {
            this.setState({
                rowData: jsonData
            });
            //console.log('dataURL', this.state.dataUrl);
            //console.log('data', this.state.rowData);

        }, function(status) {
            alert('Something went wrong.');
        });
    };

    render() {
        // in onReady, store the api for later use

        return (
            <section className="fis-datagrid">
                <AgGridReact
                    // listen for events with React callbacks
                    onRowSelected={this.onRowSelected}
                    onCellClicked={this.onCellClicked}

                    // binding to properties within React State or Props
                    showToolPanel={this.state.showToolPanel}
                    quickFilterText={this.state.quickFilterText}
                    icons={this.state.icons}

                    // column definitions and row data are immutable, the grid
                    // will update when these lists change
                    columnDefs={this.state.columnDefs}
                    rowData={this.props.rowData}

                    // or provide props the old way with no binding
                    rowSelection="multiple"
                    enableSorting="true"
                    enableFilter="true"
                    rowHeight="25"
                    headerHeight="25"
                />
                <aside className="sidepanel">
                    <FisButtonIcon icon="fis-icon-paging-up-inactive">Button</FisButtonIcon>
                    <FisButtonIcon icon="fis-icon-paging-down">Button</FisButtonIcon>
                </aside>
            </section>
        );
    }
}
