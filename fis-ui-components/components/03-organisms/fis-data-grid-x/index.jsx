import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { AgGridReact } from 'ag-grid-react-component';
import { HotKeys } from 'react-hotkeys';
import FisGridCell from 'fis-grid-cell';

const keyMap = {
    'selectAll': 'ctrl+a',
    'deselectAll': 'ctrl+d',
    'selectUp': 'shift+up',
    'selectDown': 'shift+down',
    'addRow': 'ctrl+alt+enter',
    'deleteRow': 'del',
    'editCell': 'enter'
};

export default class FisDataGridX extends Component {

    // Properties
    static propTypes = {
        className: React.PropTypes.string,
        data: React.PropTypes.array,
        columns: React.PropTypes.array,
        sort: React.PropTypes.bool,
        resize: React.PropTypes.bool,
        move: React.PropTypes.bool,
        filter: React.PropTypes.bool,
        select: React.PropTypes.string,
        edit: React.PropTypes.bool
    };

    constructor(...args) {
        super(...args);
        this.setCustomRenderer(this.props.columns);
    }
    // Predefined state
    state = {
        quickFilterText: null,
        showGrid: true,
        showToolPanel: false
    };

    onSelectAll(e) {
        e.preventDefault();
        const cell = this.api.getFocusedCell();
        const rowIndex = cell.rowIndex;
        const colIndex = cell.colIndex;
        this.api.selectAll();
        this.api.setFocusedCell(rowIndex, colIndex, null);
    }

    onDeselectAll(e) {
        e.preventDefault();
        const cell = this.api.getFocusedCell();
        const rowIndex = cell.rowIndex;
        const colIndex = cell.colIndex;
        this.api.deselectAll();
        this.api.setFocusedCell(rowIndex, colIndex, null);
    }

    onSelectUp() {
        const cell = this.api.getFocusedCell();
        this.api.selectionController.selectIndex(cell.rowIndex, true);
    }

    onSelectDown() {
        const cell = this.api.getFocusedCell();
        this.api.selectionController.selectIndex(cell.rowIndex, true);
    }

    onDeleteRow() {

    }

    onAddRow() {
        const cell = this.api.getFocusedCell();
        const rowIndex = cell.rowIndex;
        const colIndex = cell.colIndex;
        const rows = this.gridOptions.rowData;
        rows.push({});
        this.api.setRowData(rows);
        this.api.setFocusedCell(rowIndex, colIndex, null);
    }

    onEditCell() {

    }

    setCustomRenderer(columns) {
        columns.forEach((column) => {
            const __column = column;
            if (column.chilren) {
                this.setCustomRenderer(column.children);
            } else {
                __column.cellRenderer = this.fisCellRenderer;
            }
        });
    }

    fisCellRenderer(params) {
        const cell = document.createElement('div');
        const type = params.colDef.type;
        const value = params.value;
        ReactDOM.render(<FisGridCell type={type} value={value} />, cell);
        return cell;
    }

    // Predefined options
    gridOptions = {
        // Options
        rowData: this.props.data,
        columnDefs: this.props.columns,
        enableColResize: this.props.resize,
        // single || multiple
        rowSelection: this.props.select,
        // Default value
        rowBuffer: 10,
        rowDeselection: true,
        // Row group settings
        groupSuppressAutoColumn: true,
        groupHideGroupColumns: false,
        groupUseEntireRow: true,
        enableSorting: this.props.sort,
        // suppressRowClickSelection: true,
        sortingOrder: [
            'desc', 'asc', null
        ],
        groupRowInnerRenderer: (params) => {
            return params.node.key;
        },

        // Events
        onReady: (props) => {
            this.api = props.api;
            this.columnApi = props.columnApi;
        },
        onRefreshData: () => {

        },
        onShowGrid: (show) => {
            this.setState({ showGrid: show });
        },
        // Row is selected
        onRowSelected: () => {

        },
        // Row is de-selected.
        onRowDeselected: () => {

        },
        // Row is clicked.
        onRowClicked: () => {

        },
        // Row is double clicked
        onRowDoubleClicked: () => {

        },
        // Cell clicked
        onCellClicked: () => {

        },
        // Cell is double clicked.
        onCellDoubleClicked: () => {

        },
        // Cell is right clicked.
        // Context menu?
        onCellContextMenu: () => {

        },
        // Focused cell = Focused row
        onCellFocused: () => {

        },
        // Displayed rows have changed. Happens following sort, filter or tree expand / collapse events.
        onModelUpdated: () => {

        }
    }

    render() {
        const handlers = {
            'selectAll': (e) => { this.onSelectAll(e); },
            'deselectAll': (e) => { this.onDeselectAll(e); },
            'selectUp': () => { this.onSelectUp(); },
            'selectDown': () => { this.onSelectDown(); },
            'addRow': () => { this.onAddRow(); },
            'deleteRow': () => { this.onDeleteRow(); },
            'editCell': () => { this.onEditCell(); }
        };

        return (
            <HotKeys keyMap={keyMap} handlers={ handlers }>
              <section className="fis-datagrid-x">
                  <AgGridReact gridOptions={ this.gridOptions } />
              </section>
            </HotKeys>
        );
    }
}
