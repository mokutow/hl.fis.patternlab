###Selection:
- The user can select a single line.
- The user can switch between selected lines by using the arrow up and down key
- The user can select multiple lines by using the mouse + CTRL, CTRL+A, SHIFT + arrow up and down key
- The use can focus a single cell (max. 1 at a time). By focusing the cell the whole line gets selected. The user can change the focus by using the mouse or the arrow up, down, left and right.
- The user can select a continuous range of cells (like in Microsoft Excel) by pressing the Alt key and dragging the mouse.
- The user can copy a selected range of cells into the clipboard, so that it can be pasted into another part of the table or into Microsoft Excel
- The user can paste a continuous range of cells from Microsoft Excel (or equivalent) into the table.

###Edit:
- The developer can define a cell as editable xor non-editable
- The developer can define a whole row as editable xor non-editable. If a row is non-editable all cells within the row are non-editable, too.
- The developer can define a whole table as editable xor non-editable. If a table is non-editable all rows and cells are non-editable, too.
- The user can edit a focused and editable cell using the mouse (text selection, copy, paste)
- The user can edit a focused and editable cell using the keyboard

###Rows / Lines:
- The developer can define on table level if a new line can be can be added
- The developer can define on table level if rows can be deleted
- The user can add new lines, if allowed, by using CTRL+Alt+Enter

###Columns:
- The user can rearranged columns and column groups by drag and drop on the column header. A column can never be draged out of its column group, if grouped at all.
- The user can hide columns and column groups.
- The user can resize columns
- The developer can define single columns as fixed, so that their columns cannot be rearranged.
- The user cannot order rows by clicking on the column header.


###Selection:
- The user can select a single line.
- The user can switch between selected lines by using the arrow up and down key
- The user can select multiple lines by using the mouse + CTRL, CTRL+A, SHIFT + arrow up and down key
- The use can focus a single cell (max. 1 at a time). By focusing the cell the whole line gets selected. The user can change the focus by - using the mouse or the arrow up, down, left and right.

###With:
- Columns
- Column Groups
- Fixed Columns (Always left. Not affected by horizontal scrolling)

###Edit:
- The developer can define a cell as editable xor non-editable
- The developer can define a whole row as editable xor non-editable. If a row is non-editable all cells within the row are non-editable, too.
- The developer can define a whole table as editable xor non-editable. If a table is non-editable all rows and cells are non-editable, too.
- The user can edit a focused and editable cell using the mouse (text selection, copy, paste)
- The user can edit a focused and editable cell using the keyboard

IMPORTANT
All cell types will match the basic FIS Web input components, like: FisInputBase, FisInputTime, FisInputMatchCode, ...

# Sort = true/false
# Filter = true/false
# Column groups
# Row groups
# Pin column