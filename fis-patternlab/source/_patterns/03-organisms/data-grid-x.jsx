import React from 'react';
import { FisDataGridX } from 'fisui.js';

export default class FisDataGridPatternX extends React.Component {

    render() {
        const d = [{
            'date': '',
            'simple': 'simple',
            'number': 11
        }];

        const dataGroup = [
            {
                'field': 'Hello',
                'field-2': '2-Hello',
                'group': 1
            }, {
                'field': 'Hello-2',
                'field-2': '2-Hello-2',
                'group': 1
            }, {
                'field': 'Hello-3',
                'field-2': '2-Hello-3',
                'group': 2
            }, {
                'field': 'Hello-4',
                'field-2': '2-Hello-4',
                'group': 1
            }, {
                'field': 'Hello-5',
                'field-2': '2-Hello-5',
                'group': 2
            }, {
                'field': 'Hello-6',
                'field-2': '2-Hello-6',
                'group': 3
            }
        ];

        const data = [
            {
                'index': 1,
                'id': 'id_1',
                'marvel': {
                    'hero': 'Deadpool',
                    'villain': 'Deadpool'
                },
                'dc': {
                    'hero': 'The Flash',
                    'villain': 'Captain Cold'
                },
                'one': 'one',
                'two': 'two',
                'three': 'trhee',
                'four': 'four'
            }, {
                'index': 2,
                'id': 'id_2',
                'marvel': {
                    'hero': 'Doctor Strange',
                    'villain': 'Hello, Kitty'
                },
                'dc': {
                    'hero': 'Shazam',
                    'villain': 'Black Manta'
                },
                'one': 'one',
                'two': 'two',
                'three': 'trhee',
                'four': 'four'
            }, {
                'index': 3,
                'id': 'id_3',
                'marvel': {
                    'hero': 'Hulk',
                    'villain': 'Some Awesome Man'
                },
                'dc': {
                    'hero': 'Robin',
                    'villain': 'Bizzaro'
                },
                'one': 'one',
                'two': 'two',
                'three': 'trhee',
                'four': 'four'
            }
        ];

        const columnsGroup = [
            {
                headerName: 'group',
                field: 'group',
                rowGroupIndex: 0,
                cellRenderer: {
                    renderer: 'group'
                }
            }, {
                headerName: 'data',
                field: 'field'
            }, {
                headerName: 'data-2',
                field: 'field-2'
            }
        ];

        const c = [
            {
                headerName: 'Date',
                field: '12-12-2016',
                type: 'date'
            }, {
                headerName: 'Simple',
                field: 'simple'
            }, {
                headerName: 'Number',
                type: 'number'
            }, {
                headerName: 'Checkbox',
                type: 'checkbox'
            }
        ];

        const columns = [
            {
                headerName: 'Marvel Comics',
                children: [
                    {
                        headerName: 'Hero',
                        field: 'marvel.hero'
                    }, {
                        headerName: 'Villain',
                        field: 'marvel.villain'
                    }
                ]
            }, {
                headerName: 'DC Comics',
                children: [
                    {
                        headerName: 'Hero',
                        field: 'dc.hero',
                        pinned: 'left'
                    }, {
                        headerName: 'Villain',
                        field: 'dc.villain'
                    }
                ]
            }, {
                headerName: 'One more column',
                children: [
                    {
                        headerName: 'Column grouped',
                        field: 'one',
                        columnGroupShow: 'closed'
                    }, {
                        headerName: 'Two',
                        field: 'two',
                        columnGroupShow: 'open'
                    }, {
                        headerName: 'Three',
                        field: 'three',
                        columnGroupShow: 'open'
                    }, {
                        headerName: 'Four',
                        field: 'four',
                        columnGroupShow: 'open'
                    }
                ]
            }
        ];

        return (
            <div>
                <h2>Feature list</h2>
                <ul>
                    <li>Columns pin [DC Comics]</li>
                    <li>Rows group</li>
                    <li>Columns group</li>
                    <li>Sort</li>
                    <li>Resize</li>
                    <li>Rearrange attrs</li>
                    <li>Simple stub renderers for grouped rows</li>
                </ul>
                <ul>
                  <li>click = select row</li>
                  <li>ctrl+click = select/deselect row</li>
                  <li>shift+up/down = select row in which cell will be focused after up/down event</li>
                  <li>ctrl+a = select all</li>
                  <li>ctrl+d = deselect all</li>
                  <li>ctrl+alt+enter = add new empty row</li>
                </ul>
                <FisDataGridX columns={columns} data={data} select="multiple" resize sort />
                <FisDataGridX columns={columnsGroup} data={dataGroup} />
                <FisDataGridX columns={c} data={d} />
            </div>
        );
    }
}
