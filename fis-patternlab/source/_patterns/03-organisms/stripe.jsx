import React from 'react';

import { FisStripe } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';

export default class FisStripePattern extends React.Component {
    render() {
        const buttonsGroups = [
            {
                buttons: [
                    { id: 'dl', title: 'Download', variant: 'content-default', action: '#/save' },
                    { id: 'dl2', title: 'Upload', variant: 'content-default', action: '#/save' },
                    { id: 'dl3', title: 'Excel', variant: 'content-default', action: '#/save' },
                    { id: 'dl4', title: 'Word', variant: 'content-default', action: '#/save' }
                ]
            },
            {
                buttons: [
                    { id: 'dl4', title: 'Mass Select', variant: 'content-default', action: '#/save', disabled: true },
                    { id: 'dl5', title: 'Clear', variant: 'content-default', action: '#/save' },
                    { id: 'dl6', title: 'Search', variant: 'content-hero', action: '#/save' }
                ]
            },
            {
                buttons: [
                    { id: 'sdl', title: 'Download', variant: 'content-default', action: '#/save' },
                    { id: 'sdl2', title: 'Upload', variant: 'content-default', action: '#/save' },
                    { id: 'sdl3', title: 'Excel', variant: 'content-default', action: '#/save' },
                    { id: 'sdl4', title: 'Word', variant: 'content-default', action: '#/save' }
                ]
            }
        ];

        return (
            <div>
                <h1>Stripe</h1>
                <form>
                    <FisContainerRow>
                        <FisContainerColumn colSpan={60} className="colored-col">
                            <FisStripe
                                title="Search"
                                isOpened
                                buttonGroups={buttonsGroups}
                            >
                             GANZ VIEL SUCHE
                            </FisStripe>

                            <FisStripe
                                title="Result"
                                isOpened={false}
                                buttonGroups={buttonsGroups}
                            >
                                GANZ VIEL RESULTS
                            </FisStripe>

                            <FisStripe
                                title="Detail"
                                isOpened={false}
                                buttonGroups={buttonsGroups}
                            >
                                GANZ VIEL DETAIL
                            </FisStripe>

                        </FisContainerColumn>
                    </FisContainerRow>
                </form>
            </div>
        );
    }
}
