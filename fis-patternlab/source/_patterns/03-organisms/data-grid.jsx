import React from 'react';

import { FisDataGrid } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';

export default class FisDataGridPattern extends React.Component {
    render() {
        const data = '../../data/data-200.json';
        const columns = [
            { name: 'st', title: 'ST', width: 61, textAlign: 'right' },
            { name: 'TmpSep', title: 'Tmp Sep', width: 70 },
            { name: 'TranEx', title: 'Tran Ex', width: 50 },
            { name: 'ShTy', title: 'ShTy', width: 50 },
            { name: 'Shipment', title: 'Shipment', width: 150 },
            { name: 'Shipment2', title: 'Shipment2', width: 150 },
            { name: 'Shipment3', title: 'Shipment3', width: 150 },
            { name: 'Shipment4', title: 'Shipment4', width: 150 },
            { name: 'index', title: '#', width: 150 },
            { name: 'firstName', width: 150 },
            { name: 'lastName', width: 150 },
            { name: 'city', width: 200 },
            { name: 'country', width: 200 },
            { name: 'email', width: 150 }
        ];

        return (
            <div>
                <h1>Data Grid</h1>
                <form>
                    <FisContainerRow >
                        <FisContainerColumn colSpan={100}>
                            <FisDataGrid idProperty="myTable1"
                                dataSource={data}
                                pagination={false}
                                columns={columns}
                                defaultPageSize="20"
                                style={{ height: 400 }}
                            />
                        </FisContainerColumn>
                    </FisContainerRow>
                </form>
            </div>
        );
    }
}
