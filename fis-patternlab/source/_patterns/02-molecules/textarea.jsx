import React from 'react';

import { FisInputGroup } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';
import { FisTextArea } from 'fisui.js';

export default class FisTextareaPattern extends React.Component {
    render() {
        const errorsMessages = {
            error1: 'There is a rea l big error in the FIRST input-field. Please check it again.',
            error2: 'There is a real big error in the SECOND input-field. Please check it again.'
        };

        return (
            <div>
                <h1>Textarea</h1>
                <form>
                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>default</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn className="demo-col" colSpan={10}>
                            <FisInputGroup label="My Textarea Label">
                                <FisTextArea
                                    rows="5"
                                    placeholder="Name"
                                    colSpan={10}
                                    defaultValue="feld 0"
                                />
                            </FisInputGroup>
                        </FisContainerColumn>
                    </FisContainerRow>
                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>disabled</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn className="demo-col" colSpan={10}>
                            <FisInputGroup label="My Textarea Label">
                                <FisTextArea
                                    rows="5"
                                    placeholder="Name"
                                    colSpan={10}
                                    defaultValue="feld 0"
                                    disabled
                                />
                            </FisInputGroup>
                        </FisContainerColumn>
                    </FisContainerRow>

                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>read-only</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn className="demo-col" colSpan={10}>
                            <FisInputGroup label="My Textarea Label">
                                <FisTextArea
                                    rows="5"
                                    placeholder="Name"
                                    colSpan={10}
                                    defaultValue="feld 0"
                                    readOnly
                                />
                            </FisInputGroup>
                        </FisContainerColumn>
                    </FisContainerRow>

                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>error</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn className="demo-col" colSpan={10}>
                            <FisInputGroup label="My Textarea Label" isInvalid>
                                <FisTextArea
                                    rows="5"
                                    placeholder="Name"
                                    colSpan={10}
                                    errormessage={errorsMessages.error1}
                                    defaultValue="feld 0"
                                />
                            </FisInputGroup>
                        </FisContainerColumn>
                    </FisContainerRow>

                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>mandatory</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn className="demo-col" colSpan={10}>
                            <FisInputGroup label="My Textarea Label">
                                <FisTextArea
                                    rows="5"
                                    placeholder="Name"
                                    colSpan={10}
                                    mandatory
                                    defaultValue="feld 0"
                                />
                            </FisInputGroup>
                        </FisContainerColumn>
                    </FisContainerRow>
                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>error</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn className="demo-col" colSpan={10}>
                            <FisInputGroup label="My Textarea Label" isInvalid>
                                <FisTextArea
                                    rows="5"
                                    placeholder="Name"
                                    colSpan={10}
                                    errormessage={errorsMessages.error1}
                                    defaultValue="feld 0"
                                />
                            </FisInputGroup>
                        </FisContainerColumn>
                    </FisContainerRow>

                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>infoflow + suffix</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn className="demo-col" colSpan={14}>
                            <FisInputGroup label="My Textarea Label">
                                <FisTextArea
                                    rows="5"
                                    placeholder="Name"
                                    colSpan={14}
                                    placeholder="Name"
                                    defaultValue="May the force be with you"
                                    suffix="a suffix"
                                    infoflow
                                />
                            </FisInputGroup>
                        </FisContainerColumn>
                    </FisContainerRow>


                </form>
            </div>
        );
    }
}
