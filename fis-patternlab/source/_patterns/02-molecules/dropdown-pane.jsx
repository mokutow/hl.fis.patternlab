import React from 'react';

import { FisDropdownPane } from 'fisui.js';

export default class FisDropdownPanePattern extends React.Component {
    render() {
        return (
            <FisDropdownPane id="pane" title="hover me">
                <p>I'm beginning to feel like a Rap God, Rap God</p>
                <p>All my people from the front to the back nod, back nod</p>
                <p>Now who thinks their arms are long enough to slap box, slap box?</p>
                <p>They said I rap like a robot, so call me rap-bot</p>
            </FisDropdownPane>
        );
    }
}
