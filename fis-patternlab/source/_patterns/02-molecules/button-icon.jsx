import React from 'react';

import { FisButtonIcon } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';

export default class FisButtonIconPattern extends React.Component {
    render() {
        return (
            <div>
                <h1>Buttons with icons only </h1>
                <FisContainerRow>
                    <FisContainerColumn colSpan={10}>
                        <h2>
                            <span className="info">state: <strong>default (icon only)</strong></span>
                        </h2>
                    </FisContainerColumn >
                    <FisContainerColumn colSpan={15} className="">
                        <FisButtonIcon icon="fis-icon-applications">Button</FisButtonIcon>
                    </FisContainerColumn >
                </FisContainerRow>


                <FisContainerRow>
                    <FisContainerColumn colSpan={10}>
                        <h2>
                            <span className="info">state: <strong>visible Button</strong></span>
                        </h2>
                    </FisContainerColumn >
                    <FisContainerColumn colSpan={15} className="">
                        <FisButtonIcon standalone icon="fis-icon-applications">Button</FisButtonIcon>
                    </FisContainerColumn>
                </FisContainerRow>
            </div>
        );
    }
}
