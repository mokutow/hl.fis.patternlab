import React from 'react';

import { FisInputGroup } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';
import { FisInputBase } from 'fisui.js';
import { FisInputDate } from 'fisui.js';
import { FisTextArea } from 'fisui.js';

export default class FisInputGroupPattern extends React.Component {
    render() {
        const errorsMessages = {
            error1: 'There is a rea l big error in the FIRST input-field. Please check it again.',
            error2: 'There is a real big error in the SECOND input-field. Please check it again.'
        };

        return (
            <div>
                <h1>Input-Group</h1>
                <form>
                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>default</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn className="demo-col" colSpan={30}>
                            <FisInputGroup label="My Label">
                                <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={8} />
                                <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={8} />
                                <FisInputBase infoflow placeholder="Name" defaultValue="feld 1" colSpan={8} />
                            </FisInputGroup>
                        </FisContainerColumn>


                        <FisContainerColumn className="demo-col" colSpan={20}>
                            <FisInputGroup label="My Label">
                                <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={8} />
                                <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={3} />
                                <FisInputBase infoflow placeholder="Name" defaultValue="feld 1" colSpan={1} />
                            </FisInputGroup>
                        </FisContainerColumn>
                    </FisContainerRow>
                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>disabled</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn className="demo-col" colSpan={30}>
                            <FisInputGroup label="My Label">
                                <FisInputBase placeholder="Name" defaultValue="feld 0" disabled colSpan={9} />
                                <FisInputDate placeholder="Name" defaultValue="feld 1" disabled colSpan={4} />
                                <FisInputBase placeholder="Name" defaultValue="disabled" disabled colSpan={9} />
                                <FisInputBase infoflow placeholder="Name" defaultValue="feld 1" disabled colSpan={6} />
                            </FisInputGroup>
                        </FisContainerColumn>
                    </FisContainerRow>

                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>read-only</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn className="demo-col" colSpan={30}>
                            <FisInputGroup label="My Label">
                                <FisInputBase placeholder="Name" defaultValue="feld 0" readOnly colSpan={3} />
                                <FisInputDate placeholder="Name" defaultValue="feld 1" readOnly colSpan={8} />
                                <FisInputBase placeholder="Name" defaultValue="disabled" readOnly colSpan={6} />
                                <FisInputBase infoflow placeholder="Name" defaultValue="feld 1" readOnly colSpan={9} />
                            </FisInputGroup>
                        </FisContainerColumn>
                        <FisContainerColumn className="demo-col" colSpan={10}>
                            <FisInputGroup label="My Textarea Label">
                                <FisTextArea
                                    rows="5"
                                    placeholder="Name"
                                    colSpan={10}
                                    defaultValue="feld 0"
                                    readOnly
                                />
                            </FisInputGroup>
                        </FisContainerColumn>
                    </FisContainerRow>

                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>error</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn className="demo-col" colSpan={30}>
                            <FisInputGroup label=" " isInvalid>
                                <FisInputBase
                                    placeholder="empty Label above"
                                    errormessage={errorsMessages.error1}
                                    defaultValue="feld 0"
                                    colSpan={9}
                                />
                                <FisInputDate placeholder="Date" defaultValue="feld 1" colSpan={4} />
                                <FisInputBase
                                    placeholder="Name"
                                    errormessage={errorsMessages.error2}
                                    defaultValue="feld 0" colSpan={9}
                                />
                                <FisInputBase infoflow placeholder="Name" defaultValue="feld 1" colSpan={6} />
                            </FisInputGroup>
                        </FisContainerColumn>
                        <FisContainerColumn className="demo-col" colSpan={10}>
                            <FisInputGroup label="My Textarea Label" isInvalid>
                                <FisTextArea
                                    rows="5"
                                    placeholder="Name"
                                    colSpan={10}
                                    errormessage={errorsMessages.error1}
                                    defaultValue="feld 0"
                                />
                            </FisInputGroup>
                        </FisContainerColumn>
                    </FisContainerRow>

                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>mandatory</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn className="demo-col" colSpan={30}>
                            <FisInputGroup label="My Label">
                                <FisInputBase placeholder="Name" mandatory defaultValue="feld 0" colSpan={8} />
                                <FisInputDate placeholder="Date" defaultValue="feld 1" colSpan={5} />
                                <FisInputBase placeholder="Name" mandatory defaultValue="feld 0" colSpan={7} />
                                <FisInputBase infoflow placeholder="Name" defaultValue="feld 1" colSpan={3} />
                            </FisInputGroup>
                        </FisContainerColumn>
                    </FisContainerRow>

                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>InfoFlow-Label</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn className="demo-col" colSpan={30}>
                            <FisInputGroup label="My Label" infoflow>
                                <FisInputBase placeholder="Name" mandatory defaultValue="feld 0" colSpan={6} />
                                <FisInputDate placeholder="Date" defaultValue="feld 1" colSpan={6} />
                                <FisInputBase placeholder="Name" mandatory defaultValue="feld 0" colSpan={9} />
                                <FisInputBase infoflow placeholder="Name" defaultValue="feld 1" colSpan={9} />
                            </FisInputGroup>
                        </FisContainerColumn>
                    </FisContainerRow>
                </form>
            </div>
        );
    }
}
