import React from 'react';

import { FisDropdownMenu } from 'fisui.js';

export default class FisDropdownMenuPattern extends React.Component {
    render() {
        const colours = [{
            name: 'Red',
            hex: '#F21B1B'
        }, {
            name: 'Blue',
            hex: '#1B66F2'
        }, {
            name: 'Green',
            hex: '#07BA16'
        }];
        return (
        <div>
            <h1>Dropdown - Menu</h1>
            <form>
                <div className="row">
                    <div colSpan={10}>
                        <h2>
                            <span className="info">state: <strong>default</strong></span>
                        </h2>
                    </div>
                    <div colSpan={15} className="demo-col">
                        <FisDropdownMenu list={colours} selected={colours[0]} />
                    </div>
                    <div colSpan={15} className="colored-col ">
                        <FisDropdownMenu list={colours} selected={colours[0]} /></div>
                </div>
            </form>
        </div>


        );
    }
}
