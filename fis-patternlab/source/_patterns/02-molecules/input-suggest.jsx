import React from 'react';

import { FisInputSuggest } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';

export default class FisInputSuggestPattern extends React.Component {
    render() {
        return (
        <div>
            <h1>Input with dropdown-suggest</h1>
            <form>
                <FisContainerRow>
                    <FisContainerColumn colSpan={10}>
                        <h2>
                            <span className="info">state: <strong>default</strong></span>
                        </h2>
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={15} className="demo-col">
                        <FisInputSuggest id="mysuggest" placeholder="Type a programming language with C" />
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={15} className="colored-col ">
                        <FisInputSuggest id="mysuggest1" placeholder="Type  a programming language with P" />
                    </FisContainerColumn>
                </FisContainerRow>
                <FisContainerRow>
                    <FisContainerColumn colSpan={10}>
                        <h2>
                            <span className="info">state: <strong>prefilled</strong></span>
                        </h2>
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={15} className="demo-col">
                        <FisInputSuggest id="mysuggest2" defaultValue="C#" placeholder="Try to Type " />
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={15} className="colored-col ">
                        <FisInputSuggest id="mysuggest3" defaultValue="C#" placeholder="Try Type a " />
                    </FisContainerColumn>
                </FisContainerRow>
                <FisContainerRow >
                    <FisContainerColumn colSpan={10}>
                        <h2>
                            <span className="info">state: <strong>disabled</strong></span>
                        </h2>
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={15} className="demo-col">
                        <FisInputSuggest id="mysuggest4" disabled placeholder="Try to Type a programming language" />
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={15} className="colored-col ">
                        <FisInputSuggest id="mysuggest5" disabled placeholder="Try Type a programming language" />
                    </FisContainerColumn>
                </FisContainerRow>
            </form>
        </div>

        );
    }
}
