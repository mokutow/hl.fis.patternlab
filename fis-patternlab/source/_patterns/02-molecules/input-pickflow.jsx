import React from 'react';

import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';
import { FisInputPickFlow } from 'fisui.js';
import { FisInputBase } from 'fisui.js';
import { FisCheckboxRadioGroup } from 'fisui.js';
import { FisInputGroup } from 'fisui.js';

export default class FisInputPickFLowPattern extends React.Component {
    render() {
        const inputlist = [
            { name: 'Na me', checked: true, value: '1', label: 'Name', id: 't1' },
            { name: 'TmpSep', checked: false, value: '2', label: 'TmpSep', id: 't2' },
            { name: 'TranEx', checked: false, value: '3', label: 'TranEx', id: 't3' },
            { name: 'ShTy', checked: false, value: '4', label: 'ShTy', id: 't4', suffixIcon: 'fis-icon-applications' }
        ];

        const optsDefault = {
            name: 'myCheckbox1',
            label: 'Your Checkbox 1',
            disabled: false,
            mandatory: false,
            type: 'checkbox'
        };

        return (
            <div>
                <h1>Input Pick-Flow</h1>
                <form>
                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>all in one</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={21} className="demo-col">
                            <FisInputGroup label="My long label to show flexibility">
                                <FisInputPickFlow colSpan={17} >
                                    <FisInputBase placeholder="empty with placeholder" defaultValue="" colSpan={8} />
                                    <FisInputBase placeholder="Name" defaultValue="disabled" disabled colSpan={8} />
                                </FisInputPickFlow>
                                <FisCheckboxRadioGroup label="Check me" name="Name" colSpan={4} inputlist={inputlist} opts={optsDefault} />
                            </FisInputGroup>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={17} className="colored-col ">
                            <FisInputGroup label="My long label to show flexibility">
                                <FisInputPickFlow colSpan={17} className="default" >
                                    <FisInputBase placeholder="empty with placeholder" defaultValue="" colSpan={8} />
                                    <FisInputBase placeholder="Name" defaultValue="disabled" disabled colSpan={8} />
                               </FisInputPickFlow>
                            </FisInputGroup>
                        </FisContainerColumn>
                    </FisContainerRow>

                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>all in one</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={21} className="demo-col">
                            <FisInputGroup label="My long label to show flexibility">
                                <FisInputPickFlow colSpan={17} >
                                    <FisInputBase placeholder="empty with placeholder" defaultValue="" colSpan={8} />
                                    <FisInputBase placeholder="Name" defaultValue="disabled" disabled colSpan={8} />
                                </FisInputPickFlow>
                                <FisCheckboxRadioGroup label="Check me" name="Name" colSpan={4} inputlist={inputlist} opts={optsDefault} />
                            </FisInputGroup>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={17} className="colored-col ">
                            <FisInputGroup label="My long label to show flexibility">
                                <FisInputPickFlow colSpan={17} className="default" >
                                    <FisInputBase isInvalid placeholder="empty with placeholder" defaultValue="" colSpan={8} />
                                    <FisInputBase isInvalid placeholder="Name" defaultValue="disabled" disabled colSpan={8} / >
                                </FisInputPickFlow>
                            </FisInputGroup>
                        </FisContainerColumn>
                    </FisContainerRow>
                </form>
            </div>
        );
    }
}
