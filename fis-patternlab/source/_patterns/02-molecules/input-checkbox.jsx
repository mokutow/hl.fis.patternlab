import React from 'react';

import { FisCheckboxRadioGroup } from 'fisui.js';
import { FisInputGroup } from 'fisui.js';
import { FisInputBase } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';

export default class FisCheckboxRadioGroupPattern extends React.Component {
    render() {
        return (
            <div>
                <h1>Inputs [type='checkbox']</h1>
                <form>
                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>default</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="demo-col">
                            <FisCheckboxRadioGroup name="myCheckbox1" label="Your Checkbox 1" />
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="colored-col ">
                            <FisCheckboxRadioGroup name="myCheckbox1" label="Your Checkbox 1" />
                        </FisContainerColumn>
                    </FisContainerRow>
                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>disabled</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="demo-col">
                            <FisCheckboxRadioGroup name="myCheckbox1" label="Your Checkbox 1" disabled />
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="colored-col ">
                           <FisCheckboxRadioGroup name="myCheckbox1" label="Your Checkbox 1" disabled />
                        </FisContainerColumn>
                    </FisContainerRow>

                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>error</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="demo-col">
                                <FisCheckboxRadioGroup name="myCheckbox1" label="Your Checkbox 1" isInvalid />
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="colored-col ">
                            <FisCheckboxRadioGroup name="myCheckbox1" label="Your Checkbox 1" isInvalid />
                        </FisContainerColumn>
                    </FisContainerRow>

                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>stand-alone next to input-group</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="columns demo-col">
                            <FisContainerRow>
                                <FisContainerColumn colSpan={10}>
                                    <FisInputGroup label="My long label to show flexibility">
                                        <FisInputBase placeholder="empty with placeholder" defaultValue="" colSpan={5} />
                                        <FisInputBase placeholder="Name" defaultValue="disabled" disabled colSpan={5} />
                                    </FisInputGroup>
                                </FisContainerColumn>
                                <FisContainerColumn colSpan={5} >
                                    <FisCheckboxRadioGroup name="myCheckbox1" label="Your Checkbox 1" standalone />
                                </FisContainerColumn>
                            </FisContainerRow>
                        </FisContainerColumn>
                    </FisContainerRow>
                </form>
            </div>
        );
    }
}
