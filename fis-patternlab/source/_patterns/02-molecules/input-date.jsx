import React from 'react';

import { FisInputDate } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';

export default class FisInputDatePattern extends React.Component {
    render() {
        return (
          <div>
              <h1>Inputs [type='date-picker']</h1>
              <form>
                  <FisContainerRow>
                      <FisContainerColumn colSpan={10}>
                          <h2>
                              <span className="info">state: <strong>default</strong></span>
                          </h2>
                      </FisContainerColumn>
                      <FisContainerColumn colSpan={15} className="demo-col">
                        <FisInputDate
                            label="My Date"
                            errormessage="There is a real big error in this input-field. Please check it again."
                        />
                      </FisContainerColumn>
                  </FisContainerRow>
              </form>
          </div>
      );
    }
}
