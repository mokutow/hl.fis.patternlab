import React from 'react';

import { FisCheckboxRadioGroup } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';

export default class FisCheckboxRadioGroupPattern extends React.Component {
    render() {
        const inputlist = [
            { name: 'Na me', checked: true, value: '1', label: 'Name', id: 't1' },
            { name: 'TmpSep', checked: false, value: '2', label: 'TmpSep', id: 't2' },
            { name: 'TranEx', checked: false, value: '3', label: 'TranEx', id: 't3' },
            { name: 'ShTy', checked: false, value: '4', label: 'ShTy', id: 't4', suffixIcon: 'fis-icon-applications' }
        ];

        const optsDefault = {
            name: 'myCheckbox1',
            label: 'Your Checkbox 1',
            disabled: false,
            mandatory: false,
            type: 'checkbox'
        };

        const optsDisabled = {
            name: 'myCheckbox2',
            label: 'Your Checkbox 2',
            readOnly: true,
            mandatory: false,
            type: 'checkbox'
        };

        const optsError = {
            name: 'myCheckbox3',
            label: 'Your Checkbox 3',
            isInvalid: true,
            errorMessage: 'A very long Error message',
            disabled: false,
            mandatory: false,
            type: 'checkbox'
        };
        const optsMandatory = {
            name: 'myCheckbox4',
            label: 'Your Checkbox 4',
            disabled: false,
            mandatory: true,
            stacked: true,
            type: 'checkbox'
        };


        return (
            <div>
                <h1>Inputs-Group [type='checkbox']</h1>
                <form>
                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>default</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="demo-col">
                            <FisCheckboxRadioGroup inputlist={inputlist} opts={optsDefault} />
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="colored-col ">
                            <FisCheckboxRadioGroup inputlist={inputlist} opts={optsDefault} />
                        </FisContainerColumn>
                    </FisContainerRow>
                    <FisContainerRow className="row">
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>disabled</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="demo-col">
                            <FisCheckboxRadioGroup inputlist={inputlist} opts={optsDisabled} />
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="colored-col ">
                            <FisCheckboxRadioGroup inputlist={inputlist} opts={optsDisabled} />
                        </FisContainerColumn>
                    </FisContainerRow>

                    <FisContainerRow className="row">
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>error</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="demo-col">
                            <FisCheckboxRadioGroup inputlist={inputlist} opts={optsError} />
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="colored-col ">
                            <FisCheckboxRadioGroup inputlist={inputlist} opts={optsError} />
                        </FisContainerColumn>
                    </FisContainerRow>
                    <FisContainerRow className="row">
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>mandatory, stacked</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="demo-col">
                            <FisCheckboxRadioGroup inputlist={inputlist} opts={optsMandatory} />
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="colored-col ">
                            <FisCheckboxRadioGroup inputlist={inputlist} opts={optsMandatory} />
                        </FisContainerColumn>
                    </FisContainerRow>
                </form>
            </div>
        );
    }
}
