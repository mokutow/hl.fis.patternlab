import React from 'react';

import { FisText } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';

export default class FisTextPattern extends React.Component {
    render() {
        return (
            <div>
                <h1>Text</h1>
                <form>
                    <FisContainerRow>
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>default</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={10} className="demo-col">
                            <FisText>
                                MY Text is not Lorem Ipsum
                            </FisText>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={10} className="colored-col ">
                            <FisText>
                                MY Text is not Lorem Ipsum
                            </FisText>
                        </FisContainerColumn>
                    </FisContainerRow>

                </form>
            </div>
        );
    }
}
