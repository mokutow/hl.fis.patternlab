import React from 'react';

import { FisInputBase } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';

export default class FisInputBasePattern extends React.Component {
    render() {
        return (
            <div>
                <h1>Input Base</h1>
                <form>
                    <FisContainerRow className="row">
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>default</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="demo-col">
                            <FisInputBase type="number" placeholder="empty with placeholder" defaultValue="" />
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="colored-col ">
                            <FisInputBase placeholder="empty with placeholder" defaultValue="" />
                        </FisContainerColumn>
                    </FisContainerRow>
                    <FisContainerRow className="row">
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>disabled</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="demo-col">
                            <FisInputBase placeholder="empty with placeholder" defaultValue="disabled" disabled />
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="colored-col ">
                            <FisInputBase placeholder="Name" defaultValue="disabled" disabled />

                        </FisContainerColumn>
                    </FisContainerRow>
                    <FisContainerRow className="row">
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>read-only</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="demo-col">

                            <FisInputBase placeholder="Name" value="readOnly" readOnly />
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="colored-col ">
                            <FisInputBase placeholder="Name" value="readOnly" readOnly />
                        </FisContainerColumn>
                    </FisContainerRow>
                    <FisContainerRow className="row">
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>error</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="demo-col">
                            <FisInputBase
                                errormessage="There is a real big error in this input-field. Please check it again."
                                placeholder="Name"
                                defaultValue="error"
                            />
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="colored-col ">
                            <FisInputBase
                                errormessage="There is a real big error in this input-field. Please check it again."
                                placeholder="Name"
                                defaultValue="error"
                            />
                        </FisContainerColumn>
                    </FisContainerRow>
                    <FisContainerRow className="row">
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>mandatory</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="demo-col">
                            <FisInputBase placeholder="Name" mandatory />
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="colored-col ">
                            <FisInputBase placeholder="Name" mandatory />
                        </FisContainerColumn>
                    </FisContainerRow>
                    <FisContainerRow className="row">
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>prefix & suffix</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="demo-col">
                            <FisInputBase placeholder="Name" prefix="a prefix" />
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="colored-col ">
                            <FisInputBase placeholder="Name" suffix="a suffix" />
                        </FisContainerColumn>
                    </FisContainerRow>

                    <FisContainerRow className="row">
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>infoflow & suffix</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="demo-col">
                            <FisInputBase placeholder="Name" suffix="a suffix" infoflow />
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="colored-col ">
                            <FisInputBase placeholder="Name" suffix="a suffix" infoflow />
                        </FisContainerColumn>
                    </FisContainerRow>

                    <FisContainerRow className="row">
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>infoflow & suffix error</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="demo-col">
                            <FisInputBase
                                errormessage="There is a real big error in this input-field. Please check it again."
                                placeholder="Name"
                                defaultValue="error"
                                suffix="a suffix"
                                infoflow
                            />
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="colored-col ">
                            <FisInputBase
                                errormessage="There is a real big error in this input-field. Please check it again."
                                placeholder="Name"
                                suffix="a suffix"
                                defaultValue="error"
                                infoflow
                            />
                        </FisContainerColumn>
                    </FisContainerRow>
                    <FisContainerRow className="row">
                        <FisContainerColumn colSpan={10}>
                            <h2>
                                <span className="info">state: <strong>suffix icon</strong></span>
                            </h2>
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="demo-col">
                            <FisInputBase
                                placeholder="Name"
                                defaultValue="suffix icon"
                                suffixIcon="fis-icon-applications"
                            />
                        </FisContainerColumn>
                        <FisContainerColumn colSpan={15} className="colored-col ">
                            <FisInputBase
                                placeholder="Name"
                                defaultValue="suffix icon"
                                suffixIcon="fis-icon-applications"
                            />
                        </FisContainerColumn>
                    </FisContainerRow>
                </form>
            </div>
        );
    }
}
