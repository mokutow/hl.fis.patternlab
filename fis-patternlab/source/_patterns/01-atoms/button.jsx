import React from 'react';

import { FisButton } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';

export default class FisButtonPattern extends React.Component {
    render() {
        const variants =
            [
                'actionbar-default',
                'actionbar-constructive',
                'actionbar-destructive',
                'content-default',
                'content-hero'
            ];

        return (
            <div>
                <h1>Buttons</h1>

                {variants.map((variant) => {
                    const variantname = variant.replace('-', ' ');
                    return (
                        <div key={variant}>
                            <FisContainerRow>
                                <FisContainerColumn colSpan={10}>
                                    <h2>
                                        <span className="variant-demo">{variantname}</span>
                                    </h2>
                                </FisContainerColumn>
                                <FisContainerColumn colSpan={10} />
                            </FisContainerRow>
                            <FisContainerRow>
                                <FisContainerColumn colSpan={15}>
                                    <h2>
                                        <span className="info">state: <strong>default</strong></span>
                                    </h2>
                                </FisContainerColumn>
                                <FisContainerColumn colSpan={10} className="colored-col demo-btn">
                                    <FisButton variant={variant}>Button</FisButton>
                                </FisContainerColumn>
                            </FisContainerRow>
                            <FisContainerRow>
                                <FisContainerColumn colSpan={15}>
                                    <h2>
                                        <span className="info">state: <strong>hover</strong></span>
                                    </h2>
                                </FisContainerColumn>
                                <FisContainerColumn colSpan={10} className="colored-col  demo-btn">
                                    <FisButton variant={variant} className="hovered">Button</FisButton>
                                </FisContainerColumn>
                            </FisContainerRow>
                            <FisContainerRow>
                                <FisContainerColumn colSpan={15}>
                                    <h2>
                                        <span className="info">state: <strong>disabled</strong></span>
                                    </h2>
                                </FisContainerColumn>
                                <FisContainerColumn colSpan={10} className="colored-col demo-btn">
                                    <FisButton variant={variant} className="disabled">Button</FisButton>
                                </FisContainerColumn>
                            </FisContainerRow>
                            <hr />
                        </div>
                    );
                })}
            </div>
        );
    }
}
