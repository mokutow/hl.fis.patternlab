import React from 'react';

import { FisIcon } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';

export default class FisIconPattern extends React.Component {
    render() {
        return (
              <div>
                  <h2>Icons</h2>
                  <form>
                      <FisContainerRow>
                          <FisContainerColumn colSpan={10}>
                              <h2>
                                  <span className="info">state: <strong>default</strong></span>
                              </h2>
                          </FisContainerColumn>
                          <FisContainerColumn colSpan={15} className="demo-col">
                              <FisIcon icon="fis-icon-logo" >Aria-Text</FisIcon>
                              <FisIcon icon="fis-icon-logo" variant="sizeM">Aria-Text</FisIcon>
                              <FisIcon icon="fis-icon-logo" variant="sizeL">Aria-Text</FisIcon>
                              <FisIcon icon="fis-icon-logo" variant="sizeXL">Aria-Text</FisIcon>
                          </FisContainerColumn>

                      </FisContainerRow>
                  </form>
              </div>
        );
    }
}
