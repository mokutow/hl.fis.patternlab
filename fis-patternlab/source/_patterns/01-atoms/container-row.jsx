import React from 'react';

import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';

export default class FisContainerRowPattern extends React.Component {
    render() {
        return (
            <div>
                <h1>rows</h1>
                <FisContainerRow className="row-colored">
                    <FisContainerColumn colSpan={18}>Eine Reihe</FisContainerColumn>
                </FisContainerRow>

                <FisContainerRow className="row-colored">
                    <FisContainerColumn colSpan={18}>Eine Reihe</FisContainerColumn>
                </FisContainerRow>

                <FisContainerRow className="row-colored">
                    <FisContainerColumn colSpan={18}>Eine Reihe</FisContainerColumn>
                </FisContainerRow>

                <FisContainerRow className="row-colored">
                    <FisContainerColumn colSpan={18}>Eine Reihe</FisContainerColumn>
                </FisContainerRow>

                <FisContainerRow className="row-colored">
                    <FisContainerColumn colSpan={18}>Eine Reihe</FisContainerColumn>
                </FisContainerRow>
            </div>
        );
    }
}
