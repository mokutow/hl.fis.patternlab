import React from 'react';

import { FisContainerColumn } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';

export default class FisContainerColumnPattern extends React.Component {
    render() {
        return (
            <div>
                <h1>Columns</h1>
                <FisContainerRow className="row">
                    <FisContainerColumn colSpan={12} className=" columns-colored">
                        Eine Spalte 12
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={3} className="columns-colored">
                        A24
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={15} className="columns-colored">
                        Eine Spalte 15
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={20} className="columns-colored">
                        Eine Spalte 20
                    </FisContainerColumn>
                </FisContainerRow>
            </div>
        );
    }
}
