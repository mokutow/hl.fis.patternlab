module.exports = {};

// Needs require for weback expose-loader to work
const React = require('react');
module.exports.React = React;

const ReactDOM = require('react-dom');
module.exports.ReactDOM = ReactDOM;


// ---------
// Global
// ---------

import FisGridPattern from '00-globals/fis-grid.jsx';
module.exports.FisGridPattern = FisGridPattern;

import FisPalettePattern from '00-globals/palette.jsx';
module.exports.FisPalettePattern = FisPalettePattern;

import FisTypoPattern from '00-globals/typography.jsx';
module.exports.FisTypoPattern = FisTypoPattern;


// ---------
// Atoms
// ---------

import FisContainerColumnPattern from '01-atoms/container-column.jsx';
module.exports.FisContainerColumnPattern = FisContainerColumnPattern;

import FisContainerRowPattern from '01-atoms/container-row.jsx';
module.exports.FisContainerRowPattern = FisContainerRowPattern;

import FisButtonPattern from '01-atoms/button.jsx';
module.exports.FisButtonPattern = FisButtonPattern;

import FisIconPattern from '01-atoms/icon.jsx';
module.exports.FisIconPattern = FisIconPattern;

import FisTextPattern from '01-atoms/text.jsx';
module.exports.FisTextPattern = FisTextPattern;

import FisInputBasePattern from '01-atoms/input-base.jsx';
module.exports.FisInputBasePattern = FisInputBasePattern;


// ---------
// Molecules
// ---------

import FisInputGroupPattern from '02-molecules/input-group.jsx';
module.exports.FisInputGroupPattern = FisInputGroupPattern;

import FisButtonGroupPattern from '02-molecules/button-group.jsx';
module.exports.FisButtonGroupPattern = FisButtonGroupPattern;

import FisDropdownMenuPattern from '02-molecules/dropdown-menu.jsx';
module.exports.FisDropdownMenuPattern = FisDropdownMenuPattern;

import FisDropdownPanePattern from '02-molecules/dropdown-pane.jsx';
module.exports.FisDropdownPanePattern = FisDropdownPanePattern;

import FisButtonIconPattern from '02-molecules/button-icon.jsx';
module.exports.FisButtonIconPattern = FisButtonIconPattern;


import FisInputDatePattern from '02-molecules/input-date.jsx';
module.exports.FisInputDatePattern = FisInputDatePattern;

import FisInputSuggestPattern from '02-molecules/input-suggest.jsx';
module.exports.FisInputSuggestPattern = FisInputSuggestPattern;

import FisFieldsetPattern from '02-molecules/fieldset.jsx';
module.exports.FisFieldsetPattern = FisFieldsetPattern;

import FisCheckboxRadioGroupPattern from '02-molecules/input-checkbox-group.jsx';
module.exports.FisCheckboxRadioGroupPattern = FisCheckboxRadioGroupPattern;

import FisInputRadioPattern from '02-molecules/input-radio.jsx';
module.exports.FisInputRadioPattern = FisInputRadioPattern;

import FisInputPickFlowPattern from '02-molecules/input-pickflow.jsx';
module.exports.FisInputPickFlowPattern = FisInputPickFlowPattern;

import FisTextareaPattern from '02-molecules/textarea.jsx';
module.exports.FisTextareaPattern = FisTextareaPattern;


// ---------
//
// Organisms
//
// ---------
import FisTabPanelPattern from '03-organisms/tab-panel.jsx';
module.exports.FisTabPanelPattern = FisTabPanelPattern;

import FisDataGridPattern from '03-organisms/data-grid.jsx';
module.exports.FisDataGridPattern = FisDataGridPattern;

import FisDataGridPattern2 from '03-organisms/data-grid-2.jsx';
module.exports.FisDataGridPattern2 = FisDataGridPattern2;

import FisDataGridPatternX from '03-organisms/data-grid-x.jsx';
module.exports.FisDataGridPatternX = FisDataGridPatternX;

import FisStripePattern from '03-organisms/stripe.jsx';
module.exports.FisStripePattern = FisStripePattern;

import FisPanelPattern from '03-organisms/panel.jsx';
module.exports.FisPanelPattern = FisPanelPattern;

import FisClientHeaderPattern from '03-organisms/clientheader.jsx';
module.exports.FisClientHeaderPattern = FisClientHeaderPattern;

import FisLoginPattern from '05-templates/login.jsx';
module.exports.FisLoginPattern = FisLoginPattern;


// ---------
//
// Templates
//
// ---------

import FisClientPattern from '05-templates/client.jsx';
module.exports.FisClientPattern = FisClientPattern;

import S8020 from '05-templates/S8020.jsx';
module.exports.S8020 = S8020;

import E4842 from '05-templates/E4842.jsx';
module.exports.E4842 = E4842;

import T9500 from '05-templates/T9500.jsx';
module.exports.T9500 = T9500;

import T9500Cleaned from '05-templates/T9500-cleaned.jsx';
module.exports.T9500Cleaned = T9500Cleaned;

import T9500Cleaned2 from '05-templates/T9500-cleaned-and-fixed.jsx';
module.exports.T9500Cleaned2 = T9500Cleaned2;

import V7300Cleaned from '05-templates/V7300-cleaned.jsx';
module.exports.V7300Cleaned = V7300Cleaned;
