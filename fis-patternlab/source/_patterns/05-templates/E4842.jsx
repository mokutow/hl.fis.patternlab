import React from 'react';

import { FisClient } from 'fisui.js';
import { FisPanel } from 'fisui.js';
import { FisStripe } from 'fisui.js';
import { FisFieldset } from 'fisui.js';
import { FisInputGroup } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';
import { FisInputBase } from 'fisui.js';
import { FisInputDate } from 'fisui.js';
import { FisInputPickFlow } from 'fisui.js';
import { FisTabPanel } from 'fisui.js';
import { FisInputGeoHierarchy } from 'fisui.js';
import { FisInputMatchCode } from 'fisui.js';
import { FisInputEquipmentNumber } from 'fisui.js';
import { FisInputLastChange } from 'fisui.js';
import { FisDataGrid2 } from 'fisui.js';
import { FisText } from 'fisui.js';
import { FisButton } from 'fisui.js';
import { FisInputOffsetDate } from 'fisui.js';
import { FisCheckboxRadioGroup } from 'fisui.js';
import { FisTextArea } from 'fisui.js';
import { FisButtonGroup } from 'fisui.js';
// FIXME: DELETE WHEN  IS REALLY USED
/* eslint-disable */
export default class E4842 extends React.Component {
    render() {

        const user = {};
        const openAppList = {
            currentApp: {
                id: 't9500'
            }
            ,
            apps: [
                { app: { name: 'Dashboard', icon: 'fis-icon-glyphicons-home', url: '/', id: 'D000' }, noClose: true, className: 'homeBtn' },
                { app: { name: 'T9500', url: '/t9500/', id: 't9500', params: '&date=2016-01-17&end=&start=DEHAM&end=DEBRV' },
                    modal: { name: 'S8020' }, isActive: true },
                { app: { name: 'S8020', url: '/t9500/', id: 'f9500' }, isActive: false },
                { app: { name: 'E4842', url: '/t9500/', id: 'e9500' }, modal: { name: 'S8020' }, isActive: false },
                { app: { name: 'S8020', url: '/t9500/', id: 's9500' }, isActive: false },
                { app: { name: 'S8020', url: '/t9500/', id: 'r9500' }, isActive: false },
                { app: { name: 'E4842', url: '/t9500/', id: 'g9500' }, modal: { name: 'S8020' }, isActive: false }
            ]};
        const appList = [
            { id: '0', name: 'Customer Information System', sub: [
                { name: 'Bkg General 0-0', url: '0-0', sub: null },
                { name: 'Bkg Detail 0-1', url: '0-1', sub: null },
                { name: 'Bkg Special 0-2', url: '0-2', sub: null }
            ]
            },

            { id: '1', name: 'Tender Management', sub: [
                { name: 'Bkg General 2-0', url: '2-0', sub: null },
                { name: 'Bkg Detail 2-1', url: '2-1', sub: null },
                { name: 'Bkg Special 2-2', url: '2-2', sub: [
                    { name: 'lorem ipsum 2-2-0', url: '2-2-0', sub: null },
                    { name: 'lorem ipsum 2-2-1', url: '2-2-1', sub: null },
                    { name: 'lorem ipsum 2-2-2', url: '2-2-2', sub: null },
                    { name: 'lorem ipsum 2-2-3', url: '2-2-3', sub: null },
                    { name: 'lorem ipsum 2-2-4', url: '2-2-4', sub: null },
                    { name: 'lorem ipsum 2-2-5', url: '2-2-5', sub: null },
                    { name: 'lorem ipsum 2-2-6', url: '2-2-6', sub: null }
                ] },
                { name: 'lorem ipsum 2-3', url: '2-3', sub: null },
                { name: 'lorem ipsum 2-4', url: '2-4', sub: null },
                { name: 'lorem ipsum 2-5', url: '2-5', sub: null }
            ]
            },
            { id: '2', divider: true },
            { id: '3', name: 'Process Exception Advice', sub: null },
            { id: '4', name: 'Tender Management', sub: null },
            { id: '5', name: 'Sales Pricing', sub: null },
            { id: '6', name: 'CS Booking', sub: null },
            { id: '7', name: 'CS Dokumentation', sub: null },
            { id: '8', name: 'Invoicing + Finance', sub: null },
            { id: '9', name: 'CS Import', sub: null },
            { id: '10', name: 'Archive', sub: null },
            { id: '11', name: 'System Maintenance', sub: null }
        ];
        const favouriteList = [
            { id: '0', name: 'Favourites Group', sub: [
                { name: 'Bkg General 0-0', url: '0-0', sub: null },
                { name: 'Bkg Detail 0-1', url: '0-1', sub: null },
                { name: 'Bkg Special 0-2', url: '0-2', sub: null }
            ]
            },
            { id: '1', divider: true },
            { id: '2', name: 'Tender Management', sub: null },
            { id: '3', name: 'Tender Management', sub: null },
            { id: '4', name: 'Sales Pricing', sub: null },
            { id: '5', name: 'CS Booking', sub: null },
            { id: '6', name: 'CS Dokumentation', sub: null },
            { id: '7', name: 'Invoicing + Finance', sub: null },
            { id: '8', name: 'CS Import', sub: null },
            { id: '9', name: 'Archive', sub: null },
            { id: '10', name: 'System Maintenance', sub: null },
            { id: '11', divider: true },
            { id: '12', name: 'Organize Favorites', className: 'edit-favs', sub: null },
        ];


        return (
            <FisClient appsList={openAppList} applicationMenu={appList} favouriteMenu={favouriteList} user={user} >
                <form>
                    <FisPanel buttonGroupsTop={[			{ buttons: [
									{title: "Refresh", variant: "content-default"},
									{title: "Save", variant: "content-default"}
									] }
		]} buttonGroupsMiddle={[]} buttonGroupsBottom={[			{ buttons: [
									{title: "Close", variant: "content-default"},
									{title: "Cancel", variant: "content-default"}
									] }
		]}>
                        <FisStripe isOpened={true} buttonGroups={[
												{ buttons: [{title: "Clear", variant: "content-default"},{title: "Find", variant: "content-hero"}]}]}>
                            <FisContainerColumn className="flex-box" colSpan={36}>
                                <div>
                                    <FisContainerRow>
                                        <FisContainerColumn colSpan={36} className="flex-box">
                                            <FisTabPanel tabData={[
									{
										title: "MT Release",
										id: "1",
										content: (<FisContainerRow>
											<FisContainerColumn colSpan={9} className="flex-box">
												<div>
												<FisContainerRow>
													<FisContainerColumn colSpan={9} className="flex-box">
														<FisInputGroup label="Geo Hier">
															<FisInputGeoHierarchy  />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={5} className="flex-box">
														<FisInputGroup label="Location">
															<FisInputPickFlow colSpan={5}>
																<FisInputBase type="text"
																		colSpan={4} />
															</FisInputPickFlow>
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={8} className="flex-box">
														<FisInputGroup label="Depot">
															<FisInputPickFlow colSpan={8}>
																<FisInputMatchCode  />
															</FisInputPickFlow>
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={7} className="flex-box">
														<FisInputGroup label="Date From">
															<FisInputOffsetDate  />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={7} className="flex-box">
														<FisInputGroup label="Date To">
															<FisInputOffsetDate  />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												</div>
											</FisContainerColumn>
											<FisContainerColumn colSpan={8} className="flex-box">
												<div>
												<FisContainerRow>
													<FisContainerColumn colSpan={2} className="flex-box">
														<FisInputGroup label="Eq Category">
															<FisInputBase type="text"
																	colSpan={2} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={8} className="flex-box">
														<FisInputGroup label="Eq Type Group">
															<FisInputPickFlow colSpan={4}>
																<FisInputBase type="text"
																		colSpan={3} />
															</FisInputPickFlow>
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={7} className="flex-box">
														<FisInputGroup label="Eq Size Type">
															<FisInputPickFlow colSpan={4}>
																<FisInputBase type="text"
																		colSpan={3} />
															</FisInputPickFlow>
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												</div>
											</FisContainerColumn>
											<FisContainerColumn colSpan={6} className="flex-box">
												<div>
												<FisContainerRow>
													<FisContainerColumn colSpan={6} className="flex-box">
														<FisInputGroup label="Shipment">
															<FisInputPickFlow colSpan={6}>
																<FisInputBase type="number"
																	colSpan={5}
																	 />
															</FisInputPickFlow>
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={3} className="flex-box">
														<FisInputGroup label="Shipment Type">
															<FisInputBase type="text"
																	colSpan={3} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={6} className="flex-box">
														<FisInputGroup label="DP Voyage">
															<FisInputPickFlow colSpan={5}>
																<FisInputBase type="number"
																	colSpan={4}
																	 />
															</FisInputPickFlow>
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={3} className="flex-box">
														<FisInputGroup label="MoT">
															<FisInputBase type="text"
																	colSpan={3} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={3} className="flex-box">
														<FisInputGroup label="Haulage">
															<FisInputBase type="text"
																	colSpan={3} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												</div>
											</FisContainerColumn>
											<FisContainerColumn colSpan={2} className="flex-box">
												<div>
												<FisContainerRow>
													<FisContainerColumn colSpan={2} className="flex-box">
														<FisInputGroup label="ED State">
															<FisInputBase type="text"
																	colSpan={2} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={2} className="flex-box">
														<FisInputGroup label="Actual Moved">
															<FisInputBase type="text"
																	colSpan={2} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={2} className="flex-box">
														<FisInputGroup label="EDI Shipment">
															<FisInputBase type="text"
																	colSpan={2} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												</div>
											</FisContainerColumn>
											<FisContainerColumn colSpan={11} className="flex-box">
												<div>
												<FisContainerRow>
													<FisContainerColumn colSpan={2} className="flex-box">
														<FisInputGroup label="Reference Type">
															<FisInputBase type="text"
																	colSpan={2} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={11} className="flex-box">
														<FisInputGroup label="Reference">
															<FisInputBase type="text"
																	colSpan={11} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												</div>
											</FisContainerColumn>
										</FisContainerRow>
										)
									},
									{
										title: "MT Return",
										id: "2",
										content: (<FisContainerRow>
											<FisContainerColumn colSpan={9} className="flex-box">
												<div>
												<FisContainerRow>
													<FisContainerColumn colSpan={9} className="flex-box">
														<FisInputGroup label="Geo Hier">
															<FisInputGeoHierarchy  />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={5} className="flex-box">
														<FisInputGroup label="Location">
															<FisInputPickFlow colSpan={5}>
																<FisInputBase type="text"
																		colSpan={4} />
															</FisInputPickFlow>
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={8} className="flex-box">
														<FisInputGroup label="Depot">
															<FisInputPickFlow colSpan={8}>
																<FisInputMatchCode  />
															</FisInputPickFlow>
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={7} className="flex-box">
														<FisInputGroup label="Date From">
															<FisInputOffsetDate  />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={7} className="flex-box">
														<FisInputGroup label="Date To">
															<FisInputOffsetDate  />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												</div>
											</FisContainerColumn>
											<FisContainerColumn colSpan={8} className="flex-box">
												<div>
												<FisContainerRow>
													<FisContainerColumn colSpan={2} className="flex-box">
														<FisInputGroup label="Eq Category">
															<FisInputBase type="text"
																	colSpan={2} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={8} className="flex-box">
														<FisInputGroup label="Eq Type Group">
															<FisInputPickFlow colSpan={4}>
																<FisInputBase type="text"
																		colSpan={3} />
															</FisInputPickFlow>
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={7} className="flex-box">
														<FisInputGroup label="Eq Size Type">
															<FisInputPickFlow colSpan={4}>
																<FisInputBase type="text"
																		colSpan={3} />
															</FisInputPickFlow>
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={8} className="flex-box">
														<FisInputGroup label="Eq Number">
															<FisInputEquipmentNumber
																 />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												</div>
											</FisContainerColumn>
											<FisContainerColumn colSpan={6} className="flex-box">
												<div>
												<FisContainerRow>
													<FisContainerColumn colSpan={6} className="flex-box">
														<FisInputGroup label="Shipment">
															<FisInputPickFlow colSpan={6}>
																<FisInputBase type="number"
																	colSpan={5}
																	 />
															</FisInputPickFlow>
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={3} className="flex-box">
														<FisInputGroup label="Shipment Type">
															<FisInputBase type="text"
																	colSpan={3} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={6} className="flex-box">
														<FisInputGroup label="DP Voyage">
															<FisInputPickFlow colSpan={5}>
																<FisInputBase type="number"
																	colSpan={4}
																	 />
															</FisInputPickFlow>
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={3} className="flex-box">
														<FisInputGroup label="MoT">
															<FisInputBase type="text"
																	colSpan={3} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={3} className="flex-box">
														<FisInputGroup label="Haulage">
															<FisInputBase type="text"
																	colSpan={3} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												</div>
											</FisContainerColumn>
											<FisContainerColumn colSpan={2} className="flex-box">
												<div>
												<FisContainerRow>
													<FisContainerColumn colSpan={2} className="flex-box">
														<FisInputGroup label="ED State">
															<FisInputBase type="text"
																	colSpan={2} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={2} className="flex-box">
														<FisInputGroup label="Actual Moved">
															<FisInputBase type="text"
																	colSpan={2} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												</div>
											</FisContainerColumn>
											<FisContainerColumn colSpan={11} className="flex-box">
												<div>
												<FisContainerRow>
													<FisContainerColumn colSpan={2} className="flex-box">
														<FisInputGroup label="Reference Type">
															<FisInputBase type="text"
																	colSpan={2} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={11} className="flex-box">
														<FisInputGroup label="Reference">
															<FisInputBase type="text"
																	colSpan={11} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												</div>
											</FisContainerColumn>
										</FisContainerRow>
										)
									}
								]} />
                                        </FisContainerColumn>
                                    </FisContainerRow>
                                </div>
                            </FisContainerColumn>
                        </FisStripe>
                        <FisStripe isOpened={true} buttonGroups={[			{ buttons: [
										{title: "Excel Download", variant: "content-default"},
										{title: "EPOS", variant: "content-default"},
										{title: "Last Shipment", variant: "content-default"},
										{title: "D/O Manager", variant: "content-default"}
										] },
						{ buttons: [
										{title: "Details", variant: "content-default"},
										{title: "Mass Update", variant: "content-default"},
										{title: "Propose Depot", variant: "content-default"},
										{title: "Confirm", variant: "content-default"},
										{title: "Withdraw", variant: "content-default"},
										{title: "Send Prea", variant: "content-default"},
										{title: "Cancel Prea", variant: "content-default"},
										{title: "Preadvice All", variant: "content-default"}
										] }
			]}>
                            <FisContainerColumn className="flex-box" colSpan={100}>
                                <div>
                                    <FisContainerRow>
                                        <FisContainerColumn colSpan={100} className="flex-box">
                                            <FisDataGrid2 columnDefs={[{ headerName:"Location", field:"location", width: 83 },{ headerName:"Planned Depot", field:"planned depot", width: 119 },{ headerName:"Eq!Type", field:"eq!type", width: 68 },{ headerName:"Eq Number", field:"eq number", width: 133 },{ headerName:"Planned Date", field:"planned date", width: 113 },{ headerName:"ED!State", field:"ed!state", width: 65 },{ headerName:"A!C", field:"a!c", width: 53 },{ headerName:"Planned!By", field:"planned!by", width: 83 },{ headerName:"Prea!State", field:"prea!state", width: 65 },{ headerName:"Actual!Moved", field:"actual!moved", width: 70 },{ headerName:"Reference", field:"reference", width: 113 },{ headerName:"Type", field:"type", width: 65 },{ headerName:"Shipment", field:"shipment", width: 113 },{ headerName:"Main!Type", field:"main!type", width: 63 },{ headerName:"Haul.", field:"haul.", width: 66 },{ headerName:"MoT", field:"mot", width: 60 },{ headerName:"DP Voyage", field:"dp voyage", width: 97 },{ headerName:"Remark", field:"remark", width: 53 },{ headerName:"Seq.", field:"seq.", width: 68 },{ headerName:"Spec!Prod", field:"spec!prod", width: 68 },{ headerName:"Container Remark", field:"container remark", width: 183 },{ headerName:"Actual Depot", field:"actual depot", width: 119 },{ headerName:"Usage!Details", field:"usage!details", width: 84 }
						]} rowData={[{ "index": "1", "location": " ",
						"planned depot": " ",
						"eq!type": " ",
						"eq number": " ",
						"planned date": " ",
						"ed!state": " ",
						"a!c": " ",
						"planned!by": " ",
						"prea!state": " ",
						"actual!moved": " ",
						"reference": " ",
						"type": " ",
						"shipment": " ",
						"main!type": " ",
						"haul.": " ",
						"mot": " ",
						"dp voyage": " ",
						"remark": " ",
						"seq.": " ",
						"spec!prod": " ",
						"container remark": " ",
						"actual depot": " ",
						"usage!details": " "
						 },
						{ "index": "2", "location": " ",
						"planned depot": " ",
						"eq!type": " ",
						"eq number": " ",
						"planned date": " ",
						"ed!state": " ",
						"a!c": " ",
						"planned!by": " ",
						"prea!state": " ",
						"actual!moved": " ",
						"reference": " ",
						"type": " ",
						"shipment": " ",
						"main!type": " ",
						"haul.": " ",
						"mot": " ",
						"dp voyage": " ",
						"remark": " ",
						"seq.": " ",
						"spec!prod": " ",
						"container remark": " ",
						"actual depot": " ",
						"usage!details": " "
						 },
						{ "index": "3", "location": " ",
						"planned depot": " ",
						"eq!type": " ",
						"eq number": " ",
						"planned date": " ",
						"ed!state": " ",
						"a!c": " ",
						"planned!by": " ",
						"prea!state": " ",
						"actual!moved": " ",
						"reference": " ",
						"type": " ",
						"shipment": " ",
						"main!type": " ",
						"haul.": " ",
						"mot": " ",
						"dp voyage": " ",
						"remark": " ",
						"seq.": " ",
						"spec!prod": " ",
						"container remark": " ",
						"actual depot": " ",
						"usage!details": " "
						 },
						{ "index": "4", "location": " ",
						"planned depot": " ",
						"eq!type": " ",
						"eq number": " ",
						"planned date": " ",
						"ed!state": " ",
						"a!c": " ",
						"planned!by": " ",
						"prea!state": " ",
						"actual!moved": " ",
						"reference": " ",
						"type": " ",
						"shipment": " ",
						"main!type": " ",
						"haul.": " ",
						"mot": " ",
						"dp voyage": " ",
						"remark": " ",
						"seq.": " ",
						"spec!prod": " ",
						"container remark": " ",
						"actual depot": " ",
						"usage!details": " "
						 }
						]} colSpan={100} />
                                        </FisContainerColumn>
                                    </FisContainerRow>
                                </div>
                            </FisContainerColumn>
                        </FisStripe>
                        <FisStripe isOpened={true} buttonGroups={[]}>
                            <FisContainerColumn className="flex-box" colSpan={100}>
                                <div>
                                    <FisContainerRow>
                                        <FisContainerColumn colSpan={100} className="flex-box">
                                            <FisTabPanel tabData={[
									{
										title: "Details",
										id: "1",
										content: (<FisContainerRow>
											<FisContainerColumn colSpan={24} className="flex-box">
												<div>
												<FisContainerRow>
													<FisContainerColumn colSpan={15} className="flex-box">
														<FisContainerRow>
															<FisContainerColumn colSpan={9} className="flex-box">
																<FisCheckboxRadioGroup inputlist={[
																	{ label: "Release" },
																	{ label: "Return" }
																]} opts={{type: "radio", label: " ", stacked: false, mandatory: false, name: "EDetailsScuType_ShortInteger"}} />
															</FisContainerColumn>
															<FisContainerColumn colSpan={3} className="flex-box">
																<FisInputGroup label="Eq TG">
																	<FisInputBase type="text"
																			readOnly
																			infoflow
																			colSpan={3} />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={3} className="flex-box">
																<FisInputGroup label="Eq ST">
																	<FisInputBase type="text"
																			readOnly
																			infoflow
																			colSpan={3} />
																</FisInputGroup>
															</FisContainerColumn>
														</FisContainerRow>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={18} className="flex-box">
														<FisFieldset legend="Reference" >
														<FisContainerRow>
															<FisContainerColumn colSpan={2} >
																<FisInputGroup label=" ">
																	<FisInputBase type="text"
																			colSpan={2} />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={11} >
																<FisInputGroup label=" ">
																	<FisInputBase type="text"
																			colSpan={11} />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={4} >
																<FisInputGroup label="Expiration Date">
																	<FisInputDate colSpan={4}   />
																</FisInputGroup>
															</FisContainerColumn>
														</FisContainerRow>
														</FisFieldset>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={24} className="flex-box">
														<FisContainerRow>
															<FisContainerColumn colSpan={12} className="flex-box">
																<FisFieldset legend="General Remarks" >
																<FisContainerRow>
																	<FisContainerColumn colSpan={11} >
																		<FisInputGroup label=" ">
																			<FisTextArea rows={4}
																					colSpan={11}
																					 />
																		</FisInputGroup>
																	</FisContainerColumn>
																</FisContainerRow>
																</FisFieldset>
															</FisContainerColumn>
															<FisContainerColumn colSpan={12} className="flex-box">
																<FisFieldset legend="Container Remarks" >
																<FisContainerRow>
																	<FisContainerColumn colSpan={11} >
																		<FisInputGroup label=" ">
																			<FisTextArea rows={4}
																					colSpan={11}
																					 />
																		</FisInputGroup>
																	</FisContainerColumn>
																</FisContainerRow>
																</FisFieldset>
															</FisContainerColumn>
														</FisContainerRow>
													</FisContainerColumn>
												</FisContainerRow>
												</div>
											</FisContainerColumn>
											<FisContainerColumn colSpan={10} className="flex-box">
												<FisFieldset legend="Planned" >
												<FisContainerRow>
													<FisContainerColumn colSpan={9} >
														<FisInputGroup label="Geo">
															<FisInputGeoHierarchy  />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={4} >
														<FisInputGroup label="Location">
															<FisInputBase type="text"
																	readOnly
																	infoflow
																	colSpan={4} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={8} >
														<FisInputGroup label="Depot">
															<FisInputPickFlow colSpan={8}>
																<FisInputMatchCode  />
															</FisInputPickFlow>
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={9} >
														<FisInputGroup label="Eq No">
															<FisInputPickFlow colSpan={9}>
																<FisInputEquipmentNumber
																	 />
															</FisInputPickFlow>
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={4} >
														<FisInputGroup label="Date">
															<FisInputDate colSpan={4}   />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={3} >
														<FisInputGroup label="Time">
															<FisInputBase type="time" colSpan={3}   />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												</FisFieldset>
											</FisContainerColumn>
											<FisContainerColumn colSpan={10} className="flex-box">
												<FisFieldset legend="Actual" >
												<FisContainerRow>
													<FisContainerColumn colSpan={9} >
														<FisInputGroup label="Geo">
															<FisInputGeoHierarchy  />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={4} >
														<FisInputGroup label="Location">
															<FisInputBase type="text"
																	readOnly
																	infoflow
																	colSpan={4} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={8} >
														<FisInputGroup label="Depot">
															<FisInputPickFlow colSpan={8}>
																<FisInputMatchCode  />
															</FisInputPickFlow>
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={8} >
														<FisInputGroup label="Eq No">
															<FisInputEquipmentNumber
																infoflow />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={4} >
														<FisInputGroup label="Date">
															<FisInputDate colSpan={4} readOnly  />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={3} >
														<FisInputGroup label="Time">
															<FisInputBase type="time" colSpan={3} readOnly  />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												</FisFieldset>
											</FisContainerColumn>
											<FisContainerColumn colSpan={8} className="flex-box">
												<FisFieldset legend="Status" >
												<FisContainerRow>
													<FisContainerColumn colSpan={2} >
														<FisInputGroup label="ED State">
															<FisInputBase type="text"
																	readOnly
																	colSpan={2} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={2} >
														<FisInputGroup label="Prea State">
															<FisInputBase type="text"
																	colSpan={2} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={2} >
														<FisInputGroup label="Actual Moved">
															<FisInputBase type="text"
																	colSpan={2} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={4} >
														<FisInputGroup label="Created">
															<FisInputDate colSpan={4} readOnly  />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={7} >
														<FisInputGroup label="Last Change">
															<FisInputLastChange />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={5} >
														<FisInputGroup label="Changed By">
															<FisInputBase type="text"
																	readOnly
																	colSpan={5} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												</FisFieldset>
											</FisContainerColumn>
										</FisContainerRow>
										)
									},
									{
										title: "Shipment",
										id: "2",
										content: (<FisContainerRow>
											<FisContainerColumn colSpan={16} className="flex-box">
												<div>
												<FisContainerRow>
													<FisContainerColumn colSpan={7} className="flex-box">
														<FisFieldset legend="Shipment" >
														<FisContainerRow>
															<FisContainerColumn colSpan={6} >
																<FisContainerRow>
																	<FisContainerColumn colSpan={2} >
																		<FisInputGroup label="Type">
																			<FisInputBase type="text"
																					readOnly
																					colSpan={2} />
																		</FisInputGroup>
																	</FisContainerColumn>
																	<FisContainerColumn colSpan={2} >
																		<FisInputGroup label="Haulage">
																			<FisInputBase type="text"
																					readOnly
																					colSpan={2} />
																		</FisInputGroup>
																	</FisContainerColumn>
																	<FisContainerColumn colSpan={2} >
																		<FisCheckboxRadioGroup colSpan={2} inputlist={[
																			{ label: "US", readOnly: true, name: "EDetailsUsFlag_Flag" }
																		]} opts={{type: "checkbox", label: "", stacked: false, name: "EDetailsUsFlag_FlagGroup" , standalone: true}} />
																	</FisContainerColumn>
																</FisContainerRow>
															</FisContainerColumn>
														</FisContainerRow>
														</FisFieldset>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={16} className="flex-box">
														<FisFieldset legend="Shipment Remark" infoflow>
														<FisContainerRow>
															<FisContainerColumn colSpan={11} >
																<div>
																<FisContainerRow>
																	<FisContainerColumn colSpan={11} >
																		<FisInputGroup label=" ">
																			<FisTextArea rows={4}
																					colSpan={11}
																					readOnly />
																		</FisInputGroup>
																	</FisContainerColumn>
																</FisContainerRow>
																<FisContainerRow>
																	<FisContainerColumn colSpan={11} >
																		<FisCheckboxRadioGroup colSpan={11} inputlist={[
																			{ label: "More Remarks exist", readOnly: true, name: "EDetailsMoreRemarkExists_Flag" }
																		]} opts={{type: "checkbox", label: "", stacked: true, name: "EDetailsMoreRemarkExists_FlagGroup" , standalone: true}} />
																	</FisContainerColumn>
																</FisContainerRow>
																</div>
															</FisContainerColumn>
														</FisContainerRow>
														</FisFieldset>
													</FisContainerColumn>
												</FisContainerRow>
												</div>
											</FisContainerColumn>
											<FisContainerColumn colSpan={37} className="flex-box">
												<FisFieldset legend="Routing Detail" infoflow>
												<FisContainerRow>
													<FisContainerColumn colSpan={36} >
														<div>
														<FisContainerRow>
															<FisContainerColumn colSpan={36} >
																<FisInputGroup label=" ">
																	<FisInputBase type="text"
																			readOnly
																			colSpan={36} />
																</FisInputGroup>
															</FisContainerColumn>
														</FisContainerRow>
														</div>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={19} >
														<FisContainerRow>
															<FisContainerColumn colSpan={4} >
																<FisInputGroup label="Cut-off Date">
																	<FisInputDate colSpan={4} readOnly  />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={4} >
																<FisInputGroup label="First Dp Voyage">
																	<FisInputBase type="number"
																		colSpan={4}
																		readOnly
																		infoflow />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={11} >
																<FisInputGroup label="Vessel">
																	<FisInputBase type="text"
																			readOnly
																			colSpan={11} />
																</FisInputGroup>
															</FisContainerColumn>
														</FisContainerRow>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={32} >
														<FisFieldset legend="Party Functions" infoflow>
														<FisContainerRow>
															<FisContainerColumn colSpan={7} >
																<FisInputGroup label="CU">
																	<FisInputMatchCode  />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={7} >
																<FisInputGroup label="MR">
																	<FisInputMatchCode  />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={7} >
																<FisInputGroup label="SH">
																	<FisInputMatchCode  />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={7} >
																<FisInputGroup label="CN">
																	<FisInputMatchCode  />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={3} >
																<FisCheckboxRadioGroup colSpan={3} inputlist={[
																	{ label: "More", readOnly: true, name: "EDetailsMorePfExists_Flag" }
																]} opts={{type: "checkbox", label: "", stacked: false, name: "EDetailsMorePfExists_FlagGroup" , standalone: true}} />
															</FisContainerColumn>
														</FisContainerRow>
														</FisFieldset>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={32} >
														<FisContainerRow>
															<FisContainerColumn colSpan={19} >
																<FisFieldset legend="Shipment Eq Detail" >
																<FisContainerRow>
																	<FisContainerColumn colSpan={14} >
																		<FisDataGrid2 columnDefs={[{ headerName:"Type", field:"type", width: 68 },{ headerName:"Vol", field:"vol", width: 61 },{ headerName:"ED C", field:"ed c", width: 66 },{ headerName:"PA S", field:"pa s", width: 63 },{ headerName:"AM", field:"am", width: 61 }
																		]} rowData={[{ "index": "1", "type": " ",
																		"vol": " ",
																		"ed c": " ",
																		"pa s": " ",
																		"am": " "
																		 },
																		{ "index": "2", "type": " ",
																		"vol": " ",
																		"ed c": " ",
																		"pa s": " ",
																		"am": " "
																		 },
																		{ "index": "3", "type": " ",
																		"vol": " ",
																		"ed c": " ",
																		"pa s": " ",
																		"am": " "
																		 },
																		{ "index": "4", "type": " ",
																		"vol": " ",
																		"ed c": " ",
																		"pa s": " ",
																		"am": " "
																		 }
																		]} colSpan={14} />
																	</FisContainerColumn>
																</FisContainerRow>
																</FisFieldset>
															</FisContainerColumn>
															<FisContainerColumn colSpan={13} >
																<FisFieldset legend="Cargo Detail" infoflow>
																<FisContainerRow>
																	<FisContainerColumn colSpan={8} >
																		<FisDataGrid2 columnDefs={[{ headerName:"DG", field:"dg", width: 53 },{ headerName:"Description", field:"description", width: 100 },{ headerName:"IMO!Class", field:"imo!class", width: 70 },{ headerName:"HS Code", field:"hs code", width: 106 },{ headerName:"Gross!Weight", field:"gross!weight", width: 93 },{ headerName:"Unit!Weight", field:"unit!weight", width: 75 }
																		]} rowData={[{ "index": "1", "dg": " ",
																		"description": " ",
																		"imo!class": " ",
																		"hs code": " ",
																		"gross!weight": " ",
																		"unit!weight": " "
																		 },
																		{ "index": "2", "dg": " ",
																		"description": " ",
																		"imo!class": " ",
																		"hs code": " ",
																		"gross!weight": " ",
																		"unit!weight": " "
																		 },
																		{ "index": "3", "dg": " ",
																		"description": " ",
																		"imo!class": " ",
																		"hs code": " ",
																		"gross!weight": " ",
																		"unit!weight": " "
																		 },
																		{ "index": "4", "dg": " ",
																		"description": " ",
																		"imo!class": " ",
																		"hs code": " ",
																		"gross!weight": " ",
																		"unit!weight": " "
																		 }
																		]} colSpan={8} />
																	</FisContainerColumn>
																</FisContainerRow>
																</FisFieldset>
															</FisContainerColumn>
														</FisContainerRow>
													</FisContainerColumn>
												</FisContainerRow>
												</FisFieldset>
											</FisContainerColumn>
											<FisContainerColumn colSpan={14} className="flex-box">
												<FisFieldset legend="Reefer Setting" infoflow>
												<FisContainerRow>
													<FisContainerColumn colSpan={7} >
														<FisContainerRow>
															<FisContainerColumn colSpan={7} >
																<FisInputGroup label="Temperature">
																		<FisInputBase type="number"
																			colSpan={4}
																			readOnly
																			 />
																		<FisInputBase type="text"
																				readOnly
																				colSpan={2} />
																</FisInputGroup>
															</FisContainerColumn>
														</FisContainerRow>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={7} >
														<FisContainerRow>
															<FisContainerColumn colSpan={3} >
																<FisInputGroup label="Fresh Air Supply">
																	<FisInputBase type="number"
																		colSpan={3}
																		readOnly
																		 />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={4} >
																<FisText>cbm/h</FisText>
															</FisContainerColumn>
														</FisContainerRow>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={5} >
														<FisContainerRow>
															<FisContainerColumn colSpan={3} >
																<FisInputGroup label="Ventilation Opening">
																	<FisInputBase type="number"
																		colSpan={3}
																		readOnly
																		 />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={2} >
																<FisText>%</FisText>
															</FisContainerColumn>
														</FisContainerRow>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={11} >
														<FisContainerRow>
															<FisContainerColumn colSpan={3} >
																<FisInputGroup label="Dehumidification">
																	<FisInputBase type="number"
																		colSpan={3}
																		readOnly
																		 />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={8} >
																<FisCheckboxRadioGroup colSpan={8} inputlist={[
																	{ label: "Not Applicable", readOnly: true, name: "EDetailsReefer_DoNotDehumidify" }
																]} opts={{type: "checkbox", label: "%", stacked: false, name: "HLLabel6Group" }} />
															</FisContainerColumn>
														</FisContainerRow>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={13} >
														<FisInputGroup label="Special Atmosphere">
															<FisInputBase type="text"
																	readOnly
																	colSpan={13} />
														</FisInputGroup>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={12} >
														<FisContainerRow>
															<FisContainerColumn colSpan={2} >
																<FisInputGroup label="O2">
																	<FisInputBase type="number"
																		colSpan={2}
																		readOnly
																		 />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={2} >
																<FisText>%</FisText>
															</FisContainerColumn>
															<FisContainerColumn colSpan={2} >
																<FisInputGroup label="CO2">
																	<FisInputBase type="number"
																		colSpan={2}
																		readOnly
																		 />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={2} >
																<FisText>%</FisText>
															</FisContainerColumn>
															<FisContainerColumn colSpan={2} >
																<FisInputGroup label="N">
																	<FisInputBase type="number"
																		colSpan={2}
																		readOnly
																		 />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={2} >
																<FisText>%</FisText>
															</FisContainerColumn>
														</FisContainerRow>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={4} >
														<FisContainerRow>
															<FisContainerColumn colSpan={2} >
																<FisInputGroup label="Ventilation Delay">
																	<FisInputBase type="number"
																		colSpan={2}
																		readOnly
																		 />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={2} >
																<FisText>h</FisText>
															</FisContainerColumn>
														</FisContainerRow>
													</FisContainerColumn>
												</FisContainerRow>
												<FisContainerRow>
													<FisContainerColumn colSpan={4} >
														<FisContainerRow>
															<FisContainerColumn colSpan={2} >
																<FisInputGroup label="Humidity Control">
																	<FisInputBase type="number"
																		colSpan={2}
																		readOnly
																		 />
																</FisInputGroup>
															</FisContainerColumn>
															<FisContainerColumn colSpan={2} >
																<FisText>%</FisText>
															</FisContainerColumn>
														</FisContainerRow>
													</FisContainerColumn>
												</FisContainerRow>
												</FisFieldset>
											</FisContainerColumn>
										</FisContainerRow>
										)
									},
									{
										title: "Preadvice",
										id: "3",
										content: (<div>
										<FisContainerRow>
											<FisContainerColumn colSpan={7} className="flex-box">
												<FisContainerRow>
													<FisContainerColumn colSpan={7} className="flex-box">
														<FisDataGrid2 columnDefs={[{ headerName:"Type", field:"type", width: 96 },{ headerName:"Status", field:"status", width: 124 },{ headerName:"Depot", field:"depot", width: 119 },{ headerName:"Eq Number", field:"eq number", width: 133 },{
																	headerName: "Planned",
																	children: [
																		{ headerName:"Date", field:"date", width: 113 },
																		{ headerName:"Time", field:"time", width: 103 }
																	]
																},{ headerName:"Reference", field:"reference", width: 113 },{
																	headerName: "Created",
																	children: [
																		{ headerName:"By", field:"by", width: 96 },
																		{ headerName:"Date", field:"date", width: 113 },
																		{ headerName:"Time", field:"time", width: 113 }
																	]
																},{
																	headerName: "Transmission",
																	children: [
																		{ headerName:"Date", field:"date", width: 113 },
																		{ headerName:"Time", field:"time", width: 103 }
																	]
																}
														]} rowData={[{ "index": "1", "type": " ",
														"status": " ",
														"depot": " ",
														"eq number": " ",
														"date": " ",
														"time": " ",
														"reference": " ",
														"by": " ",
														"date": " ",
														"time": " ",
														"date": " ",
														"time": " "
														 },
														{ "index": "2", "type": " ",
														"status": " ",
														"depot": " ",
														"eq number": " ",
														"date": " ",
														"time": " ",
														"reference": " ",
														"by": " ",
														"date": " ",
														"time": " ",
														"date": " ",
														"time": " "
														 },
														{ "index": "3", "type": " ",
														"status": " ",
														"depot": " ",
														"eq number": " ",
														"date": " ",
														"time": " ",
														"reference": " ",
														"by": " ",
														"date": " ",
														"time": " ",
														"date": " ",
														"time": " "
														 },
														{ "index": "4", "type": " ",
														"status": " ",
														"depot": " ",
														"eq number": " ",
														"date": " ",
														"time": " ",
														"reference": " ",
														"by": " ",
														"date": " ",
														"time": " ",
														"date": " ",
														"time": " "
														 }
														]} colSpan={7} />
													</FisContainerColumn>
												</FisContainerRow>
											</FisContainerColumn>
										</FisContainerRow>
										<FisContainerRow>
											<FisContainerColumn colSpan={23} className="flex-box">
												<FisContainerRow>
													<FisContainerColumn colSpan={23} className="flex-box">
														<FisButtonGroup colSpan={23}>
															<FisButton variant="content-default" colSpan={11}>Preadvice Details</FisButton>
															<FisButton variant="content-default" colSpan={12}>Transmission Details</FisButton>
														</FisButtonGroup>
													</FisContainerColumn>
												</FisContainerRow>
											</FisContainerColumn>
										</FisContainerRow>
										</div>
										)
									}
								]} />
                                        </FisContainerColumn>
                                    </FisContainerRow>
                                </div>
                            </FisContainerColumn>
                        </FisStripe>
                    </FisPanel>
                </form>
            </FisClient>
        );
    }
}
