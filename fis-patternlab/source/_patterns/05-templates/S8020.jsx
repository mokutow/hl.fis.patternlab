import React from 'react';

import { FisClient } from 'fisui.js';
import { FisPanel } from 'fisui.js';
import { FisStripe } from 'fisui.js';
import { FisFieldset } from 'fisui.js';
import { FisInputGroup } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';
import { FisInputBase } from 'fisui.js';
import { FisInputDate } from 'fisui.js';
import { FisInputPickFlow } from 'fisui.js';
import { FisCheckboxRadioGroup } from 'fisui.js';
import { FisInputMatchCode } from 'fisui.js';
import { FisText } from 'fisui.js';
import { FisButton } from 'fisui.js';
import { FisInputHarmonizedSystemCode } from 'fisui.js';
import { FisButtonIcon } from 'fisui.js';
import { FisTextArea } from 'fisui.js';
import { FisButtonGroup } from 'fisui.js';
// FIXME: DELETE WHEN  IS REALLY USED
/* eslint-disable */
export default class S8020 extends React.Component {
    render() {
        const user = {};
        const openAppList = {
            currentApp: {
                id: 't9500'
            }
            ,
            apps: [
                { app: { name: 'Dashboard', icon: 'fis-icon-glyphicons-home', url: '/', id: 'D000' }, noClose: true, className: 'homeBtn' },
                { app: { name: 'T9500', url: '/t9500/', id: 't9500', params: '&date=2016-01-17&end=&start=DEHAM&end=DEBRV' },
                    modal: { name: 'S8020' }, isActive: true },
                { app: { name: 'S8020', url: '/t9500/', id: 'f9500' }, isActive: false },
                { app: { name: 'E4842', url: '/t9500/', id: 'e9500' }, modal: { name: 'S8020' }, isActive: false },
                { app: { name: 'S8020', url: '/t9500/', id: 's9500' }, isActive: false },
                { app: { name: 'S8020', url: '/t9500/', id: 'r9500' }, isActive: false },
                { app: { name: 'E4842', url: '/t9500/', id: 'g9500' }, modal: { name: 'S8020' }, isActive: false }
            ]};
        const appList = [
            { id: '0', name: 'Customer Information System', sub: [
                { name: 'Bkg General 0-0', url: '0-0', sub: null },
                { name: 'Bkg Detail 0-1', url: '0-1', sub: null },
                { name: 'Bkg Special 0-2', url: '0-2', sub: null }
            ]
            },

            { id: '1', name: 'Tender Management', sub: [
                { name: 'Bkg General 2-0', url: '2-0', sub: null },
                { name: 'Bkg Detail 2-1', url: '2-1', sub: null },
                { name: 'Bkg Special 2-2', url: '2-2', sub: [
                    { name: 'lorem ipsum 2-2-0', url: '2-2-0', sub: null },
                    { name: 'lorem ipsum 2-2-1', url: '2-2-1', sub: null },
                    { name: 'lorem ipsum 2-2-2', url: '2-2-2', sub: null },
                    { name: 'lorem ipsum 2-2-3', url: '2-2-3', sub: null },
                    { name: 'lorem ipsum 2-2-4', url: '2-2-4', sub: null },
                    { name: 'lorem ipsum 2-2-5', url: '2-2-5', sub: null },
                    { name: 'lorem ipsum 2-2-6', url: '2-2-6', sub: null }
                ] },
                { name: 'lorem ipsum 2-3', url: '2-3', sub: null },
                { name: 'lorem ipsum 2-4', url: '2-4', sub: null },
                { name: 'lorem ipsum 2-5', url: '2-5', sub: null }
            ]
            },
            { id: '2', divider: true },
            { id: '3', name: 'Process Exception Advice', sub: null },
            { id: '4', name: 'Tender Management', sub: null },
            { id: '5', name: 'Sales Pricing', sub: null },
            { id: '6', name: 'CS Booking', sub: null },
            { id: '7', name: 'CS Dokumentation', sub: null },
            { id: '8', name: 'Invoicing + Finance', sub: null },
            { id: '9', name: 'CS Import', sub: null },
            { id: '10', name: 'Archive', sub: null },
            { id: '11', name: 'System Maintenance', sub: null }
        ];
        const favouriteList = [
            { id: '0', name: 'Favourites Group', sub: [
                { name: 'Bkg General 0-0', url: '0-0', sub: null },
                { name: 'Bkg Detail 0-1', url: '0-1', sub: null },
                { name: 'Bkg Special 0-2', url: '0-2', sub: null }
            ]
            },
            { id: '1', divider: true },
            { id: '2', name: 'Tender Management', sub: null },
            { id: '3', name: 'Tender Management', sub: null },
            { id: '4', name: 'Sales Pricing', sub: null },
            { id: '5', name: 'CS Booking', sub: null },
            { id: '6', name: 'CS Dokumentation', sub: null },
            { id: '7', name: 'Invoicing + Finance', sub: null },
            { id: '8', name: 'CS Import', sub: null },
            { id: '9', name: 'Archive', sub: null },
            { id: '10', name: 'System Maintenance', sub: null },
            { id: '11', divider: true },
            { id: '12', name: 'Organize Favorites', className: 'edit-favs', sub: null },
        ];


        return (
            <FisClient appsList={openAppList} applicationMenu={appList} favouriteMenu={favouriteList} user={user} >
                <form>
                    <FisPanel buttonGroupsTop={[			{ buttons: [
									{title: "Refresh", variant: "content-default"}
									] }
		]} buttonGroupsMiddle={[			{ buttons: [
									{title: "Revenues", variant: "content-default"},
									{title: "Create MTDs", variant: "content-default"},
									{title: "BB &lt;-&gt; MT", variant: "content-default"},
									{title: "Distribute BC", variant: "content-default"},
									{title: "Bkg-Detail", variant: "content-default"},
									{title: "Clear", variant: "content-default"}
									] }
		]} buttonGroupsBottom={[			{ buttons: [
									{title: "Close", variant: "content-default"},
									{title: "Cancel", variant: "content-default"}
									] }
		]}>
                        <FisStripe isOpened={true} buttonGroups={[			{ buttons: [
										{title: "eBkg Exceptions", variant: "content-default"},
										{title: "RA HS Code", variant: "content-default"},
										{title: "EU Customs", variant: "content-default"},
										{title: "Proposal", variant: "content-default"},
										{title: "ScheduLing", variant: "content-default"},
										{title: "Announce Sel.", variant: "content-default"},
										{title: "Ann. Equip.", variant: "content-default"},
										{title: "Ann. Compl.", variant: "content-default"},
										{title: "Copy", variant: "content-default"},
										{title: "Save", variant: "content-default"}
										] }
			]}>
                            <FisContainerColumn className="flex-box" colSpan={58}>
                                <div>
                                    <FisContainerRow>
                                        <FisContainerColumn colSpan={58} className="flex-box">
                                            <FisContainerRow>
                                                <FisContainerColumn colSpan={18} className="flex-box">
                                                    <FisFieldset legend="Shipment" infoflow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={15} >
                                                                <FisContainerRow>
                                                                    <FisContainerColumn colSpan={6} >
                                                                        <div>
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={6} >
                                                                                    <FisInputGroup label="Number">
                                                                                        <FisInputPickFlow colSpan={6}>
                                                                                            <FisInputBase type="number"
                                                                                                          colSpan={5}
                                                                                            />
                                                                                        </FisInputPickFlow>
                                                                                    </FisInputGroup>
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={3} >
                                                                                    <FisInputGroup label="Restriction Class">
                                                                                        <FisInputBase type="text"
                                                                                                      infoflow
                                                                                                      colSpan={3} />
                                                                                    </FisInputGroup>
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
                                                                        </div>
                                                                    </FisContainerColumn>
                                                                    <FisContainerColumn colSpan={4} >
                                                                        <div>
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={3} >
                                                                                    <FisInputGroup label="Type">
                                                                                        <FisInputBase type="text"
                                                                                                      colSpan={3} />
                                                                                    </FisInputGroup>
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={4} >
                                                                                    <FisInputGroup label="Terms">
                                                                                        <FisInputBase type="text"
                                                                                                      colSpan={4} />
                                                                                    </FisInputGroup>
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
                                                                        </div>
                                                                    </FisContainerColumn>
                                                                    <FisContainerColumn colSpan={5} >
                                                                        <FisCheckboxRadioGroup colSpan={5} inputlist={[
													{ label: "PT-Cons", name: "EShipment_PartnerCons" }
												]} opts={{type: "checkbox", label: "", stacked: false, name: "EShipment_PartnerConsGroup" , standalone: true}} />
                                                                    </FisContainerColumn>
                                                                </FisContainerRow>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={14} >
                                                                <FisContainerRow>
                                                                    <FisContainerColumn colSpan={9} >
                                                                        <FisInputGroup label="Rate Agreement">
                                                                            <FisInputPickFlow colSpan={9}>
                                                                                <FisInputBase type="text"
                                                                                              infoflow
                                                                                              colSpan={8} />
                                                                            </FisInputPickFlow>
                                                                        </FisInputGroup>
                                                                    </FisContainerColumn>
                                                                    <FisContainerColumn colSpan={5} >
                                                                        <FisCheckboxRadioGroup colSpan={5} inputlist={[
													{ label: "Remarks", readOnly: true, name: "ERateAgreementService_BookingRelRemark" }
												]} opts={{type: "checkbox", label: "", stacked: false, name: "ERateAgreementService_BookingRelRemarkGroup" , standalone: true}} />
                                                                    </FisContainerColumn>
                                                                </FisContainerRow>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={17} >
                                                                <FisContainerRow>
                                                                    <FisContainerColumn colSpan={17} >
                                                                        <FisInputGroup label="Status">
                                                                            <FisInputPickFlow colSpan={17}>
                                                                                <FisInputBase type="text"
                                                                                              readOnly
                                                                                              colSpan={16} />
                                                                            </FisInputPickFlow>
                                                                        </FisInputGroup>
                                                                    </FisContainerColumn>
                                                                </FisContainerRow>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={16} >
                                                                <FisInputGroup label="Internal Ref.">
                                                                    <FisInputBase type="text"
                                                                                  colSpan={16} />
                                                                </FisInputGroup>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={9} >
                                                                <FisInputGroup label="Local Bkg. Ref.">
                                                                    <FisInputBase type="text"
                                                                                  colSpan={9} />
                                                                </FisInputGroup>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                    </FisFieldset>
                                                </FisContainerColumn>
                                                <FisContainerColumn colSpan={27} className="flex-box">
                                                    <FisFieldset legend="Customer" >
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={12} >
                                                                <FisInputGroup label="Matchcode">
                                                                    <FisInputPickFlow colSpan={12}>
                                                                        <FisInputMatchCode infoflow />
                                                                        <FisInputBase type="text"
                                                                                      colSpan={4} />
                                                                    </FisInputPickFlow>
                                                                </FisInputGroup>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={14} >
                                                                <FisInputGroup label="Reference">
                                                                    <FisInputBase type="text"
                                                                                  colSpan={14} />
                                                                </FisInputGroup>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={14} >
                                                                <FisInputGroup label="Contact">
                                                                    <FisInputBase type="text"
                                                                                  colSpan={14} />
                                                                </FisInputGroup>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={14} >
                                                                <FisInputGroup label="Telephone">
                                                                    <FisInputBase type="text"
                                                                                  colSpan={14} />
                                                                </FisInputGroup>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={14} >
                                                                <FisInputGroup label="Mail Address 1">
                                                                    <FisInputBase type="email"  colSpan={14}  />
                                                                </FisInputGroup>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={26} >
                                                                <FisInputGroup label="Mail Address 2">
                                                                    <FisInputBase type="email"  colSpan={14}  />
                                                                    <FisInputPickFlow colSpan={12}>
                                                                        <FisCheckboxRadioGroup colSpan={2} inputlist={[{ label: "", readOnly: true, name: "EMoreBcReceiver_Flag" }]} opts={{type: "checkbox", label: "", stacked: false, name: "EMoreBcReceiver_Flag" , standalone: true}} />
                                                                        <FisText>More BC Receiver</FisText>
                                                                    </FisInputPickFlow>
                                                                </FisInputGroup>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                    </FisFieldset>
                                                </FisContainerColumn>
                                                <FisContainerColumn colSpan={13} className="flex-box">
                                                    <FisFieldset legend="Party Functions" infoflow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={12} >
                                                                <FisInputGroup label="MR">
                                                                    <FisInputPickFlow colSpan={12}>
                                                                        <FisInputMatchCode infoflow />
                                                                        <FisInputBase type="text"
                                                                                      colSpan={4} />
                                                                    </FisInputPickFlow>
                                                                </FisInputGroup>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={12} >
                                                                <FisInputGroup label="EP">
                                                                    <FisInputPickFlow colSpan={12}>
                                                                        <FisInputMatchCode infoflow />
                                                                        <FisInputBase type="text"
                                                                                      colSpan={4} />
                                                                    </FisInputPickFlow>
                                                                </FisInputGroup>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={12} >
                                                                <FisInputGroup label="IS">
                                                                    <FisInputPickFlow colSpan={12}>
                                                                        <FisInputMatchCode infoflow />
                                                                        <FisInputBase type="text"
                                                                                      colSpan={4} />
                                                                    </FisInputPickFlow>
                                                                </FisInputGroup>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={12} >
                                                                <FisInputGroup label="FF">
                                                                    <FisInputPickFlow colSpan={12}>
                                                                        <FisInputMatchCode infoflow />
                                                                        <FisInputBase type="text"
                                                                                      colSpan={4} />
                                                                    </FisInputPickFlow>
                                                                </FisInputGroup>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={12} >
                                                                <FisInputGroup label="SH">
                                                                    <FisInputPickFlow colSpan={12}>
                                                                        <FisInputMatchCode infoflow />
                                                                        <FisInputBase type="text"
                                                                                      colSpan={4} />
                                                                    </FisInputPickFlow>
                                                                </FisInputGroup>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                    </FisFieldset>
                                                </FisContainerColumn>
                                            </FisContainerRow>
                                        </FisContainerColumn>
                                    </FisContainerRow>
                                    <FisContainerRow>
                                        <FisContainerColumn colSpan={49} className="flex-box">
                                            <FisContainerRow>
                                                <FisContainerColumn colSpan={23} className="flex-box">
                                                    <FisFieldset legend="Routing" infoflow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={12} >
                                                                <FisContainerRow>
                                                                    <FisContainerColumn colSpan={6} >
                                                                        <FisInputGroup label="Export">
                                                                            <FisInputBase type="text"
                                                                                          colSpan={3} />
                                                                            <FisInputBase type="text"
                                                                                          colSpan={3} />
                                                                        </FisInputGroup>
                                                                    </FisContainerColumn>
                                                                    <FisContainerColumn colSpan={6} >
                                                                        <FisInputGroup label="Import">
                                                                            <FisInputBase type="text"
                                                                                          colSpan={3} />
                                                                            <FisInputBase type="text"
                                                                                          colSpan={3} />
                                                                        </FisInputGroup>
                                                                    </FisContainerColumn>
                                                                </FisContainerRow>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={16} >
                                                                <FisContainerRow>
                                                                    <FisContainerColumn colSpan={16} >
                                                                        <FisInputGroup label="Start">
                                                                            <FisInputPickFlow colSpan={5}>
                                                                                <FisInputBase type="text"
                                                                                              colSpan={4} />
                                                                            </FisInputPickFlow>
                                                                            <FisInputPickFlow colSpan={8}>
                                                                                <FisInputMatchCode infoflow />
                                                                            </FisInputPickFlow>
                                                                            <FisInputBase type="text"
                                                                                          colSpan={3} />
                                                                        </FisInputGroup>
                                                                    </FisContainerColumn>
                                                                </FisContainerRow>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={16} >
                                                                <FisContainerRow>
                                                                    <FisContainerColumn colSpan={16} >
                                                                        <FisInputGroup label="Via">
                                                                            <FisInputPickFlow colSpan={5}>
                                                                                <FisInputBase type="text"
                                                                                              colSpan={4} />
                                                                            </FisInputPickFlow>
                                                                            <FisInputPickFlow colSpan={8}>
                                                                                <FisInputMatchCode infoflow />
                                                                            </FisInputPickFlow>
                                                                            <FisInputBase type="text"
                                                                                          colSpan={3} />
                                                                        </FisInputGroup>
                                                                    </FisContainerColumn>
                                                                </FisContainerRow>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={19} >
                                                                <FisContainerRow>
                                                                    <FisContainerColumn colSpan={16} >
                                                                        <FisInputGroup label="Via">
                                                                            <FisInputPickFlow colSpan={5}>
                                                                                <FisInputBase type="text"
                                                                                              colSpan={4} />
                                                                            </FisInputPickFlow>
                                                                            <FisInputPickFlow colSpan={8}>
                                                                                <FisInputMatchCode infoflow />
                                                                            </FisInputPickFlow>
                                                                            <FisInputBase type="text"
                                                                                          colSpan={3} />
                                                                        </FisInputGroup>
                                                                    </FisContainerColumn>
                                                                    <FisContainerColumn colSpan={3} >
                                                                        <FisCheckboxRadioGroup colSpan={3} inputlist={[
													{ label: "More", name: "EMorePrecarrVias_Flag" }
												]} opts={{type: "checkbox", label: "", stacked: false, name: "EMorePrecarrVias_FlagGroup" , standalone: true}} />
                                                                    </FisContainerColumn>
                                                                </FisContainerRow>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={16} >
                                                                <FisContainerRow>
                                                                    <FisContainerColumn colSpan={16} >
                                                                        <FisInputGroup label="POL">
                                                                            <FisInputPickFlow colSpan={5}>
                                                                                <FisInputBase type="text"
                                                                                              colSpan={4} />
                                                                            </FisInputPickFlow>
                                                                            <FisInputPickFlow colSpan={8}>
                                                                                <FisInputMatchCode infoflow />
                                                                            </FisInputPickFlow>
                                                                            <FisInputBase type="text"
                                                                                          readOnly
                                                                                          colSpan={3} />
                                                                        </FisInputGroup>
                                                                    </FisContainerColumn>
                                                                </FisContainerRow>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={11} >
                                                                <FisContainerRow>
                                                                    <FisContainerColumn colSpan={11} >
                                                                        <FisInputGroup label="DP Voy.">
                                                                            <FisInputPickFlow colSpan={11}>
                                                                                <FisInputBase type="number"
                                                                                              colSpan={4}
                                                                                />
                                                                                <FisInputBase type="text"
                                                                                              readOnly
                                                                                              placeholder="Schedule Voyage"
                                                                                              colSpan={6} />
                                                                            </FisInputPickFlow>
                                                                        </FisInputGroup>
                                                                    </FisContainerColumn>
                                                                </FisContainerRow>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={17} >
                                                                <FisInputGroup label="Vessel">
                                                                    <FisInputBase type="text"
                                                                                  readOnly
                                                                                  colSpan={17} />
                                                                </FisInputGroup>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={16} >
                                                                <FisContainerRow>
                                                                    <FisContainerColumn colSpan={16} >
                                                                        <FisInputGroup label="POD">
                                                                            <FisInputPickFlow colSpan={5}>
                                                                                <FisInputBase type="text"
                                                                                              colSpan={4} />
                                                                            </FisInputPickFlow>
                                                                            <FisInputPickFlow colSpan={8}>
                                                                                <FisInputMatchCode infoflow />
                                                                            </FisInputPickFlow>
                                                                            <FisInputBase type="text"
                                                                                          colSpan={3} />
                                                                        </FisInputGroup>
                                                                    </FisContainerColumn>
                                                                </FisContainerRow>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={19} >
                                                                <FisContainerRow>
                                                                    <FisContainerColumn colSpan={16} >
                                                                        <FisInputGroup label="Via">
                                                                            <FisInputPickFlow colSpan={5}>
                                                                                <FisInputBase type="text"
                                                                                              colSpan={4} />
                                                                            </FisInputPickFlow>
                                                                            <FisInputPickFlow colSpan={8}>
                                                                                <FisInputMatchCode infoflow />
                                                                            </FisInputPickFlow>
                                                                            <FisInputBase type="text"
                                                                                          colSpan={3} />
                                                                        </FisInputGroup>
                                                                    </FisContainerColumn>
                                                                    <FisContainerColumn colSpan={3} >
                                                                        <FisCheckboxRadioGroup colSpan={3} inputlist={[
													{ label: "More", name: "EMoreOncarrVias_Flag" }
												]} opts={{type: "checkbox", label: "", stacked: false, name: "EMoreOncarrVias_FlagGroup" , standalone: true}} />
                                                                    </FisContainerColumn>
                                                                </FisContainerRow>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={13} >
                                                                <FisContainerRow>
                                                                    <FisContainerColumn colSpan={13} >
                                                                        <FisInputGroup label="End">
                                                                            <FisInputPickFlow colSpan={5}>
                                                                                <FisInputBase type="text"
                                                                                              colSpan={4} />
                                                                            </FisInputPickFlow>
                                                                            <FisInputPickFlow colSpan={8}>
                                                                                <FisInputMatchCode infoflow />
                                                                            </FisInputPickFlow>
                                                                        </FisInputGroup>
                                                                    </FisContainerColumn>
                                                                </FisContainerRow>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={22} >
                                                                <FisCheckboxRadioGroup colSpan={22} inputlist={[
											{ label: "Schedule US Flag Restricted Vessel Only", name: "EScheduleUsFlagOnly_Flag" }
										]} opts={{type: "checkbox", label: "", stacked: true, name: "EScheduleUsFlagOnly_FlagGroup" , standalone: true}} />
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                    </FisFieldset>
                                                </FisContainerColumn>
                                                <FisContainerColumn colSpan={26} className="flex-box">
                                                    <div>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={26} className="flex-box">
                                                                <FisFieldset legend="Equipment" infoflow>
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={20} >
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={13} >
                                                                                    <FisInputGroup label="Depot">
                                                                                        <FisInputPickFlow colSpan={5}>
                                                                                            <FisInputBase type="text"
                                                                                                          colSpan={4} />
                                                                                        </FisInputPickFlow>
                                                                                        <FisInputPickFlow colSpan={8}>
                                                                                            <FisInputMatchCode infoflow />
                                                                                        </FisInputPickFlow>
                                                                                    </FisInputGroup>
                                                                                </FisContainerColumn>
                                                                                <FisContainerColumn colSpan={7} >
                                                                                    <FisCheckboxRadioGroup colSpan={7} inputlist={[
															{ label: "More Depots", readOnly: true, name: "EMoreDepotsExists_Flag" }
														]} opts={{type: "checkbox", label: "", stacked: false, name: "EMoreDepotsExists_FlagGroup" , standalone: true}} />
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={25} >
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={3} >
                                                                                    <div>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={3} >
                                                                                                <FisInputGroup label="22GP">
                                                                                                    <FisInputBase type="number"
                                                                                                                  colSpan={3}
                                                                                                                  infoflow />
                                                                                                </FisInputGroup>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={3} >
                                                                                                <FisInputGroup label="42GP">
                                                                                                    <FisInputBase type="number"
                                                                                                                  colSpan={3}
                                                                                                                  infoflow />
                                                                                                </FisInputGroup>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={3} >
                                                                                                <FisInputGroup label="45GP">
                                                                                                    <FisInputBase type="number"
                                                                                                                  colSpan={3}
                                                                                                                  infoflow />
                                                                                                </FisInputGroup>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                    </div>
                                                                                </FisContainerColumn>
                                                                                <FisContainerColumn colSpan={3} >
                                                                                    <div>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={3} >
                                                                                                <FisInputGroup label="22RT">
                                                                                                    <FisInputBase type="number"
                                                                                                                  colSpan={3}
                                                                                                                  infoflow />
                                                                                                </FisInputGroup>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={3} >
                                                                                                <FisInputGroup label="42UT">
                                                                                                    <FisInputBase type="number"
                                                                                                                  colSpan={3}
                                                                                                                  infoflow />
                                                                                                </FisInputGroup>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={3} >
                                                                                                <FisInputGroup label="45RT">
                                                                                                    <FisInputBase type="number"
                                                                                                                  colSpan={3}
                                                                                                                  infoflow />
                                                                                                </FisInputGroup>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                    </div>
                                                                                </FisContainerColumn>
                                                                                <FisContainerColumn colSpan={3} >
                                                                                    <div>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={3} >
                                                                                                <FisInputGroup label="22VH">
                                                                                                    <FisInputBase type="number"
                                                                                                                  colSpan={3}
                                                                                                                  infoflow />
                                                                                                </FisInputGroup>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={3} >
                                                                                                <FisInputGroup label="22UP">
                                                                                                    <FisInputBase type="number"
                                                                                                                  colSpan={3}
                                                                                                                  infoflow />
                                                                                                </FisInputGroup>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={3} >
                                                                                                <FisInputGroup label="42UP">
                                                                                                    <FisInputBase type="number"
                                                                                                                  colSpan={3}
                                                                                                                  infoflow />
                                                                                                </FisInputGroup>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                    </div>
                                                                                </FisContainerColumn>
                                                                                <FisContainerColumn colSpan={16} >
                                                                                    <div>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={7} >
                                                                                                <FisContainerRow>
                                                                                                    <FisContainerColumn colSpan={7} >
                                                                                                        <FisInputGroup label="Others">
                                                                                                            <FisInputPickFlow colSpan={4}>
                                                                                                                <FisInputBase type="text"
                                                                                                                              colSpan={3} />
                                                                                                            </FisInputPickFlow>
                                                                                                            <FisInputBase type="number"
                                                                                                                          colSpan={3}
                                                                                                                          infoflow />
                                                                                                        </FisInputGroup>
                                                                                                    </FisContainerColumn>
                                                                                                </FisContainerRow>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={16} >
                                                                                                <FisContainerRow>
                                                                                                    <FisContainerColumn colSpan={7} >
                                                                                                        <FisInputGroup label="Others">
                                                                                                            <FisInputPickFlow colSpan={4}>
                                                                                                                <FisInputBase type="text"
                                                                                                                              colSpan={3} />
                                                                                                            </FisInputPickFlow>
                                                                                                            <FisInputBase type="number"
                                                                                                                          colSpan={3}
                                                                                                                          infoflow />
                                                                                                        </FisInputGroup>
                                                                                                    </FisContainerColumn>
                                                                                                    <FisContainerColumn colSpan={9} >
                                                                                                        <FisCheckboxRadioGroup colSpan={9} inputlist={[
																			{ label: "SOW-Container(s)", readOnly: true, name: "ESowContainerFound_Flag" }
																		]} opts={{type: "checkbox", label: "", stacked: false, name: "ESowContainerFound_FlagGroup" , standalone: true}} />
                                                                                                    </FisContainerColumn>
                                                                                                </FisContainerRow>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={14} >
                                                                                                <FisContainerRow>
                                                                                                    <FisContainerColumn colSpan={7} >
                                                                                                        <FisInputGroup label="Others">
                                                                                                            <FisInputPickFlow colSpan={4}>
                                                                                                                <FisInputBase type="text"
                                                                                                                              colSpan={3} />
                                                                                                            </FisInputPickFlow>
                                                                                                            <FisInputBase type="number"
                                                                                                                          colSpan={3}
                                                                                                                          infoflow />
                                                                                                        </FisInputGroup>
                                                                                                    </FisContainerColumn>
                                                                                                    <FisContainerColumn colSpan={7} >
                                                                                                        <FisCheckboxRadioGroup colSpan={7} inputlist={[
																			{ label: "Substitution", readOnly: true, name: "ESubstitutedContainerFound_Flag" }
																		]} opts={{type: "checkbox", label: "", stacked: false, name: "ESubstitutedContainerFound_FlagGroup" , standalone: true}} />
                                                                                                    </FisContainerColumn>
                                                                                                </FisContainerRow>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                    </div>
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={15} >
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={9} >
                                                                                    <FisInputGroup label="Pos.Date/Time">
                                                                                        <FisInputDate colSpan={4}   />
                                                                                        <FisInputBase type="text"
                                                                                                      colSpan={2} />
                                                                                        <FisInputBase type="time" colSpan={3}   />
                                                                                    </FisInputGroup>
                                                                                </FisContainerColumn>
                                                                                <FisContainerColumn colSpan={6} >
                                                                                    <FisCheckboxRadioGroup colSpan={6} inputlist={[
															{ label: "Diff. Pos.", readOnly: true, name: "EDifferentPositioningInfo_Flag" }
														]} opts={{type: "checkbox", label: "", stacked: false, name: "EDifferentPositioningInfo_FlagGroup" , standalone: true}} />
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={20} >
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={9} >
                                                                                    <FisInputGroup label="Inland Cut-Off  ">
                                                                                        <FisInputDate colSpan={4}   />
                                                                                        <FisInputBase type="time" colSpan={3}   />
                                                                                    </FisInputGroup>
                                                                                </FisContainerColumn>
                                                                                <FisContainerColumn colSpan={11} >
                                                                                    <FisButtonGroup colSpan={11}>
                                                                                        <FisButton variant="content-default" colSpan={11}>EQ Request Detail</FisButton>
                                                                                    </FisButtonGroup>
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                </FisFieldset>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={25} className="flex-box">
                                                                <FisFieldset legend="Commodity" infoflow>
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={19} >
                                                                            <FisInputGroup label="Short Description">
                                                                                <FisInputBase type="text"
                                                                                              colSpan={19} />
                                                                            </FisInputGroup>
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={13} >
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={7} >
                                                                                    <FisInputGroup label="HS Code">
                                                                                        <FisInputPickFlow colSpan={7}>
                                                                                            <FisInputHarmonizedSystemCode  />
                                                                                        </FisInputPickFlow>
                                                                                    </FisInputGroup>
                                                                                </FisContainerColumn>
                                                                                <FisContainerColumn colSpan={6} >
                                                                                    <FisCheckboxRadioGroup colSpan={6} inputlist={[
															{ label: "More Cargo", readOnly: true, name: "EMoreCargo_Flag" }
														]} opts={{type: "checkbox", label: "", stacked: false, name: "EMoreCargo_FlagGroup" , standalone: true}} />
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={24} >
                                                                            <FisContainerRow>
                                                                                <FisContainerColumn colSpan={10} >
                                                                                    <div>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={10} >
                                                                                                <FisContainerRow>
                                                                                                    <FisContainerColumn colSpan={10} >
                                                                                                        <FisInputGroup label="Total Gross Wt">
                                                                                                            <FisInputBase type="number"
                                                                                                                          colSpan={7}
                                                                                                            />
                                                                                                            <FisInputBase type="text"
                                                                                                                          colSpan={3} />
                                                                                                        </FisInputGroup>
                                                                                                    </FisContainerColumn>
                                                                                                </FisContainerRow>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                        <FisContainerRow>
                                                                                            <FisContainerColumn colSpan={9} >
                                                                                                <FisContainerRow>
                                                                                                    <FisContainerColumn colSpan={9} >
                                                                                                        <FisInputGroup label="Total Gross Vol">
                                                                                                            <FisInputBase type="number"
                                                                                                                          colSpan={6}
                                                                                                            />
                                                                                                            <FisInputBase type="text"
                                                                                                                          colSpan={3} />
                                                                                                        </FisInputGroup>
                                                                                                    </FisContainerColumn>
                                                                                                </FisContainerRow>
                                                                                            </FisContainerColumn>
                                                                                        </FisContainerRow>
                                                                                    </div>
                                                                                </FisContainerColumn>
                                                                                <FisContainerColumn colSpan={6} >
                                                                                    <FisCheckboxRadioGroup inputlist={[
															{ label: "Total" },
															{ label: "Per Cntr" }
														]} opts={{type: "radio", label: " ", stacked: true, mandatory: false, name: "ETotalSingleWeightVolume_Integer"}} />
                                                                                </FisContainerColumn>
                                                                                <FisContainerColumn colSpan={8} >
                                                                                    <FisContainerRow>
                                                                                        <FisContainerColumn colSpan={4} >
                                                                                            <div>
                                                                                                <FisContainerRow>
                                                                                                    <FisContainerColumn colSpan={4} >
                                                                                                        <FisCheckboxRadioGroup colSpan={4} inputlist={[
																			{ label: "DG", readOnly: true, name: "EShipment_DaDgContained" },
																			{ label: "Temp", readOnly: true, name: "EShipment_DaTempContained" }
																		]} opts={{type: "checkbox", label: "", stacked: true, name: "EShipment_DaDgContainedGroup" }} />
                                                                                                    </FisContainerColumn>
                                                                                                </FisContainerRow>
                                                                                            </div>
                                                                                        </FisContainerColumn>
                                                                                        <FisContainerColumn colSpan={4} >
                                                                                            <div>
                                                                                                <FisContainerRow>
                                                                                                    <FisContainerColumn colSpan={4} >
                                                                                                        <FisCheckboxRadioGroup colSpan={4} inputlist={[
																			{ label: "Vent", readOnly: true, name: "EShipment_DaVentContained" },
																			{ label: "OOG", readOnly: true, name: "EShipment_DaOogContained" }
																		]} opts={{type: "checkbox", label: "", stacked: true, name: "EShipment_DaVentContainedGroup" }} />
                                                                                                    </FisContainerColumn>
                                                                                                </FisContainerRow>
                                                                                            </div>
                                                                                        </FisContainerColumn>
                                                                                    </FisContainerRow>
                                                                                </FisContainerColumn>
                                                                            </FisContainerRow>
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                </FisFieldset>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                    </div>
                                                </FisContainerColumn>
                                            </FisContainerRow>
                                        </FisContainerColumn>
                                    </FisContainerRow>
                                    <FisContainerRow>
                                        <FisContainerColumn colSpan={45} className="flex-box">
                                            <FisFieldset legend="Customs References" >
                                                <FisContainerRow>
                                                    <FisContainerColumn colSpan={31} >
                                                        <div>
                                                            <FisContainerRow>
                                                                <FisContainerColumn colSpan={31} >
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={6} >
                                                                            <FisInputGroup label=" ">
                                                                                <FisInputBase type="text"
                                                                                              colSpan={6} />
                                                                            </FisInputGroup>
                                                                        </FisContainerColumn>
                                                                        <FisContainerColumn colSpan={4} >
                                                                            <FisInputGroup label="US-AES">
                                                                                <FisInputBase type="text"
                                                                                              colSpan={4} />
                                                                            </FisInputGroup>
                                                                        </FisContainerColumn>
                                                                        <FisContainerColumn colSpan={10} >
                                                                            <FisCheckboxRadioGroup colSpan={10} inputlist={[
													{ label: "More Customs Info", readOnly: true, name: "EMoreCustomsInfos_Flag" }
												]} opts={{type: "checkbox", label: "", stacked: false, name: "EMoreCustomsInfos_FlagGroup" , standalone: true}} />
                                                                        </FisContainerColumn>
                                                                        <FisContainerColumn colSpan={11} >
                                                                            <FisCheckboxRadioGroup inputlist={[
													{ label: "Cntr" },
													{ label: "Cargo" },
													{ label: "Shipment" }
												]} opts={{type: "radio", label: " ", stacked: false, mandatory: false, name: "ECustomsLevel_Integer"}} />
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                </FisContainerColumn>
                                                            </FisContainerRow>
                                                            <FisContainerRow>
                                                                <FisContainerColumn colSpan={26} >
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={19} >
                                                                            <FisInputGroup label=" ">
                                                                                <FisInputBase type="text"
                                                                                              colSpan={19} />
                                                                            </FisInputGroup>
                                                                        </FisContainerColumn>
                                                                        <FisContainerColumn colSpan={3} >
                                                                            <FisInputGroup label="Part No">
                                                                                <FisInputBase type="text"
                                                                                              colSpan={3} />
                                                                            </FisInputGroup>
                                                                        </FisContainerColumn>
                                                                        <FisContainerColumn colSpan={4} >
                                                                            <FisInputGroup label="SCAC">
                                                                                <FisInputBase type="text"
                                                                                              colSpan={4} />
                                                                            </FisInputGroup>
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                </FisContainerColumn>
                                                            </FisContainerRow>
                                                        </div>
                                                    </FisContainerColumn>
                                                    <FisContainerColumn colSpan={2} >
                                                        <div>
                                                            <FisContainerRow>
                                                                <FisContainerColumn colSpan={2} >
                                                                    <FisButtonIcon icon="fis-icon fis-icon-arrow-up" />
                                                                </FisContainerColumn>
                                                            </FisContainerRow>
                                                            <FisContainerRow>
                                                                <FisContainerColumn colSpan={2} >
                                                                    <FisButtonIcon icon="fis-icon fis-icon-arrow-down" />
                                                                </FisContainerColumn>
                                                            </FisContainerRow>
                                                        </div>
                                                    </FisContainerColumn>
                                                    <FisContainerColumn colSpan={11} >
                                                        <div>
                                                            <FisContainerRow>
                                                                <FisContainerColumn colSpan={11} >
                                                                    <FisInputGroup label="CR">
                                                                        <FisTextArea rows={4}
                                                                                     colSpan={11}
                                                                                     readOnly />
                                                                    </FisInputGroup>
                                                                </FisContainerColumn>
                                                            </FisContainerRow>
                                                        </div>
                                                    </FisContainerColumn>
                                                </FisContainerRow>
                                            </FisFieldset>
                                        </FisContainerColumn>
                                    </FisContainerRow>
                                    <FisContainerRow>
                                        <FisContainerColumn colSpan={35} className="flex-box">
                                            <FisFieldset legend="External References" >
                                                <FisContainerRow>
                                                    <FisContainerColumn colSpan={32} >
                                                        <div>
                                                            <FisContainerRow>
                                                                <FisContainerColumn colSpan={32} >
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={4} >
                                                                            <FisInputGroup label=" ">
                                                                                <FisInputBase type="text"
                                                                                              colSpan={4} />
                                                                            </FisInputGroup>
                                                                        </FisContainerColumn>
                                                                        <FisContainerColumn colSpan={9} >
                                                                            <FisCheckboxRadioGroup colSpan={9} inputlist={[
													{ label: "More References", readOnly: true, name: "EMoreDiffReferenceValues_Flag" }
												]} opts={{type: "checkbox", label: "", stacked: false, name: "EMoreDiffReferenceValues_FlagGroup" , standalone: true}} />
                                                                        </FisContainerColumn>
                                                                        <FisContainerColumn colSpan={19} >
                                                                            <FisCheckboxRadioGroup inputlist={[
													{ label: "Shipment" },
													{ label: "Container" },
													{ label: "Cargo" },
													{ label: "Cargo Item" }
												]} opts={{type: "radio", label: " ", stacked: false, mandatory: false, name: "EReferenceLevel_Integer"}} />
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                </FisContainerColumn>
                                                            </FisContainerRow>
                                                            <FisContainerRow>
                                                                <FisContainerColumn colSpan={19} >
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={19} >
                                                                            <FisInputGroup label=" ">
                                                                                <FisInputBase type="text"
                                                                                              colSpan={19} />
                                                                            </FisInputGroup>
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                </FisContainerColumn>
                                                            </FisContainerRow>
                                                        </div>
                                                    </FisContainerColumn>
                                                    <FisContainerColumn colSpan={2} >
                                                        <div>
                                                            <FisContainerRow>
                                                                <FisContainerColumn colSpan={2} >
                                                                    <FisButtonIcon icon="fis-icon fis-icon-arrow-up" />
                                                                </FisContainerColumn>
                                                            </FisContainerRow>
                                                            <FisContainerRow>
                                                                <FisContainerColumn colSpan={2} >
                                                                    <FisButtonIcon icon="fis-icon fis-icon-arrow-down" />
                                                                </FisContainerColumn>
                                                            </FisContainerRow>
                                                        </div>
                                                    </FisContainerColumn>
                                                </FisContainerRow>
                                            </FisFieldset>
                                        </FisContainerColumn>
                                    </FisContainerRow>
                                    <FisContainerRow>
                                        <FisContainerColumn colSpan={47} className="flex-box">
                                            <FisFieldset legend="Remarks" infoflow>
                                                <FisContainerRow>
                                                    <FisContainerColumn colSpan={46} >
                                                        <FisContainerRow>
                                                            <FisContainerColumn colSpan={8} >
                                                                <div>
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={3} >
                                                                            <FisInputGroup label="Type">
                                                                                <FisInputBase type="text"
                                                                                              colSpan={3} />
                                                                            </FisInputGroup>
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={8} >
                                                                            <FisCheckboxRadioGroup colSpan={8} inputlist={[
													{ label: "More Remarks", readOnly: true, name: "EMoreRemarks_Flag" }
												]} opts={{type: "checkbox", label: "", stacked: true, name: "EMoreRemarks_FlagGroup" , standalone: true}} />
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                </div>
                                                            </FisContainerColumn>
                                                            <FisContainerColumn colSpan={36} >
                                                                <FisInputGroup label="Text">
                                                                    <FisTextArea rows={4}
                                                                                 colSpan={36}
                                                                    />
                                                                </FisInputGroup>
                                                            </FisContainerColumn>
                                                            <FisContainerColumn colSpan={2} >
                                                                <div>
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={2} >
                                                                            <FisButtonIcon icon="fis-icon fis-icon-arrow-up" />
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                    <FisContainerRow>
                                                                        <FisContainerColumn colSpan={2} >
                                                                            <FisButtonIcon icon="fis-icon fis-icon-arrow-down" />
                                                                        </FisContainerColumn>
                                                                    </FisContainerRow>
                                                                </div>
                                                            </FisContainerColumn>
                                                        </FisContainerRow>
                                                    </FisContainerColumn>
                                                </FisContainerRow>
                                            </FisFieldset>
                                        </FisContainerColumn>
                                    </FisContainerRow>
                                </div>
                            </FisContainerColumn>
                        </FisStripe>
                    </FisPanel>
                </form>
            </FisClient>
        );
    }
}
