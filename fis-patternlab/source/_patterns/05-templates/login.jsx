import React from 'react';
import { FisClient } from 'fisui.js';
import { FisLogin } from 'fisui.js';

export default class FisLoginPattern extends React.Component {
    render() {
        return (
            <FisClient>
                <FisLogin />
            </FisClient>
        );
    }
}
