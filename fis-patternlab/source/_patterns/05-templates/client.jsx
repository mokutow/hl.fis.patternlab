import React from 'react';

import { FisClient } from 'fisui.js';
import { FisPanel } from 'fisui.js';
import { FisStripe } from 'fisui.js';
import { FisFieldset } from 'fisui.js';
import { FisInputGroup } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';
import { FisInputBase } from 'fisui.js';
import { FisInputDate } from 'fisui.js';
import { FisInputPickFlow } from 'fisui.js';
import { FisCheckboxRadioGroup } from 'fisui.js';
// FIXME: DELETE WHEN  IS REALLY USED
/* eslint-disable */
export default class FisClientPattern extends React.Component {
    render() {

        const user = {};
        const openAppList = {
            currentApp: {
                id: 't9500'
            }
            ,
            apps: [
                { app: { name: 'Dashboard', icon: 'fis-icon-glyphicons-home', url: '/', id: 'D000' }, noClose: true, className: 'homeBtn' },
                { app: { name: 'T9500', url: '/t9500/', id: 't9500', params: '&date=2016-01-17&end=&start=DEHAM&end=DEBRV' },
                    modal: { name: 'S8020' }, isActive: true },
                { app: { name: 'S8020', url: '/t9500/', id: 'f9500' }, isActive: false },
                { app: { name: 'E4842', url: '/t9500/', id: 'e9500' }, modal: { name: 'S8020' }, isActive: false },
                { app: { name: 'S8020', url: '/t9500/', id: 's9500' }, isActive: false },
                { app: { name: 'S8020', url: '/t9500/', id: 'r9500' }, isActive: false },
                { app: { name: 'E4842', url: '/t9500/', id: 'g9500' }, modal: { name: 'S8020' }, isActive: false }
            ]};
        const appList = [
            { id: '0', name: 'Customer Information System', sub: [
                { name: 'Bkg General 0-0', url: '0-0', sub: null },
                { name: 'Bkg Detail 0-1', url: '0-1', sub: null },
                { name: 'Bkg Special 0-2', url: '0-2', sub: null }
            ]
            },

            { id: '1', name: 'Tender Management', sub: [
                { name: 'Bkg General 2-0', url: '2-0', sub: null },
                { name: 'Bkg Detail 2-1', url: '2-1', sub: null },
                { name: 'Bkg Special 2-2', url: '2-2', sub: [
                    { name: 'lorem ipsum 2-2-0', url: '2-2-0', sub: null },
                    { name: 'lorem ipsum 2-2-1', url: '2-2-1', sub: null },
                    { name: 'lorem ipsum 2-2-2', url: '2-2-2', sub: null },
                    { name: 'lorem ipsum 2-2-3', url: '2-2-3', sub: null },
                    { name: 'lorem ipsum 2-2-4', url: '2-2-4', sub: null },
                    { name: 'lorem ipsum 2-2-5', url: '2-2-5', sub: null },
                    { name: 'lorem ipsum 2-2-6', url: '2-2-6', sub: null }
                ] },
                { name: 'lorem ipsum 2-3', url: '2-3', sub: null },
                { name: 'lorem ipsum 2-4', url: '2-4', sub: null },
                { name: 'lorem ipsum 2-5', url: '2-5', sub: null }
            ]
            },
            { id: '2', divider: true },
            { id: '3', name: 'Process Exception Advice', sub: null },
            { id: '4', name: 'Tender Management', sub: null },
            { id: '5', name: 'Sales Pricing', sub: null },
            { id: '6', name: 'CS Booking', sub: null },
            { id: '7', name: 'CS Dokumentation', sub: null },
            { id: '8', name: 'Invoicing + Finance', sub: null },
            { id: '9', name: 'CS Import', sub: null },
            { id: '10', name: 'Archive', sub: null },
            { id: '11', name: 'System Maintenance', sub: null }
        ];
        const favouriteList = [
            { id: '0', name: 'Favourites Group', sub: [
                { name: 'Bkg General 0-0', url: '0-0', sub: null },
                { name: 'Bkg Detail 0-1', url: '0-1', sub: null },
                { name: 'Bkg Special 0-2', url: '0-2', sub: null }
            ]
            },
            { id: '1', divider: true },
            { id: '2', name: 'Tender Management', sub: null },
            { id: '3', name: 'Tender Management', sub: null },
            { id: '4', name: 'Sales Pricing', sub: null },
            { id: '5', name: 'CS Booking', sub: null },
            { id: '6', name: 'CS Dokumentation', sub: null },
            { id: '7', name: 'Invoicing + Finance', sub: null },
            { id: '8', name: 'CS Import', sub: null },
            { id: '9', name: 'Archive', sub: null },
            { id: '10', name: 'System Maintenance', sub: null },
            { id: '11', divider: true },
            { id: '12', name: 'Organize Favorites', className: 'edit-favs', sub: null },
        ];


        const inputlist = [
            { name: 'Name', checked: true, value: '1', label: 'Name', id: 't1' },
            { name: 'TmpSep', checked: false, value: '2', label: 'TmpSep', id: 't2', tooltip:"my checkbox-tooltip" },
        ];

        const optsDefault = {
            name: 'myCheckbox1',
            label: 'Your Checkbox 1',
            disabled: false,
            mandatory: false
        };

        const buttonsGroups = [
            {
                buttons: [
                    { id: 'dl', title: 'Download', variant: 'content-default', action: '#/save' },
                    { id: 'dl2', title: 'Upload', variant: 'content-default', action: '#/save' },
                    { id: 'dl3', title: 'Excel', variant: 'content-default', action: '#/save' },
                    { id: 'dl4', title: 'Word', variant: 'content-default', action: '#/save' }
                ]
            },
            {
                buttons: [
                    { id: 'dl4', title: 'Mass Select with Tooltip', variant: 'content-default', action: '#/save', disabled: true, tooltip: 'May the force be with you' },
                    { id: 'dl5', title: 'Clear', variant: 'content-default', action: '#/save' },
                    { id: 'dl6', title: 'Search', variant: 'content-hero', action: '#/save' }
                ]
            }
        ];

        const buttonsGroupsPanelTop = [
            {
                buttons: [
                    { id: 'dl', title: 'Download', variant: 'actionbar-destructive', action: '#/save' },
                    { id: 'dl2', title: 'Upload', variant: 'actionbar-destructive', action: '#/save' }

                ]
            }
        ];
        const buttonsGroupsPanelMiddle = [{
            buttons: [
                { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' },
                { id: 'dl2', title: 'Upload', variant: 'actionbar-default', action: '#/save' },
                { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' }
            ]
        }, {

            buttons: [
                { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' },
                { id: 'dl2', title: 'Upload', variant: 'actionbar-default', action: '#/save' },
                { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' }
            ]
        },
            {

                buttons: [
                    { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl2', title: 'Upload', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl2', title: 'Upload', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl2', title: 'Upload', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' },
                ]
            },
            {
                buttons: [
                    { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl2', title: 'Upload', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl2', title: 'Upload', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl2', title: 'Upload', variant: 'actionbar-default', action: '#/save' },
                    { id: 'dl', title: 'Download', variant: 'actionbar-default', action: '#/save' },
                ]
            }
        ];
        const buttonsGroupsPanelBottom = [
            {
                buttons: [
                    { id: 'dl4', title: 'Mass Select', variant: 'actionbar-destructive', action: '#/save', disabled: true },
                    { id: 'dl6', title: 'Search', variant: 'actionbar-constructive', action: '#/save' }
                ]
            }
        ];


        return (
            <FisClient appsList={openAppList} applicationMenu={appList} favouriteMenu={favouriteList} user={user} >
                <FisPanel
                    id="T9500"
                    buttonGroupsTop={buttonsGroupsPanelTop}
                    buttonGroupsMiddle={buttonsGroupsPanelMiddle}
                    buttonGroupsBottom={buttonsGroupsPanelBottom}
                >


                    <FisStripe
                        title="Search"
                        isOpened
                        buttonGroups={buttonsGroups}
                    >
                        <FisContainerColumn >
                            <FisFieldset legend="Transportation request" infoflow>
                                <FisContainerRow>
                                    <FisContainerColumn>
                                        <FisInputGroup label="My Label">
                                            <FisInputBase
                                                placeholder="Name + tooltip"
                                                colSpan={5}
                                                tooltip="my input-tooltip"
                                            />
                                            <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={5} />
                                            <FisInputPickFlow className="default" colSpan={10}>
                                                <FisInputBase
                                                    placeholder="empty with placeholder + tooltip"
                                                    defaultValue=""
                                                    colSpan={5}
                                                    tooltip="my input-tooltip"
                                                />
                                                <FisInputBase
                                                    variant="disabled"
                                                    placeholder="Name"
                                                    defaultValue="disabled"
                                                    disabled colSpan={4}
                                                />
                                            </FisInputPickFlow>
                                        </FisInputGroup>
                                    </FisContainerColumn>
                                </FisContainerRow>
                                <FisContainerRow>
                                    <FisContainerColumn >
                                        <FisInputGroup label="My Label">
                                            <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={5} />
                                            <FisCheckboxRadioGroup inputlist={inputlist} opts={optsDefault} />
                                            <FisInputBase
                                                infoflow
                                                placeholder="Name"
                                                defaultValue="feld info"
                                                colSpan={5}
                                            />
                                        </FisInputGroup>
                                    </FisContainerColumn>
                                    <FisContainerColumn >
                                        <FisCheckboxRadioGroup inputlist={inputlist} opts={optsDefault} />
                                    </FisContainerColumn>
                                </FisContainerRow>
                                <FisContainerRow>
                                    <FisContainerColumn colSpan={9}>
                                        <FisInputGroup label="My Label">
                                            <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={5} />
                                            <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={4} />
                                        </FisInputGroup>
                                    </FisContainerColumn>
                                    <FisContainerColumn colSpan={8}>
                                        <FisCheckboxRadioGroup inputlist={inputlist} opts={optsDefault} />
                                    </FisContainerColumn>
                                    <FisContainerColumn colSpan={8}>
                                        <FisCheckboxRadioGroup inputlist={inputlist} opts={optsDefault} />
                                    </FisContainerColumn>
                                </FisContainerRow>
                                <FisContainerRow>
                                    <FisContainerColumn >
                                        <FisInputGroup label="My Label">
                                            <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={5} />
                                            <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={5} />
                                            <FisInputBase
                                                infoflow
                                                placeholder="Name"
                                                defaultValue="feld  info"
                                                colSpan={5}
                                            />
                                        </FisInputGroup>
                                    </FisContainerColumn>
                                </FisContainerRow>
                                <FisContainerRow>
                                    <FisContainerColumn >
                                        <FisInputGroup label="My Label">
                                            <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={5} />
                                            <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={5} />
                                            <FisInputBase
                                                infoflow
                                                placeholder="Name"
                                                defaultValue="feld  info"
                                                colSpan={5}
                                            />
                                        </FisInputGroup>
                                    </FisContainerColumn>
                                </FisContainerRow>
                                <FisContainerRow>
                                    <FisContainerColumn >
                                        <FisInputGroup label="My Label">
                                            <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={5} />
                                            <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={5} />
                                            <FisInputBase
                                                infoflow
                                                placeholder="Name"
                                                defaultValue="feld  info"
                                                colSpan={5}
                                            />
                                        </FisInputGroup>
                                    </FisContainerColumn>
                                </FisContainerRow>
                            </FisFieldset>
                        </FisContainerColumn>
                        <FisContainerColumn >
                            <FisContainerRow>
                                <FisContainerColumn >
                                    <FisFieldset legend="Transportation request" infoflow>
                                        <FisContainerRow>
                                            <FisContainerColumn >
                                                <FisInputGroup label="1-1 My Label">
                                                    <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={5} />
                                                    <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={5} />
                                                    <FisInputBase
                                                        infoflow
                                                        placeholder="Name"
                                                        defaultValue="feld  info"
                                                        colSpan={5}
                                                    />
                                                </FisInputGroup>
                                            </FisContainerColumn>
                                        </FisContainerRow>
                                        <FisContainerRow>
                                            <FisContainerColumn >
                                                <FisInputGroup label="1-2 My Label">
                                                    <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={5} />
                                                    <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={5} />
                                                    <FisInputBase
                                                        infoflow
                                                        placeholder="Name"
                                                        defaultValue="feld  info"
                                                        colSpan={5}
                                                    />
                                                </FisInputGroup>
                                            </FisContainerColumn>
                                        </FisContainerRow>
                                    </FisFieldset>
                                </FisContainerColumn>
                            </FisContainerRow>
                            <FisContainerRow>
                                <FisContainerColumn >
                                    <FisFieldset legend="Request" infoflow>
                                        <FisContainerRow>
                                            <FisContainerColumn >
                                                <FisInputGroup label="2-1 My Label">
                                                    <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={5} />
                                                    <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={5} />
                                                    <FisInputBase
                                                        infoflow
                                                        placeholder="Name"
                                                        defaultValue="feld  info"
                                                        colSpan={5}
                                                    />
                                                </FisInputGroup>
                                            </FisContainerColumn>
                                        </FisContainerRow>
                                        <FisContainerRow>
                                            <FisContainerColumn >
                                                <FisInputGroup label="2-2 My Label">
                                                    <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={5} />
                                                    <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={5} />
                                                    <FisInputBase
                                                        infoflow
                                                        placeholder="Name"
                                                        defaultValue="feld  info"
                                                        colSpan={5}
                                                    />
                                                </FisInputGroup>
                                            </FisContainerColumn>
                                        </FisContainerRow>
                                    </FisFieldset>
                                </FisContainerColumn>
                            </FisContainerRow>
                            <FisContainerRow>
                                <FisContainerColumn >
                                    <FisFieldset legend="Request" infoflow>
                                        <FisContainerRow>
                                            <FisContainerColumn >
                                                <FisInputGroup label="2-1 My Label">
                                                    <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={5} />
                                                    <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={5} />
                                                    <FisInputBase
                                                        infoflow
                                                        placeholder="Name"
                                                        defaultValue="feld  info"
                                                        colSpan={5}
                                                    />
                                                </FisInputGroup>
                                            </FisContainerColumn>
                                        </FisContainerRow>
                                        <FisContainerRow>
                                            <FisContainerColumn >
                                                <FisInputGroup label="2-2 My Label">
                                                    <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={5} />
                                                    <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={5} />
                                                    <FisInputBase
                                                        infoflow
                                                        placeholder="Name"
                                                        defaultValue="feld 1"
                                                        colSpan={5}
                                                    />
                                                </FisInputGroup>
                                            </FisContainerColumn>
                                        </FisContainerRow>
                                    </FisFieldset>
                                </FisContainerColumn>
                            </FisContainerRow>

                        </FisContainerColumn>
                        <FisContainerColumn >
                            <FisFieldset legend="Transportation request" infoflow>
                                <FisContainerRow>
                                    <FisContainerColumn >
                                        <FisInputGroup label="My Label">
                                            <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={5} />
                                            <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={5} />
                                            <FisInputBase
                                                infoflow
                                                placeholder="Name"
                                                defaultValue="feld  info"
                                                colSpan={5}
                                            />
                                        </FisInputGroup>
                                    </FisContainerColumn>
                                </FisContainerRow>
                                <FisContainerRow>
                                    <FisContainerColumn >
                                        <FisInputGroup label="My Label">
                                            <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={5} />
                                            <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={5} />
                                            <FisInputBase
                                                infoflow
                                                placeholder="Name"
                                                defaultValue="feld  info"
                                                colSpan={5}
                                            />
                                        </FisInputGroup>
                                    </FisContainerColumn>
                                </FisContainerRow>
                                <FisContainerRow>
                                    <FisContainerColumn >
                                        <FisInputGroup label="My Label">
                                            <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={5} />
                                            <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={5} />
                                            <FisInputBase
                                                infoflow
                                                placeholder="Name"
                                                defaultValue="feld  info"
                                                colSpan={5}
                                            />
                                        </FisInputGroup>
                                    </FisContainerColumn>
                                </FisContainerRow>
                                <FisContainerRow>
                                    <FisContainerColumn >
                                        <FisInputGroup label="My Label">
                                            <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={5} />
                                            <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={5} />
                                            <FisInputBase
                                                infoflow
                                                placeholder="Name"
                                                defaultValue="feld  info"
                                                colSpan={5}
                                            />
                                        </FisInputGroup>
                                    </FisContainerColumn>
                                </FisContainerRow>
                                <FisContainerRow>
                                    <FisContainerColumn >
                                        <FisInputGroup label="My Label">
                                            <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={5} />
                                            <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={5} />
                                            <FisInputBase
                                                infoflow
                                                placeholder="Name"
                                                defaultValue="feld  info"
                                                colSpan={5}
                                            />
                                        </FisInputGroup>
                                    </FisContainerColumn>
                                </FisContainerRow>
                                <FisContainerRow>
                                    <FisContainerColumn >
                                        <FisInputGroup label="My Label">
                                            <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={5} />
                                            <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={5} />
                                            <FisInputBase
                                                infoflow
                                                placeholder="Name"
                                                defaultValue="feld  info"
                                                colSpan={5}
                                            />
                                        </FisInputGroup>
                                    </FisContainerColumn>
                                </FisContainerRow>
                            </FisFieldset>
                        </FisContainerColumn>
                        <FisContainerColumn >
                            <FisContainerRow>
                                <FisContainerColumn >
                                    <FisFieldset legend="Request" infoflow>
                                        <FisContainerRow>
                                            <FisContainerColumn >
                                                <FisInputGroup label="2-1 My Label">
                                                    <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={5} />
                                                    <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={5} />
                                                    <FisInputBase
                                                        infoflow
                                                        placeholder="Name"
                                                        defaultValue="feld  info"
                                                        colSpan={5}
                                                    />
                                                </FisInputGroup>
                                            </FisContainerColumn>
                                        </FisContainerRow>
                                        <FisContainerRow>
                                            <FisContainerColumn >
                                                <FisInputGroup label="2-2 My Label">
                                                    <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={5} />
                                                    <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={5} />
                                                    <FisInputBase
                                                        infoflow
                                                        placeholder="Name"
                                                        defaultValue="feld  info"
                                                        colSpan={5}
                                                    />
                                                </FisInputGroup>
                                            </FisContainerColumn>
                                        </FisContainerRow>
                                    </FisFieldset>
                                </FisContainerColumn>
                            </FisContainerRow>
                            <FisContainerRow>
                                <FisContainerColumn >
                                    <FisFieldset legend="Request" infoflow>
                                        <FisContainerRow>
                                            <FisContainerColumn >
                                                <FisInputGroup label="2-1 My Label">
                                                    <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={5} />
                                                    <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={5} />
                                                    <FisInputBase
                                                        infoflow
                                                        placeholder="Name"
                                                        defaultValue="feld  info"
                                                        colSpan={5}
                                                    />
                                                </FisInputGroup>
                                            </FisContainerColumn>
                                        </FisContainerRow>
                                        <FisContainerRow>
                                            <FisContainerColumn >
                                                <FisInputGroup label="2-2 My Label">
                                                    <FisInputBase placeholder="Name" defaultValue="feld 0" colSpan={5} />
                                                    <FisInputDate placeholder="Name" defaultValue="feld 1" colSpan={5} />
                                                    <FisInputBase
                                                        infoflow
                                                        placeholder="Name"
                                                        defaultValue="feld  info"
                                                        colSpan={5}
                                                    />
                                                </FisInputGroup>
                                            </FisContainerColumn>
                                        </FisContainerRow>
                                    </FisFieldset>
                                </FisContainerColumn>
                            </FisContainerRow>
                        </FisContainerColumn>
                    </FisStripe>
                    <FisStripe
                        title="Result"
                        isOpened={false}
                        buttonGroups={buttonsGroups}
                    >
                        <div>GANZ VIEL RESULTS</div>
                    </FisStripe>

                    <FisStripe
                        title="Detail"
                        isOpened={false}
                        buttonGroups={buttonsGroups}
                    >
                        <div>GANZ VIEL DETAIL</div>
                    </FisStripe>
                </FisPanel>
            </FisClient>
        );
    }
}
