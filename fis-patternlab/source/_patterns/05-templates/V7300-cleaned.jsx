import React from 'react';

import { FisPanel } from 'fisui.js';
import { FisStripe } from 'fisui.js';
import { FisInputGroup } from 'fisui.js';
import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';
import { FisInputBase } from 'fisui.js';
import { FisInputDate } from 'fisui.js';
import { FisInputSuggest } from 'fisui.js';
import { FisCheckboxRadioGroup } from 'fisui.js';
import { FisText } from 'fisui.js';
import { FisDataGrid2 } from 'fisui.js';

// FIXME: DELETE WHEN  IS REALLY USED
/* eslint-disable */
export default class V7300Cleaned extends React.Component {
    render() {

        return (
                <form>

                    <FisPanel buttonGroupsTop={[]} buttonGroupsMiddle={[]} buttonGroupsBottom={[]}>
            			<FisStripe isOpened={true} buttonGroups={[
            												{ buttons: [{title: "Clear", variant: "content-default"},{title: "Find", variant: "content-hero"}]}]}>
            				<FisContainerColumn className="flex-box" colSpan={100}>
            				<div>
            				<FisContainerRow>
            					<FisContainerColumn colSpan={100} className="flex-box">
            						<FisText>To obtain your schedule, provide start location, end location and the required time range. By default, the transport is preset to apply from / to carrier's corresponding container terminal or ramp.</FisText>
            					</FisContainerColumn>
            				</FisContainerRow>
            				<FisContainerRow>
            					<FisContainerColumn colSpan={100} className="flex-box">
            						<FisContainerRow>
            							<FisContainerColumn colSpan={42} className="flex-box">
            								<FisContainerRow>
            									<FisContainerColumn colSpan={35} className="flex-box">
            										<div>
            										<FisContainerRow>
            											<FisContainerColumn colSpan={24} className="flex-box">
            												<FisInputGroup label="Start Location">
            													<FisInputBase type="text" mandatory={true}
            															colSpan={24} />
            												</FisInputGroup>
            											</FisContainerColumn>
                                                        <FisContainerColumn colSpan={10} className="flex-box">
            												<FisContainerRow>
            													<FisContainerColumn colSpan={4} className="flex-box">
            														<FisInputGroup label=" ">
            															<FisInputDate colSpan={4} mandatory={true} placeholder="2015-12-24" />
            														</FisInputGroup>
            													</FisContainerColumn>
            													<FisContainerColumn colSpan={5} className="flex-box">
            														<FisInputGroup label=" ">
            															<FisInputBase type="text" prefix="Plus" suffix="week(s)"
            																	colSpan={5} />
            														</FisInputGroup>
            													</FisContainerColumn>
            												</FisContainerRow>
            											</FisContainerColumn>
            										</FisContainerRow>
            										<FisContainerRow>
            											<FisContainerColumn colSpan={24} className="flex-box">
            												<FisInputGroup label="Via 1">
            													<FisInputBase type="text"
            															colSpan={24} />
            												</FisInputGroup>
            											</FisContainerColumn>
            										</FisContainerRow>
            										<FisContainerRow>
            											<FisContainerColumn colSpan={24} className="flex-box">
            												<FisInputGroup label="Via 2">
            													<FisInputBase type="text"
            															colSpan={24} />
            												</FisInputGroup>
            											</FisContainerColumn>
            										</FisContainerRow>
            										<FisContainerRow>
            											<FisContainerColumn colSpan={24} className="flex-box">
            												<FisInputGroup label="End Location">
            													<FisInputBase type="text" mandatory={true}
            															colSpan={24} />
            												</FisInputGroup>
            											</FisContainerColumn>
                                                        <FisContainerColumn colSpan={4} className="flex-box">
                                                            <FisInputGroup label=" ">
                                                                <FisInputDate colSpan={4} placeholder="2015-12-24" />
                                                            </FisInputGroup>
                                                        </FisContainerColumn>
            										</FisContainerRow>
            										<FisContainerRow>
            											<FisContainerColumn colSpan={11} className="flex-box">
            												<FisInputGroup label="Export MoT">
            													<FisInputSuggest colSpan={11} />
            												</FisInputGroup>
            											</FisContainerColumn>
            										</FisContainerRow>
            										<FisContainerRow>
            											<FisContainerColumn colSpan={11} className="flex-box">
            												<FisInputGroup label="Import MoT">
            													<FisInputSuggest colSpan={11} />
            												</FisInputGroup>
            											</FisContainerColumn>
            										</FisContainerRow>
            										<FisContainerRow>
            											<FisContainerColumn colSpan={21} className="flex-box">
                                                            <FisCheckboxRadioGroup colSpan={21} inputlist={[{ label: "Optimize routing for reefer equipment", name: "EReeferFlag_Flag" }]} opts={{type: "checkbox", label: "", stacked: false, name: "EReeferFlag_Flag" , standalone: false}} />
            											</FisContainerColumn>
            										</FisContainerRow>
            										</div>
            									</FisContainerColumn>
            								</FisContainerRow>
            							</FisContainerColumn>
            							<FisContainerColumn colSpan={20} className="flex-box">
            								<div>
            								<FisContainerRow>
            									<FisContainerColumn colSpan={20} className="flex-box">
            										<FisCheckboxRadioGroup inputlist={[
            											{ label: "Received at your door (CH)" },
            											{ label: "Received at container terminal (MH)" }
            										]} opts={{type: "radio", label: " ", stacked: true}} />
            									</FisContainerColumn>
            								</FisContainerRow>
            								<FisContainerRow>
            									<FisContainerColumn colSpan={20} className="flex-box">
            										<FisCheckboxRadioGroup inputlist={[
            											{ label: "Delivered at your door (CH)" },
            											{ label: "Delivered at container terminal (MH)" }
            										]} opts={{type: "radio", label: " ", stacked: true}} />
            									</FisContainerColumn>
            								</FisContainerRow>
            								</div>
            							</FisContainerColumn>
            						</FisContainerRow>
            					</FisContainerColumn>
            				</FisContainerRow>
            				</div>
            				</FisContainerColumn>
            			</FisStripe>

            			<FisStripe isOpened={true} buttonGroups={[{ buttons: [
										{title: "Routing Details", variant: "content-default"},
										{title: "Select for Booking", variant: "content-default"},
										{title: "Select Routing", variant: "content-default"}
										] }]}>
            				<FisContainerColumn className="flex-box" colSpan={100}>
            				<div>
            				<FisContainerRow>
            					<FisContainerColumn colSpan={12} className="flex-box">
            						<FisContainerRow>
            							<FisContainerColumn colSpan={6} className="flex-box">
            								<FisInputGroup label="Connection">
            									<FisInputBase type="text"
            											readOnly
            											colSpan={6} />
            								</FisInputGroup>
            							</FisContainerColumn>
            							<FisContainerColumn colSpan={6} className="flex-box">
            								<FisInputGroup label="Period">
            									<FisInputBase type="text"
            											readOnly
            											colSpan={6} />
            								</FisInputGroup>
            							</FisContainerColumn>
            						</FisContainerRow>
            					</FisContainerColumn>
            				</FisContainerRow>
            				<FisContainerRow>
            					<FisContainerColumn colSpan={100} className="flex-box">
            						<FisDataGrid2 columnDefs={[{ headerName:"Start Location", field:"Start Location", width: 245 },{ headerName:"Port of Loading", field:"Port of Loading", width: 245 },{ headerName:"Transshipments", field:"Transshipments", width: 20 },{ headerName:"Vessels / Services", field:"Vessels / Services", width: 245 },{ headerName:"Port of Discharge", field:"Port of Discharge", width: 245 },{ headerName:"End Location", field:"End Location", width: 245 },{ headerName:"Transit Time (days)", field:"Transit Time (days)", width: 20 }
            						]} rowData={[]} />
            					</FisContainerColumn>
            				</FisContainerRow>
            				</div>
            				</FisContainerColumn>
            			</FisStripe>
            			<FisStripe isOpened={true} title="Routing Details" buttonGroups={[{ buttons: [
										{title: "Vessel Details", variant: "content-default"},
										{title: "Vessel Tracing", variant: "content-default"},
										{title: "Terminal Details", variant: "content-default"}
										] }]}>
            				<FisContainerColumn className="flex-box" colSpan={100}>
            				<div>
            				<FisContainerRow>
            					<FisContainerColumn colSpan={100} className="flex-box">
            						<div>
            						<FisContainerRow>
            							<FisContainerColumn colSpan={100} className="flex-box">
            								<FisDataGrid2 columnDefs={[{ headerName:"Location", field:"Location", width: 245 },{ headerName:"Arrival", field:"Arrival", width: 80 },{ headerName:"Departure", field:"Departure", width: 80 },{ headerName:"Vessel / Mode of transport", field:"Vessel / Mode of transport", width: 245 },{ headerName:"Voyage", field:"Voyage", width: 80 },{ headerName:"Service", field:"Service", width: 80 }
            								]} rowData={[]} />
            							</FisContainerColumn>
            						</FisContainerRow>
            						</div>
            					</FisContainerColumn>
            				</FisContainerRow>
            				<FisContainerRow>
            					<FisContainerColumn colSpan={100} className="flex-box">
            						<div>
            						<FisContainerRow>
            							<FisContainerColumn colSpan={100} className="flex-box">
            								<FisText>Irrespective of the routing shown on our website, Hapag-Lloyd may not be allowed to perform certain transports due to local regulations. In case of discrepancies between this website and your quotation, booking confirmation or any individual agreement with Hapag-Lloyd, your quotation, booking confirmation or agreement with Hapag-Lloyd shall prevail.</FisText>
            							</FisContainerColumn>
            						</FisContainerRow>
            						</div>
            					</FisContainerColumn>
            				</FisContainerRow>
            				</div>
            				</FisContainerColumn>
            			</FisStripe>
            		</FisPanel>
                </form>
        );
    }
}
