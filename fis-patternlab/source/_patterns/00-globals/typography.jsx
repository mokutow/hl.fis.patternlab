import React from 'react';

import { FisContainerRow } from 'fisui.js';
import { FisContainerColumn } from 'fisui.js';

export default class FisTypoPattern extends React.Component {
    render() {
        return (
            <div>
                <h1>Fontstyles<span className="info"> @ 14px basesize</span></h1>

                <FisContainerRow className="row">
                    <FisContainerColumn colSpan={10}>
                        <h2>
                            Headline
                        </h2>
                        <small>
                            18/24px #333
                        </small>
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={65}>
                        <h1>Welcome to Future FIS</h1>
                    </FisContainerColumn>
                </FisContainerRow>

                <hr />


                <FisContainerRow className="row">
                    <FisContainerColumn colSpan={10}>
                        <h2>
                            Copytext
                        </h2>
                        <small>
                            14/20px #333 , #999, #CCC
                        </small>
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={15} className="demo-col">
                        <p>May the Force be with you</p>
                        <p className="text-color-mid">May the  Force be with you</p>
                        <p className="text-color-light">May the Force be with you</p>
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={30} className="demo-col">
                        <p><strong>May the Force be with you</strong></p>
                        <p className="text-color-mid"><strong>May the Force be with you</strong></p>
                        <p className="text-color-light"><strong>May the Force be with you</strong></p>
                    </FisContainerColumn>
                </FisContainerRow>

                <hr />

                <FisContainerRow className="row">
                    <FisContainerColumn colSpan={10}>
                        <h2>
                            Small Text
                        </h2>
                        <small>
                            12/16px #333 , #999
                        </small>
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={65}>
                        <small>May the Force be with you</small>
                        <br />
                        <small className="text-light">May the Force be with you</small>

                    </FisContainerColumn>
                </FisContainerRow>

                <hr />


                <FisContainerRow className="row">
                    <FisContainerColumn colSpan={10}>
                        <h2>
                            Links
                        </h2>
                        <small>
                            @Copytext<br />
                            #E75200, #42639C, #CCC
                        </small>
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={65}>
                        <a href="#">May the Force be with you</a>
                        <br />
                        <a href="#" className="secondary">May the Force be with you</a>
                        <br />
                        <a href="#" className="disabled">May the Force be with you</a>
                    </FisContainerColumn>
                </FisContainerRow>

                <hr />

                <FisContainerRow className="row">
                    <FisContainerColumn colSpan={10}>
                        <h2>
                            Table copy
                        </h2>
                        <small>
                            @Copytext<br />
                            #333, #CCC
                        </small>
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={65}>
                        <p >May the Force be with you</p>
                        <p className="text-color-light">May the Force be with you</p>
                    </FisContainerColumn>
                </FisContainerRow>

                <hr />

                <FisContainerRow className="row">
                    <FisContainerColumn colSpan={10}>
                        <h2>
                            Table Monospaced Copy
                        </h2>
                        <small>
                            Segoe UI Mono Regular / Bold<br />
                            14/23px
                            #333, #CCC
                        </small>
                    </FisContainerColumn>
                    <FisContainerColumn colSpan={65}>
                        <p className="mono">May the Force be with you at SHIPMENT Nr. 123456789</p>
                        <p className="mono text-color-light">May the Force be with you at SHIPMENT Nr. 123456789</p>
                    </FisContainerColumn>
                </FisContainerRow>
            </div>
        );
    }
}
