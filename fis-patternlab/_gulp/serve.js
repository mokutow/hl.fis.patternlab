module.exports = (context) => {
    return () => {
        context.browserSync.init({
            debug: true,
            notify: false,
            open: false,
            ui: false,
            server: {
                baseDir: context.fispatternlab.src + '/public/'
            }
        });

        context.gulp.watch([
            context.fisuicomponents.src + '/components/**/*.scss',
            '!' + context.fisuicomponents.src + '/components/00-globals/_svgsprite.scss',
            context.fispatternlab.src + '/source/_patterns/**/*.scss'],
            ['lab:sass:build']);

        context.gulp.watch(context.fisuicomponents.src + '/assets/**/*', ['lab:assets:reload']);

        context.gulp.watch(context.fispatternlab.src + '/source/_styleguide/**/*', ['lab:styleguide:reload']);

        context.gulp.watch(
            [
                context.fispatternlab.src + '/source/_patterns/**/*.mustache',
                context.fispatternlab.src + '/source/_patterns/**/*.json',
                context.fispatternlab.src + '/source/_data/*.json'],
            ['lab:mustache:reload']
        );

        context.gulp.watch(
            [
                context.fispatternlab.src + '/source/_patterns/**/*.jsx',
                context.fispatternlab.src + '/source/_patterns/**/*.js',
                context.fisuicomponents.src + '/components/**/*.jsx',
                context.fisuicomponents.src + '/components/**/*.js'
            ],
            ['lab:js:build']);
    };
};
