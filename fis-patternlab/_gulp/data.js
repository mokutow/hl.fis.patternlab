module.exports = (context) => {
    return () => {
        return context.gulp.src('**/*', { cwd: `${context.fispatternlab.src}/source/_data/` })
            .pipe(context.gulp.dest('public/data', { cwd: context.fispatternlab.src }));
    };
};
