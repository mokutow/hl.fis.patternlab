module.exports = (context) => {
    return () => {
        return context.gulp.src(['**/*', '!**/*.scss'], { cwd: context.fispatternlab.src + '/source/_styleguide' })
            .pipe(context.gulp.dest('public/styleguide', { cwd: context.fispatternlab.src }));
    };
};
