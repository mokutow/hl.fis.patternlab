module.exports = (context) => {
    return () => {
        return context.gulp.src('**/*', { cwd: context.fisuicomponents.target })
            .pipe(context.gulp.dest('public', { cwd: context.fispatternlab.src }));
    };
};
