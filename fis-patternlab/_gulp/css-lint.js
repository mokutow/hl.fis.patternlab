
module.exports = (context) => {
    return () => {
        return context.gulp.src(context.fispatternlab.src + '/source/_patterns/**/*.scss')
            .pipe(context.gulpPlugins.cached('lab:sass:lint'))
            .pipe(context.gulpPlugins.scssLint({ config: './_gulp-scsslint.yml' }))
            .pipe(context.gulpPlugins.scssLint.failReporter('E'))
            .on('error', context.gulpNotify.onError('<%= error.message %>'));
    };
};
