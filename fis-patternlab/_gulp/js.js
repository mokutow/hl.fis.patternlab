const path = require('path');
const named = require('vinyl-named');
const webpack = require('webpack-stream');

module.exports = (context) => {
    return () => {
        const WEBPACK_OPTIONS = {
            output: {
                filename: '[name].js',
                library: 'FisPatterns',
                libraryTarget: 'umd',
                umdNamedDefine: true
            },
            resolve: {
                root: [
                    path.resolve(`${context.fispatternlab.src}/source/_patterns`),
                    path.resolve(`${context.fisuicomponents.src}/components`),
                    path.resolve(`${context.fisuicomponents.src}/components/00-globals`),
                    path.resolve(`${context.fisuicomponents.src}/components/01-atoms`),
                    path.resolve(`${context.fisuicomponents.src}/components/02-molecules`),
                    path.resolve(`${context.fisuicomponents.src}/components/03-organisms`),
                    path.resolve(`${context.fisuicomponents.src}/components/04-business`)
                ],
                extensions: ['', '.js', '.jsx'],
                alias: {
                    'fisui.js': 'index.js'
                }
            },
            module: {
                loaders: [
                    {
                        test: /\.jsx?$/,
                        loaders: ['babel'],
                        include: [
                            path.resolve(path.join(context.fispatternlab.src, 'source/_patterns')),
                            path.resolve(path.join(context.fisuicomponents.src, 'components'))
                        ]
                    },
                    { test: require.resolve('react'), loader: 'expose?React' },
                    { test: require.resolve('react-dom'), loader: 'expose?ReactDOM' }
                ]
            },
            devtool: 'cheap-module-source-map'
        };

        context.gulp.src(context.fispatternlab.src + '/source/_patterns/patterns.js')
            .pipe(named())
            .pipe(webpack(WEBPACK_OPTIONS))
            .on('error', context.gulpNotify.onError('<%= error.message %>'))
            .pipe(context.gulp.dest(context.fispatternlab.src + '/public/js/'))
            .pipe(context.gulpPlugins.cached('lab:js:build'))
            .pipe(context.gulpPlugins.if(context.browserSync.active, context.browserSync.reload({ stream: true })));
    };
};
