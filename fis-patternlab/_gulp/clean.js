const del = require('del');

module.exports = (context) => {
    return () => {
        // The glob pattern ** matches all children and the parent.
        del.sync([context.fispatternlab.src + '/public/**', '!' + context.fispatternlab.src + '/public'], { force: true });
    };
};
