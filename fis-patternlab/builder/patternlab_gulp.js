/**
 * patternlab-node - v1.0.0 - 2015
 *
 * Brian Muenzenmeyer, and the web community.
 * Licensed under the MIT license.
 *
 * Many thanks to Brad Frost and Dave Olsen for inspiration, encouragement, and advice.
 *
 **/
var patternlab_engine = require('./patternlab.js');
var path = require('path');

module.exports = function(gulp) {

    gulp.task('patternlab', /*['clean'],*/ function(cb){
        var cwd = process.cwd();
        process.chdir(path.normalize(__dirname + path.sep + '..'))
        var patternlab = patternlab_engine();
        patternlab.build(false);
        process.chdir(cwd);
        cb();
    });

    gulp.task('patternlab:only_patterns', ['clean'], function(){
        var patternlab = patternlab_engine();
        patternlab.build_patterns_only(false);
    });

    gulp.task('patternlab:help', function(){
        var patternlab = patternlab_engine();
        patternlab.help();
    });

}
