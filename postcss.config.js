module.exports = () => {
    const autoprefixer = require('autoprefixer');
    const autoprefixerOptions = {
        browsers: [
            'ie >= 10',
            'ie_mob >= 10',
            'ff >= 30',
            'chrome >= 34',
            'safari >= 7',
            'opera >= 23',
            'ios >= 7',
            'android >= 4.4',
            'bb >= 10'],
        cascade: false };

    const postcssProcessors = [
        autoprefixer(autoprefixerOptions)
    ];

    return postcssProcessors;
};
